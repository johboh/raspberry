#-------------------------------------------------
#
# Project created by QtCreator 2013-01-28T10:53:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network

TARGET = now_playing
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    imagerequest.cpp

HEADERS  += mainwindow.h \
    imagerequest.h

FORMS    += mainwindow.ui
