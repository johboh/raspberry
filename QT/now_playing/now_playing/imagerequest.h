#ifndef IMAGEREQUEST_H
#define IMAGEREQUEST_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QPixmap>

class ImageRequest : public QObject
{
    Q_OBJECT
public:
    enum ImageSize { Small, Normal, Large };

    explicit ImageRequest(QObject *parent = 0);
    ImageRequest(QString host, QString album_uri, ImageSize size, int id);

    virtual ~ImageRequest();
signals:
    void onImage(ImageRequest * request, QPixmap *image, QString &album_uri, ImageRequest::ImageSize size, int id);
public slots:
     void finished(QNetworkReply *reply);

private:
    QNetworkAccessManager *_nam;
    QString _album_uri;
    ImageSize _size;
    QPixmap _image;
    int _id;
};

#endif // IMAGEREQUEST_H
