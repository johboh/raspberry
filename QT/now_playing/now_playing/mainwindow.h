#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariant>
#include <QLabel>
#include <QPixmapCache>
#include <imagerequest.h>

#define QUEUE_SIZE 5

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onImage(ImageRequest * request, QPixmap *image, QString &album_uri, ImageRequest::ImageSize size, int id);
    void fetchQueue(int limit = QUEUE_SIZE);
    void finished(QNetworkReply *reply);
    void close();

private:
    void setImage(QPixmap *image, int id);

private:
    Ui::MainWindow *ui;

    QNetworkAccessManager *_nam;
    QVariantMap _queue;
    QPixmapCache _cache;
    QLabel _queue_labels[QUEUE_SIZE];
};

#endif // MAINWINDOW_H
