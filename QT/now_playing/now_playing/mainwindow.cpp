#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <QLabel>

#define REFRESH_STATUS_RATE 2000
//#define HOST "127.0.0.1"
#define HOST "fjun.com"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setStyleSheet("background-color: black;");
    ui->label_title->setStyleSheet("color: white; font-size: 28px;");

    _nam = new QNetworkAccessManager(this);
    connect(_nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(finished(QNetworkReply*)));

    _cache.setCacheLimit(1024 * 10);

    for(int i = 0; i < sizeof(_queue_labels) / sizeof(QLabel); i++)
        ui->gridLayout->addWidget(&_queue_labels[i], i, 0);

    fetchQueue(QUEUE_SIZE);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::close() {
   QApplication::quit();
}

void MainWindow::fetchQueue(int limit) {
    QString url = "http://" + QString(HOST) + ":666/1/queue/limit/" + QString::number(limit);
    _nam->get(QNetworkRequest(QUrl(url)));
}

void MainWindow::finished(QNetworkReply *reply) {
    if(reply->error() == QNetworkReply::NoError) {
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        QVariantMap queue = doc.object().toVariantMap();

        if (queue != _queue) {
            _queue = queue;
            QList<QVariant> tracks_list = _queue["tracks"].toList();
            if (tracks_list.size() < 1) {
                QTimer::singleShot(REFRESH_STATUS_RATE, this, SLOT(fetchQueue()));
                return;
            }

            QVariantMap track_map = qvariant_cast<QVariantMap>(tracks_list[0]);
            QVariantMap artist_map = qvariant_cast<QVariantMap>(track_map["artist"]);

            QString track_name = track_map["name"].toString();
            QString artist_name = artist_map["name"].toString();

            QString status = track_name + " - " + artist_name + "\n";
            ui->label_title->setText(status);

            for(int i = 0; i < tracks_list.size(); i++) {
                QPixmap image;
                QString key = qvariant_cast<QVariantMap>(qvariant_cast<QVariantMap>(tracks_list[i])["album"])["uri"].toString();
                if(_cache.find(key, &image)) {
                        setImage(&image, i);
                } else {
                    ImageRequest *request = new ImageRequest(HOST, key, ImageRequest::Large, i);
                    connect(request, SIGNAL(onImage(ImageRequest * , QPixmap*, QString&, ImageRequest::ImageSize, int)), this, SLOT(onImage(ImageRequest *, QPixmap*, QString&, ImageRequest::ImageSize, int)));
                }
            }
        }
        QTimer::singleShot(REFRESH_STATUS_RATE, this, SLOT(fetchQueue()));
    }
    else {
        ui->label_title->setText(reply->errorString());
        QTimer::singleShot(REFRESH_STATUS_RATE*2, this, SLOT(fetchQueue()));
    }
}

void MainWindow::onImage(ImageRequest * request, QPixmap *image, QString &album_uri, ImageRequest::ImageSize size, int id) {
    if (image != NULL) {
        _cache.insert(album_uri, *image);
        setImage(image, id);
    }
    delete request;
}

void MainWindow::setImage(QPixmap *image, int id) {
    if (id == 0) {
        ui->label->setPixmap(*image);
    } else if (id < sizeof(_queue_labels) / sizeof(QLabel)) {
        _queue_labels[id].setPixmap(*image);
    }
}
