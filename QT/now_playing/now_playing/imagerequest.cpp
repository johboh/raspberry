#include "imagerequest.h"

ImageRequest::ImageRequest(QObject *parent) :
    QObject(parent)
{
}

ImageRequest::ImageRequest(QString host,QString album_uri, ImageSize size, int id) :
    _album_uri(album_uri),
    _size(size),
    _id(id) {

    _nam = new QNetworkAccessManager(this);
    connect(_nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(finished(QNetworkReply*)));

    QString ssize = "normal";
    if (size == Large)
        ssize = "large";
    else if (size == Small)
        ssize = "small";
    QString url = "http://" + host + ":666/1/" + album_uri + "/image/" + ssize;
    _nam->get(QNetworkRequest(QUrl(url)));
}

ImageRequest::~ImageRequest() {
    delete _nam;
}

void ImageRequest::finished(QNetworkReply *reply) {
    if(reply->error() == QNetworkReply::NoError) {
        if (_image.loadFromData(reply->readAll())) {
            onImage(this, &_image, _album_uri, _size, _id);
        } else {
            onImage(this, NULL, _album_uri, _size, _id);
        }
    } else {
        onImage(this, NULL, _album_uri, _size, _id);
    }
}
