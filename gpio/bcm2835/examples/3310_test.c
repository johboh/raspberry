// spi.c
//
// Example program for bcm2835 library
// Shows how to interface with SPI to transfer a byte to and from an SPI device
//
// After installing bcm2835, you can build this 
// with something like:
// gcc -o 3310_test 3310.c 3310_test.c -l bcm2835
// sudo ./3310_test
//
// Or you can test it before installing with:
// gcc -o spi -I ../../src ../../src/bcm2835.c spi.c
// sudo ./spi
//
// Author: Mike McCauley (mikem@open.com.au)
// Copyright (C) 2012 Mike McCauley
// $Id: RF22.h,v 1.21 2012/05/30 01:51:25 mikem Exp $

#include <bcm2835.h>
#include "3310.h"
#include <stdio.h>


int main(int argc, char **argv)
{
    // If you call this, it will not actually access the GPIO
// Use for testing
//        bcm2835_set_debug(1);

      if (!bcm2835_init())
	return 1;

    bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // The default
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // The default
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_2048); // 2048 = 8us = 125kHz
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                     // CS is CS0
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);

    // Setup RESET and Data/Control to be output.
    bcm2835_gpio_fsel(PIN_RES, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(PIN_DC, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(PIN_RES, HIGH);
    bcm2835_gpio_write(PIN_DC, HIGH);

    printf("About to init!\n");

    // Init display.
    displayInit();

    displayGotoXY(0, 1);
    displayWriteString("Guess Artist");
    displayGotoXY(0, 2);
    displayWriteString("Michael Jackson");
    displayGotoXY(0, 3);
    displayWriteString("Britney Spears");
    displayGotoXY(0, 4);
    displayWriteString("Evin Beningway");
    displayGotoXY(0, 5);
    displayWriteString("Julia Estherfan");

    bcm2835_spi_end();
    return 0;
}

