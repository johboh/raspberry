// input.c
//
// Example program for bcm2835 library
// Reads and prints the state of an input pin
//
// After installing bcm2835, you can build this 
// with something like:
// gcc -o input input.c -l bcm2835
// sudo ./input
//
// Or you can test it before installing with:
// gcc -o input -I ../../src ../../src/bcm2835.c input.c
// sudo ./input
//
// Author: Mike McCauley (mikem@open.com.au)
// Copyright (C) 2011 Mike McCauley
// $Id: RF22.h,v 1.21 2012/05/30 01:51:25 mikem Exp $

#include <bcm2835.h>
#include <stdio.h>

// Input on RPi pin GPIO 15
#define PIN_A RPI_GPIO_P1_13
#define PIN_B RPI_GPIO_P1_12
#define PIN_C RPI_GPIO_P1_18
#define PIN_D RPI_GPIO_P1_11

typedef struct {
    RPiGPIOPin pin;
    char ch;
} Pin;

int main(int argc, char **argv)
{
    // If you call this, it will not actually access the GPIO
    // Use for testing
//    bcm2835_set_debug(1);

    if (!bcm2835_init())
	return 1;

    Pin pins[4] = { {PIN_A, 'A'},
             {PIN_B, 'B'},
             {PIN_C, 'C'},
             {PIN_D, 'D'}
           };

    const uint8_t numPins = sizeof(pins) / sizeof(Pin);
    // Setup pins
    int i = 0;
    for(i = 0; i < numPins; i++) {
        Pin *pin = &pins[i];

        // Setup pin as input.
        bcm2835_gpio_fsel(pin->pin, BCM2835_GPIO_FSEL_INPT);
        // With pullup
        bcm2835_gpio_set_pud(pin->pin, BCM2835_GPIO_PUD_UP);
        // Clear low level detection, as it will hang (bug).
        bcm2835_gpio_clr_len(pin->pin);
    }

    // Read forever.
    while (1)
    {
        // Read all inputs.
        for(i = 0; i < numPins; i++) {
            Pin *pin = &pins[i];
            uint8_t value = bcm2835_gpio_lev(pin->pin);
            printf("%c: %d ", pin->ch, value);
        }
        printf("\n");
	
	// wait a bit
	delay(250);
    }

    return 0;
}

