#include "3310.h"
#include <bcm2835.h>

/**
* Initialize display.
*/
void displayInit(void) {
    // Reset display.
    bcm2835_gpio_write(PIN_RES, LOW);
    delay(100);
    bcm2835_gpio_write(PIN_RES, HIGH);

    // Init display.
    displayWriteCommand(0x21);  // LCD Extended Commands.
    displayWriteCommand(0xd8);  // Vop  v1: 0xc8 (for 3V)// v2: 0xa0 (for 3V) // v3: 0xc2 (2v6-5v) 
    displayWriteCommand(0x06);  // Set Temp coefficent
    displayWriteCommand(0x13);  // LCD bias mode 1:48.
    displayWriteCommand(0x20);  // LCD Standard Commands, Horizontal addressing mode.
    displayWriteCommand(0x0c);  // LCD in normal mode.

    displayClear();
}

/**
* Write a command to the LCD controller.
*/
void displayWriteCommand(uint8_t command)
{
    // Set in command mode.
    bcm2835_gpio_write(PIN_DC, LOW);

    // Write command
    bcm2835_spi_transfer(command);
}

/**
* Write a data to the LCD controller.
*/
void displayWriteData(uint8_t data)
{
    // Set in data mode.
    bcm2835_gpio_write(PIN_DC, HIGH);

    // Write data
    bcm2835_spi_transfer(data);
}

/**
* Clear the display.
*/
void displayClear()
{
    uint16_t i = 0;
    uint16_t j = 0;

    // Start at position (0,0)
    displayGotoXY(0,0);

    for(i=0; i<8; i++) {
        for(j=0; j<90; j++) {
            displayWriteData(0x00);
        }
    }   

    // Go back to position (0,0)
    displayGotoXY(0,0);
}

/**
* Sets cursor location to xy location corresponding to basic font size.
* x - range: 0 to 84
* y -> range: 0 to 5
*/
void displayGotoXY(uint8_t x, uint8_t y) {
    displayWriteCommand(0x80 | x); // row
    displayWriteCommand(0x40 | y); // column
}

/**
* Write a character at the current location.
*
*/
void displayWriteChar(unsigned char ch)
{
    unsigned char i;

    // Clear first.
    displayWriteData(0x00);    
   
    for(i=0; i<5; i++)
        displayWriteData(FontLookup[ ch - 32 ][ i ]);
	 
   displayWriteData(0x00);
} 

/**
* Write a string at the current location.
*/
void displayWriteString(unsigned char * str) {
    while(*str) {
        displayWriteChar(*str++);
    }
}
