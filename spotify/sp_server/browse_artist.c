#include "commands.h"
#include "error.h"
#include "metadata.h"
#include <semaphore.h>

static void artist_browse_complete(sp_artistbrowse *result, void * userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

int command_browse_artist(sp_session *session, client_t * client, sp_link *link, sp_artistbrowse_type browse_type) {

	if (!link)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type artist (null).");
	
	if (sp_link_type(link) != SP_LINKTYPE_ARTIST) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type artist.");
	}

	sp_artistbrowse * browse = sp_artistbrowse_create(session, sp_link_as_artist(link), browse_type, &artist_browse_complete, client);
	sp_link_release(link);
	if (!browse)
		return command_error(client, ERROR_BROWSE_FAILED, "Failed to create artist browse request.");

	// If not loaded? wait for callback.
	while(sp_artistbrowse_error(browse) == SP_ERROR_IS_LOADING) {
		pthread_mutex_unlock(client->api_mutex);
		sem_wait(&client->sem);
		pthread_mutex_lock(client->api_mutex);
	}

	// Check for browse error, like non existing resource.
	sp_error err = sp_artistbrowse_error(browse);
	if (err != SP_ERROR_OK) {
		sp_artistbrowse_release(browse);
		if (err == SP_ERROR_OTHER_PERMANENT)
			return command_error(client, ERROR_RESOURCE_NOT_FOUND, sp_error_message(err));
		else
			return command_error(client, ERROR_BROWSE_FAILED, sp_error_message(err));
	}		

	uint32_t i = 0;	

	// Artist name and uri.
	json_t * result = artist_to_json(sp_artistbrowse_artist(browse));
	// Biography
	json_object_set_new(result, "biography", json_string(sp_artistbrowse_biography(browse)));

	json_t *tracks = json_array();
	for(i = 0; i < sp_artistbrowse_num_albums(browse); i++) {
		sp_track *track = sp_artistbrowse_track(browse, i);
		json_t * result_track = track_to_json(session, track, 0, 1);
		json_object_set_new(result_track, "index", json_integer(i));
		json_array_append_new(tracks, result_track);
	}
	json_object_set_new(result, "tracks", tracks);
	
	json_t *albums = json_array();
	for(i = 0; i < sp_artistbrowse_num_albums(browse); i++) {
		sp_album *album = sp_artistbrowse_album(browse, i);
		json_t * result_album = album_to_json(album, 1);
		json_array_append_new(albums, result_album);
	}
	json_object_set_new(result, "albums", albums);
	
	json_t *similar_artists = json_array();
	for(i = 0; i < sp_artistbrowse_num_similar_artists(browse); i++) {
		sp_artist *artist = sp_artistbrowse_similar_artist(browse, i);
		json_t * result_artist = artist_to_json(artist);
		json_array_append_new(similar_artists, result_artist);
	}
	json_object_set_new(result, "similar_artists", similar_artists);

	sp_artistbrowse_release(browse);
	return command_success(client, result);
}
