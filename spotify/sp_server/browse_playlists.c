#include "commands.h"
#include "error.h"
#include "metadata.h"
#include <semaphore.h>
#include <string.h>
#include <inttypes.h>

static void container_loaded(sp_playlistcontainer *pc, void *userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

sp_uint64 S64(const char *s) {
  sp_uint64 i;
  char c ;
  int scanned = sscanf(s, "%" SCNu64 "%c", &i, &c);
  if (scanned == 1) return i;
  if (scanned > 1) {
    // TBD about extra data found
    return i;
    }
  // TBD failed to scan;  
  return 0;  
}

int command_browse_playlists(sp_session *session, client_t * client, uint8_t flat, const char * root) {

	sp_uint64 folder_id = 0;
	if (root) {
		folder_id = S64(root);
	}

	sp_playlistcontainer *pc = sp_session_playlistcontainer(session);
	if (!pc)
		return command_error(client, ERROR_BROWSE_FAILED, "Failed to create playlist container.");
	sp_playlistcontainer_add_ref(pc);

	// Install callbacks.
	sp_playlistcontainer_callbacks callbacks;
	memset(&callbacks, 0, sizeof(sp_playlistcontainer_callbacks));
	callbacks.container_loaded = container_loaded;
	sp_playlistcontainer_add_callbacks(pc, &callbacks, client);
	
	// If not loaded? wait for callback.
	while(!sp_playlistcontainer_is_loaded(pc)) {
		pthread_mutex_unlock(client->api_mutex);
		sem_wait(&client->sem);
		pthread_mutex_lock(client->api_mutex);
	}
	
	// remove callbacks.
	sp_playlistcontainer_remove_callbacks(pc, &callbacks, client);

	json_t * result = json_object();
	json_t *playlists = json_array();
	uint32_t i = 0;
	uint32_t start = 0;
	uint32_t length = sp_playlistcontainer_num_playlists(pc);
	uint32_t current_depth = 0;

	// Find start of folder.
	uint8_t found_folder = 0;
	if (folder_id != 0) {
		for(i = 0; i < sp_playlistcontainer_num_playlists(pc); i++) {
			sp_playlist_type type = sp_playlistcontainer_playlist_type(pc, i);
			if (type == SP_PLAYLIST_TYPE_START_FOLDER) {
				sp_uint64 id = sp_playlistcontainer_playlist_folder_id(pc, i);
				if (id == folder_id) {
					start = i+1;
					found_folder = 1;
				}
				if (found_folder) {
					++current_depth;
				}
			}
			else if (type == SP_PLAYLIST_TYPE_END_FOLDER && found_folder) {
				--current_depth;
			}
			if (current_depth == 0 && found_folder) {
				length = i;
				break;
			}
		}
	}
	current_depth = 0;

	for(i = start; i < length; i++) {
		sp_playlist_type type = sp_playlistcontainer_playlist_type(pc, i);
		if (type == SP_PLAYLIST_TYPE_PLAYLIST && (flat || (!flat && current_depth == 0))) {
			sp_playlist *playlist = sp_playlistcontainer_playlist(pc, i);
			json_t * result_playlist = playlist_to_json(session, playlist);
			json_object_set_new(result_playlist, "index", json_integer(i));
			json_array_append_new(playlists, result_playlist);
		} else if (type == SP_PLAYLIST_TYPE_START_FOLDER && !flat) {
			if (current_depth == 0) {
				char buffer[512];
				sp_playlistcontainer_playlist_folder_name(pc, i, buffer, sizeof(buffer));
				sp_uint64 id = sp_playlistcontainer_playlist_folder_id(pc, i);
				json_t * folder = json_object();
				json_object_set_new(folder, "name", json_string(buffer));
				char buffer2[256];
				snprintf(buffer2, sizeof(buffer2), "%"PRIu64, id);
				json_object_set_new(folder, "id", json_string(buffer2));
				json_object_set_new(folder, "index", json_integer(i));
				json_object_set_new(folder, "type", json_string("folder"));
				json_array_append_new(playlists, folder);
			}
			++current_depth;
		} else if (type == SP_PLAYLIST_TYPE_END_FOLDER && !flat) {
			current_depth--;
		}
	}
	json_object_set_new(result, "playlists", playlists);
	
	
	sp_playlistcontainer_release(pc);
	return command_success(client, result);
}
