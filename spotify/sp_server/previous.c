#include "commands.h"
#include "state.h"
#include "error.h"

int command_previous(sp_session *session, client_t * client, uint8_t restart) {
	current_track_ended(session, restart ? kEndRestart : kEndPrevious);
	return command_status(session, client);
}
