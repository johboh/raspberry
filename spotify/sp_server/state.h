#ifndef __STATE_H__
#define __STATE_H__

#include <stdint.h>
#include <libspotify/api.h>

#define MAX_NUM_TRACKS 65535
#define MAX_QUEUE_METADATA_BYTES 128

typedef enum {
	kPlayingNone,
	kPlayingTrack,
	kPlayingAlbum,
	kPlayingPlaylist,
	kPlayingSearch,
} context_t;

typedef struct {
	uint8_t new_browse;
	sp_session *session;
	int index;
} session_index_t;

typedef struct {
	uint8_t paused;
	uint8_t loading;
	context_t context;
	union {
		sp_track * track;
		sp_albumbrowse * album_browse;
		sp_playlist * playlist;
		sp_search * search;
		void * resource;
	};
	sp_track *current_track;
	uint32_t current_index;
	session_index_t si;
	void (*playlist_metadata_updated)(sp_session *, void *);
	sp_playlist_callbacks *playlist_callbacks;
} playback_t;

typedef enum {
	kPlayonceNone,
	kPlayoncePause,
	kPlayonceKeep,
	kPlayoncePlay,
} playonce_t;

void state_init(void);

int metadata_updated_add_callback(void (*callback)(sp_session *, void *), void *args);
int metadata_updated_remove_callback(void (*callback)(sp_session *, void *), void *args);
void metadata_updated_notify(sp_session *session);

void repeat_set(uint8_t enable);
uint8_t repeat(void);

void shuffle_set(uint8_t enable);
uint8_t shuffle(void);

playback_t * playback(void);
void playback_set(context_t context, void * resource);
void playback_set_current(sp_track * current_track, uint32_t current_index, uint8_t playing);

void logged_in_set_callback(void (*callback)(sp_session *, sp_error error, void *), void *args);
void logged_in_clear_callback(void);
void logged_out_set_callback(void (*callback)(sp_session *, void *), void *args);
void logged_out_clear_callback(void);

void logged_in_notify(sp_session *session, sp_error error);
void logged_out_notify(sp_session *session);
uint8_t is_logged_in(sp_session *session);

void stop_no_release(sp_session *session);
void stop_and_release_playback(sp_session *session);
int load_and_play_next_available_track(sp_session *session, uint32_t start_index, int previous);
int load_and_playonce(sp_session *session, sp_track * track, uint32_t offset_ms, playonce_t next_action);

typedef enum {
	kEndOfPlayback,
	kEndNext,
	kEndPrevious,
	kEndRestart,
} end_reason_t;
void current_track_ended(sp_session * session, end_reason_t reason);

void generate_mapping(int32_t length);
uint32_t get_mapping(uint32_t index);
uint32_t get_mapping_length(void);

uint32_t get_context_num_tracks(void);
sp_track * get_context_track(uint32_t index);

sp_error get_last_error();

typedef struct {
	sp_track *track;
	char *metadata;
	uint32_t timestamp;
} queue_entry_t;

int queue_add(sp_track * track, uint32_t index, char *metadata);
int queue_remove(uint32_t index);
void queue_clear(void);
uint32_t queue_num_tracks(void);
queue_entry_t *queue_get_entry(uint32_t index);

void history_clear(void);
uint32_t history_num_tracks(void);
queue_entry_t *history_get_entry(uint32_t index);

#endif // __STATE_H__
