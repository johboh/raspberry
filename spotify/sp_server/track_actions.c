#include "track_actions.h"
#include "error.h"
#include <string.h>

int track_action(sp_session *session, client_t * client, sp_link *link, char **args, uint32_t num_args) {

	if (!link)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type track. (null).");
		
	if (sp_link_type(link) != SP_LINKTYPE_TRACK) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type track.");
	}

	sp_track * track = sp_link_as_track(link);
	sp_link_release(link);
	if (!track)
		return command_error(client, ERROR_RESOURCE_NOT_FOUND, "The track does not exist (null).");

	
	// Star or unstarred?
	if (num_args == 2 && !strcmp("star", args[0])) {
		int star = atoi(args[1]) ? 1 : 0;
		sp_track_set_starred(session, &track, 1, star);
		return command_success(client, NULL);
	}
		
	return command_error(client, ERROR_UNKNOWN_COMMAND, "Command for track not implemented.");
}