#ifndef __PLAYLIST_ACTIONS_H__
#define __PLAYLIST_ACTIONS_H__

#include <stdint.h>

#include <libspotify/api.h>
#include "client.h"

int playlist_action(sp_session *session, client_t * client, sp_link *link, sp_playlist *playlist, char **args, uint32_t num_args);

#endif // __PLAYLIST_ACTIONS_H__