#include "commands.h"
#include "state.h"
#include "error.h"

int command_volume(sp_session *session, client_t * client, int volume) {
	// Todo: Set volume 0-100, also -1 for mute.
	return command_status(session, client);
}

int command_volume_normalization(sp_session *session, client_t * client, int on) {
	sp_session_set_volume_normalization(session, on);
	return command_status(session, client);
}