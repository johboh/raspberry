#include "commands.h"
#include "state.h"
#include "error.h"
#include <string.h>

static void playback_track_metadata_updated(sp_session *session, void *args) {
	client_t *client = (client_t *)args;
	sem_post(&client->sem);
}

int command_play_track(sp_session *session, client_t * client, sp_link *link, uint32_t offset_ms, char * next_action) {
	if (!link)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type track (null).");
	
	if (sp_link_type(link) != SP_LINKTYPE_TRACK) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type track.");
	}

	uint8_t playonce = !!next_action;
	if (playonce && strcmp("pause", next_action) && strcmp("keep", next_action) && strcmp("resume", next_action)) {
		return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Unknown next action. Should one of: pause, keep, resume");
	}

	sp_track * track = sp_link_as_track(link);
	if (!track) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Invalid link type (null)."); 
	}
	sp_track_add_ref(track);
	sp_link_release(link);

	// If not loaded? wait for callback.
	if (sp_track_error(track) == SP_ERROR_IS_LOADING)
		metadata_updated_add_callback(&playback_track_metadata_updated, client);

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
		
	// As long as track is not loaded, wait.
	while(sp_track_error(track) == SP_ERROR_IS_LOADING) {
		pthread_mutex_unlock(client->api_mutex);
		// Timeout 1 sec.
		ts.tv_sec += 1;
		sem_timedwait(&client->sem, &ts);
		pthread_mutex_lock(client->api_mutex);
	}

	// Remove callback.
	metadata_updated_remove_callback(&playback_track_metadata_updated, client);
	
	// Loaded ok, or does track not exists?
	if (sp_track_error(track) != SP_ERROR_OK) {
		sp_track_release(track);
		return command_error(client, ERROR_RESOURCE_NOT_FOUND, "Track not found.");
	}
	else if (sp_track_get_availability(session, track) != SP_TRACK_AVAILABILITY_AVAILABLE) {
		sp_track_release(track);
		return command_error(client, ERROR_RESOURCE_UNAVAILABLE, "Track is not available for playback.");
	}

	// playonce command?
	if (playonce) {
		stop_no_release(session);
	} else {
		// When playing track, always free up old resource, even if the resouce is the same.
		stop_and_release_playback(session);
		generate_mapping(0);
	}

	// playonce command?
	if (playonce) {
		playonce_t na = !strcmp("pause", next_action) ? kPlayoncePause : (!strcmp("keep", next_action) ? kPlayonceKeep : kPlayoncePlay);
		load_and_playonce(session, track, offset_ms, na);
	} else {
		// Try to start playback
		generate_mapping(1);
		playback_set(kPlayingTrack, track);
		load_and_play_next_available_track(session, 0, 0);
		playback()->loading = 0;
	}
	
	return command_status(session, client);
}
