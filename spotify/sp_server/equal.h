#ifndef __EQUAL_H__
#define __EQUAL_H__

#include <libspotify/api.h>

int isCurrentAlbum(sp_link * link);
int isCurrentSearch(sp_link *link);
int isCurrentPlaylist(sp_link * link, sp_playlist *playlist);

#endif // __EQUAL_H__