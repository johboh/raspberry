#ifndef __PLAYLISTS_ACTIONS_H__
#define __PLAYLISTS_ACTIONS_H__

#include <stdint.h>

#include <libspotify/api.h>
#include "client.h"

int playlists_action(sp_session *session, client_t * client, char **args, uint32_t num_args);

#endif // __PLAYLISTS_ACTIONS_H__