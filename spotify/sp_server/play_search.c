#include "commands.h"
#include "state.h"
#include "error.h"
#include "equal.h"
#include <string.h>

static void search_complete(sp_search *search, void * userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

int command_play_search(sp_session *session, client_t * client, sp_link *link, uint32_t index) {

	// TODO: Check if search result was 0?

	if (!link)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type search (null).");
	
	if (sp_link_type(link) != SP_LINKTYPE_SEARCH) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type search.");
	}

	// Minimum 0.
	index = index < 0 ? 0 : index;

	// Free up resource if switching to new playback type and/or search.
	uint8_t is_same = isCurrentSearch(link);
	char uri[512];
	sp_link_as_string(link, uri, sizeof(uri));
	sp_link_release(link);

	if (!is_same)
		stop_and_release_playback(session);

	playback()->loading = 1;

	// Create new search request.
	if (!is_same || !playback()->search) {
		// TODO: in future, take offset as arguments.
		const int num_tracks = 100;
		// TODO: How to get query from search uri?
		int pre_length = strlen("spotify:search:");
		if (strlen(uri) < pre_length)
			return command_error(client, ERROR_SEARCH_FAILED, "Failed to create search request. invalid uri.");

			sp_search * search = sp_search_create(session, uri+pre_length, 0, num_tracks, 0, 0, 0, 0, 0, 0, SP_SEARCH_STANDARD, &search_complete, client);
		if (!search)
			return command_error(client, ERROR_SEARCH_FAILED, "Failed to create search request.");
			
		playback_set(kPlayingSearch, search);
	}
	sp_search * search = playback()->search;
	
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	
	// If not loaded? wait for callback.
	while(!sp_search_is_loaded(search)) {
		pthread_mutex_unlock(client->api_mutex);
		// Timeout 1 sec.
		ts.tv_sec += 1;
		sem_timedwait(&client->sem, &ts);
		pthread_mutex_lock(client->api_mutex);
	}
	
	sp_error err = sp_search_error(search);
	if (err != SP_ERROR_OK) {
		sp_search_release(search);
		return command_error(client, ERROR_SEARCH_FAILED, sp_error_message(err));
	}
	
	int num_tracks = sp_search_num_tracks(search);
	
	// Minimum 0 and maximum num_tracks.
	if (index < 0)
		index = 0;
	else if (index >= num_tracks)
		index = num_tracks;
	
	// Free old track, if any.
	if (is_same && playback()->current_track) {
		sp_track_release(playback()->current_track);
		playback()->current_track = NULL;
	}
	
	if (!is_same)
		generate_mapping(num_tracks);
	
	// Try to start playback.
	load_and_play_next_available_track(session, index, 0);
	playback()->loading = 0;
	
	return command_status(session, client);
}
