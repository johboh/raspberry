#ifndef __SCROBBLE_H__
#define __SCROBBLE_H__

#include <libspotify/api.h>

#include "state.h"
#include "client.h"
#include "error.h"

int command_lastfm_login(sp_session *session, client_t * client, const char * username, const char * password);
int command_lastfm_enable(sp_session *session, client_t * client, uint8_t enable);
int command_lastfm_logout(sp_session *session, client_t * client);

void scrobble_init(void);
void scrobble_track_start(sp_track *track);
void scrobble_track_paused(sp_track *track, uint32_t paused);
void scrobble_track_end(sp_track *track);

uint32_t scrobble_lastfm_enabled(void);
void scrobble_lastfm_set_enabled(uint32_t enabled);
char * scrobble_lastfm_username(void);
void scrobble_lastfm_set_username(const char * username);

#endif // __SCROBBLE_H__
