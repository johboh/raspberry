#include "commands.h"
#include "error.h"
#include "equal.h"
#include "metadata.h"
#include <semaphore.h>
#include "state.h"
#include <string.h>

static void playlist_state_changed(sp_playlist *pl, void *userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

static void playlist_metadata_updated(sp_session *session, void *args) {
	client_t *client = (client_t *)args;
	sem_post(&client->sem);
}

static int playlist_loaded_with_metadata(sp_playlist * playlist, uint8_t no_tracks) {
	if (playlist == NULL || !sp_playlist_is_loaded(playlist))
		return 0;
		
	if (no_tracks)
		return 1;
		
	// Not all tracks loaded? Wait some more.
	int num_tracks = sp_playlist_num_tracks(playlist);
	uint32_t i = 0;
	for(i = 0; i < num_tracks; i++) {
		sp_track *track = sp_playlist_track(playlist, i);
		if (!sp_track_is_loaded(track))
			return 0;
	}
	
	// All loaded.
	return 1;
}

int command_browse_playlist(sp_session *session, client_t * client, sp_link *link, sp_playlist *playlist, uint8_t no_tracks) {

	// Eiter a link or a playlist (inbox/own starred)

	if (!link && !playlist)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type playlist (null).");
	
	if (!playlist && sp_link_type(link) != SP_LINKTYPE_PLAYLIST && sp_link_type(link) != SP_LINKTYPE_STARRED) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type playlist.");
	}

	if (link) {
		playlist = sp_playlist_create(session, link);
		sp_link_release(link);
	} 
	if (!playlist)
		return command_error(client, ERROR_RESOURCE_NOT_FOUND, "The playlist does not exist (null).");

	uint8_t isCurrentContext = isCurrentPlaylist(NULL, playlist);

	// register callbacks.
	sp_playlist_callbacks callbacks;
	memset(&callbacks, 0, sizeof(sp_playlist_callbacks));
	callbacks.playlist_state_changed = &playlist_state_changed;
	sp_playlist_add_callbacks(playlist, &callbacks, client);

	// If not loaded? add callback.
	if (!playlist_loaded_with_metadata(playlist, no_tracks) && !no_tracks)
		metadata_updated_add_callback(&playlist_metadata_updated, client);

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);

	// If not loaded? wait for callback.
	while(!playlist_loaded_with_metadata(playlist, no_tracks)) {
		pthread_mutex_unlock(client->api_mutex);
		// Timeout 1 sec.
		ts.tv_sec += 1;
		sem_timedwait(&client->sem, &ts);
		pthread_mutex_lock(client->api_mutex);
	}

	// Remove callbacks.
	metadata_updated_remove_callback(&playlist_metadata_updated, client);
	sp_playlist_remove_callbacks(playlist, &callbacks, client);
	
	// Get subscribers.
	sp_playlist_update_subscribers(session, playlist);
	
	// Playlist metadata.
	json_t * result = playlist_to_json(session, playlist);
	
	// sp_playlist_num_tracks // sp_playlist_track, track name, uri, artist, artist uri, album, album uri.
	json_t *tracks = json_array();
	uint32_t i = 0;
	for(i = 0; i < sp_playlist_num_tracks(playlist) && !no_tracks; i++) {
		sp_track *track = sp_playlist_track(playlist, i);
		json_t * result_track = NULL;
		// In inbox, a track may be something else..
		if (sp_track_is_placeholder(track)) {
			sp_link * link = sp_link_create_from_track(track, 0);
			sp_linktype type = sp_link_type(link);
			switch(type) {
				case SP_LINKTYPE_ALBUM:
					result_track = album_to_json(sp_link_as_album(link), 1);
					json_object_set_new(result_track, "type", json_string("album"));
				break;
				case SP_LINKTYPE_ARTIST:
					result_track = artist_to_json(sp_link_as_artist(link));
					json_object_set_new(result_track, "type", json_string("artist"));
				break;
				case SP_LINKTYPE_TRACK:
					result_track = track_to_json(session, sp_link_as_track(link), 1, 1);
					json_object_set_new(result_track, "type", json_string("track"));
				break;
				case SP_LINKTYPE_PLAYLIST:
				case SP_LINKTYPE_STARRED: {
					sp_playlist *local = sp_playlist_create(session, link);
					result_track = playlist_to_json(session, local);
					sp_playlist_release(local);
					json_object_set_new(result_track, "type", json_string("playlist"));
				break;
				}
				default:
					break;
			}
			sp_link_release(link);
		} else {
			result_track = track_to_json(session, track, 1, 1);
			json_object_set_new(result_track, "type", json_string("track"));
		}
		
		// If we have a valid "track".
		if (result_track) {
			json_object_set_new(result_track, "index", json_integer(i));
			if (isCurrentContext && playback()->current_index == i)
				json_object_set_new(result_track, "is_playing", json_integer(1));
			json_object_set_new(result_track, "seen", json_integer(sp_playlist_track_seen(playlist, i)));
			json_object_set_new(result_track, "create_time", json_integer(sp_playlist_track_create_time(playlist, i)));
			
			sp_user * creator = sp_playlist_track_creator(playlist, i);
			if (creator) {
				json_object_set_new(result_track, "creator_username", json_string(sp_user_canonical_name(creator)));
				json_object_set_new(result_track, "creator_name", json_string(sp_user_display_name(creator)));
			}
			
			const char * message = sp_playlist_track_message(playlist, i);
			if (message)
				json_object_set_new(result_track, "message", json_string(message));
		
			json_array_append_new(tracks, result_track);
		}
	}
	json_object_set_new(result, "tracks", tracks);

	sp_playlist_release(playlist);
	return command_success(client, result);
}
