#include "scrobble.h"
#include <string.h>
#include <time.h>
#include "settings.h"

static char g_username[256];

static void flush_settings(sp_session *session) {
	// Save settings.
	sp_user * user = sp_session_user(session);
	if (user)
		settings_save(sp_user_canonical_name(user));
}

int command_lastfm_login(sp_session *session, client_t * client, const char * username, const char * password) {
	sp_session_set_social_credentials(session, SP_SOCIAL_PROVIDER_LASTFM, username, password);
	command_lastfm_enable(session, client, 1);
	// TODO: Handle login error, scrobble error callback.
	
	strncpy(g_username, username, sizeof(g_username));
	flush_settings(session);
	return command_error(client, ERROR_SUCCESS, "Login success.");
}

int command_lastfm_enable(sp_session *session, client_t * client, uint8_t enable) {
	sp_session_set_scrobbling(session, SP_SOCIAL_PROVIDER_LASTFM, enable ? SP_SCROBBLING_STATE_LOCAL_ENABLED : SP_SCROBBLING_STATE_LOCAL_DISABLED);
	// TODO: Handle login error, scrobble error callback.
	return command_error(client, ERROR_SUCCESS, "success.");
}

int command_lastfm_logout(sp_session *session, client_t * client) {
	sp_session_set_social_credentials(session, SP_SOCIAL_PROVIDER_LASTFM, "", "");
	command_lastfm_enable(session, client, 0);

	g_username[0] = '\0';
	flush_settings(session);
	return command_error(client, ERROR_SUCCESS, "Logout success.");
}

void scrobble_init() {
	g_username[0] = '\0';
}

char * scrobble_lastfm_username(void) {
	return g_username;
}

void scrobble_lastfm_set_username(const char * username) {
	strncpy(g_username, username, sizeof(g_username));
}