#include "commands.h"
#include "state.h"
#include "error.h"
#include "state.h"
#include "metadata.h"
#include "limits.h"
#include "status.h"

int command_history(sp_session *session, client_t * client, int limit) {
	json_t * result = json_object();

	json_t *tracks = json_array();
	int32_t i = 0;

	uint32_t count = UINT_MAX;

	if (limit >= 0) {
		count = limit;
	}

	// Print tracks from history, backwards. Latest first.
	for (i = history_num_tracks() - 1; i >= 0 && count > 0; --i) {
		queue_entry_t *entry = history_get_entry(i);
		// Dont add empty track.
		if (!entry || !entry->track) {
			continue;
		}
		json_t * result_track = track_to_json(session, entry->track, 1, 1);
		json_object_set_new(result_track, "index", json_integer(i));
		if (entry->metadata) {
			json_object_set_new(result_track, "metadata", json_string(entry->metadata));
		}
		if (entry->timestamp > 0) {
			json_object_set_new(result_track, "timestamp", json_integer(entry->timestamp));
		}
		json_array_append_new(tracks, result_track);
		count--;
	}

	json_object_set_new(result, "tracks", tracks);
	return command_success(client, result);
}