#include "commands.h"
#include "error.h"
#include "equal.h"
#include "metadata.h"
#include "state.h"
#include <semaphore.h>

static void album_browse_complete(sp_albumbrowse *result, void * userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

int command_browse_album(sp_session *session, client_t * client, sp_link *link) {

	if (!link)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type album (null).");
	
	if (sp_link_type(link) != SP_LINKTYPE_ALBUM) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type album.");
	}

	uint8_t isCurrentContext = isCurrentAlbum(link);

	sp_albumbrowse * browse = sp_albumbrowse_create(session, sp_link_as_album(link), &album_browse_complete, client);
	sp_link_release(link);
	if (!browse)
		return command_error(client, ERROR_BROWSE_FAILED, "Failed to create album browse request.");

	// If not loaded? wait for callback.
	while(sp_albumbrowse_error(browse) == SP_ERROR_IS_LOADING) {
		pthread_mutex_unlock(client->api_mutex);
		sem_wait(&client->sem);
		pthread_mutex_lock(client->api_mutex);
	}

	// Check for browse error, like non existing resource.
	sp_error err = sp_albumbrowse_error(browse);
	if (err != SP_ERROR_OK) {
		sp_albumbrowse_release(browse);
		if (err == SP_ERROR_OTHER_PERMANENT)
			return command_error(client, ERROR_RESOURCE_NOT_FOUND, sp_error_message(err));
		else
			return command_error(client, ERROR_BROWSE_FAILED, sp_error_message(err));
	}	

	// Album uri and name.
	json_t * result = album_to_json(sp_albumbrowse_album(browse), 0);
	// Copyrights
	json_t *copyrights = json_array();
	uint32_t i = 0;
	for(i = 0; i < sp_albumbrowse_num_copyrights(browse); i++)
		json_array_append_new(copyrights, json_string(sp_albumbrowse_copyright(browse, i)));
	json_object_set_new(result, "copyrights", copyrights);
	// Review
	json_object_set_new(result, "review", json_string(sp_albumbrowse_review(browse)));
	
	// Album artist uri and name.
	json_object_set_new(result, "artist", artist_to_json(sp_albumbrowse_artist(browse)));

	// sp_albumbrowse_num_tracks // sp_albumbrowse_track, track name, uri, artist, artist uri, album, album uri.
	json_t *tracks = json_array();
	for(i = 0; i < sp_albumbrowse_num_tracks(browse); i++) {
		sp_track *track = sp_albumbrowse_track(browse, i);
		json_t * result_track = track_to_json(session, track, 1, 0);
		json_object_set_new(result_track, "index", json_integer(i));
		if (isCurrentContext && playback()->current_index == i)
			json_object_set_new(result_track, "is_playing", json_integer(1));
		json_array_append_new(tracks, result_track);
	}
	json_object_set_new(result, "tracks", tracks);

	sp_albumbrowse_release(browse);
	return command_success(client, result);
}
