/**
 * Copyright (c) 2006-2010 Spotify Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 * This example application is the most minimal way to just play a spotify URI.
 *
 * This file is part of the libspotify examples suite.
 */
#include "main.h"

#include <errno.h>
#include <libgen.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include<sys/stat.h>
#include <signal.h>

#include <libspotify/api.h>

#include <jansson.h>
#include "audio.h"

#include "web_service.h"
#include "client.h"
#include "commands.h"
#include "state.h"
#include "settings.h"
#include "scrobble.h"

/* --- Data --- */
/// The application key is specific to each project, and allows Spotify
/// to produce statistics on how our service is used.
extern const uint8_t g_appkey[];
/// The size of the application key.
extern const size_t g_appkey_size;

/// The output queue for audo data
static audio_fifo_t g_audiofifo;
/// Synchronization mutex for the main thread
static pthread_mutex_t g_notify_mutex;
/// Synchronization condition variable for the main thread
static pthread_cond_t g_notify_cond;
/// Synchronization variable telling the main thread to process events
static int g_notify_do = 0;
/// Synchronization variable telling the main thread to process events
static int g_current_track_ended = 0;
/// The global session handle
static sp_session *g_sess;
/// API mutex.
static pthread_mutex_t g_api_mutex;
/// Playback position
static uint32_t g_position_ms;

void audio_flush() {
	audio_fifo_flush(&g_audiofifo);
}

void int_handler(int signum) {
	web_service_close();
	exit(signum);
}

void set_position(uint32_t position_ms) {
	g_position_ms = position_ms;
}

uint32_t get_position(void) {
	return g_position_ms;
}

/* ---------------------------  SESSION CALLBACKS  ------------------------- */
/**
 * This callback is called when an attempt to login has succeeded or failed.
 *
 * @sa sp_session_callbacks#logged_in
 */
static void logged_in(sp_session *sess, sp_error error) {
	logged_in_notify(sess, error);

	if (SP_ERROR_OK != error) {
		fprintf(stderr, "Login failed: %s\n", sp_error_message(error));
	} else {
		// Load settings on login success.
		sp_user * user = sp_session_user(sess);
		if (user) {
			settings_load(sp_user_canonical_name(user));
			settings_save(sp_user_canonical_name(user));
		}
	}
}
/**
 * This callback is called when an attempt to logout has succeeded.
 *
 * @sa sp_session_callbacks#logged_out
 */
static void logged_out(sp_session *sess) {
	logged_out_notify(sess);
}
/**
 * This callback is called from an internal libspotify thread to ask
 * us to reiterate the main loop.
 *
 * We notify the main thread using a condition variable and a protected variable.
 *
 * @sa sp_session_callbacks#notify_main_thread
 */
static void notify_main_thread(sp_session *sess) {
	pthread_mutex_lock(&g_notify_mutex);
	g_notify_do = 1;
	pthread_cond_signal(&g_notify_cond);
	pthread_mutex_unlock(&g_notify_mutex);
}

/**
 * This callback is used from libspotify whenever there is PCM data available.
 *
 * @sa sp_session_callbacks#music_delivery
 */
static int music_delivery(sp_session *sess, const sp_audioformat *format,
                          const void *frames, int num_frames) {
	audio_fifo_t *af = &g_audiofifo;
	audio_fifo_data_t *afd;
	size_t s;

	if (num_frames == 0)
		return 0; // Audio discontinuity, do nothing

	pthread_mutex_lock(&af->mutex);

	/* Buffer one second of audio */
	if (af->qlen > format->sample_rate) {
		pthread_mutex_unlock(&af->mutex);

		return 0;
	}

	s = num_frames * sizeof(int16_t) * format->channels;

	afd = malloc(sizeof(audio_fifo_data_t) + s);
	memcpy(afd->samples, frames, s);

	afd->nsamples = num_frames;

	afd->rate = format->sample_rate;
	afd->channels = format->channels;

	TAILQ_INSERT_TAIL(&af->q, afd, link);
	af->qlen += num_frames;

	pthread_cond_signal(&af->cond);
	pthread_mutex_unlock(&af->mutex);

	g_position_ms += (num_frames * 1000) / format->sample_rate;

	return num_frames;
}


/**
 * This callback is used from libspotify when the current track has ended
 *
 * @sa sp_session_callbacks#end_of_track
 */
static void end_of_track(sp_session *session) {
	pthread_mutex_lock(&g_notify_mutex);
	g_current_track_ended = 1;
	pthread_cond_signal(&g_notify_cond);
	pthread_mutex_unlock(&g_notify_mutex);
}

/**
 * Callback called when libspotify has new metadata available
 *
 * @sa sp_session_callbacks#metadata_updated
 */
static void metadata_updated(sp_session *sess) {
	metadata_updated_notify(sess);
}

/**
 * Notification that some other connection has started playing on this account.
 * Playback has been stopped.
 *
 * @sa sp_session_callbacks#play_token_lost
 */
static void play_token_lost(sp_session *session) {
	audio_flush();
	playback_set_current(playback()->current_track, playback()->current_index, 0);
	sp_session_player_play(session, 0);
}

static void log_message(sp_session *session, const char *msg) {
	puts(msg);
}

/**** DEBUG ****/
sp_error g_last_connection_error;
sp_error g_last_streaming_error;
char g_last_message_to_user[2048];
/**** END DEBUG ****/

static void connection_error(sp_session *session, sp_error error) {
	g_last_connection_error = error;
	printf("Connection error: %s\n", sp_error_message(g_last_connection_error));
}

static void message_to_user(sp_session *session, const char *message) {
	strncpy(g_last_message_to_user, message, sizeof(g_last_message_to_user));
	printf("Message to user: %s\n", message);
}

static void streaming_error(sp_session *session, sp_error error) {
	g_last_streaming_error = error;
	printf("Streaming error: %s\n", sp_error_message(g_last_streaming_error));
}

/**
 * The session callbacks
 */
static sp_session_callbacks session_callbacks = {
	.logged_in = &logged_in,
	.logged_out = &logged_out,
	.notify_main_thread = &notify_main_thread,
	.music_delivery = &music_delivery,
	.metadata_updated = &metadata_updated,
	.play_token_lost = &play_token_lost,
	.log_message = &log_message,
	.end_of_track = &end_of_track,
	.connection_error = &connection_error,
	.message_to_user = &message_to_user,
	.streaming_error = &streaming_error,
};

/**
 * The session configuration. Note that application_key_size is an
 * external, so we set it in main() instead.
 */
static sp_session_config spconfig = {
	.api_version = SPOTIFY_API_VERSION,
	.cache_location = "",
	.settings_location = "/root/.sp_server",
	.application_key = g_appkey,
	.application_key_size = 0, // Set in main()
	.user_agent = "sp_server",
	.callbacks = &session_callbacks,
	NULL,
};

/* -------------------------  END SESSION CALLBACKS  ----------------------- */

/**
 * Show usage information
 *
 * @param  progname  The program name
 */
static void usage(const char *progname) {
	fprintf(stderr, "usage: %s -d server_port_number -s settings_directory_path [-u <username> -p <password>]\n", progname);
}

static void process_client(client_t * client) {
	pthread_mutex_lock(&g_api_mutex);
	client->api_mutex = &g_api_mutex;
	command_parse(g_sess, client);
	pthread_mutex_unlock(&g_api_mutex);
}

int main(int argc, char **argv) {
	sp_session *sp;
	sp_error err;
	int next_timeout = 0;
	const char *username      = NULL;
	const char *password      = NULL;
	const char *settings_path = NULL;
	int opt;
	int port = -1;

	while ((opt = getopt(argc, argv, "u:p:d:l:s:")) != EOF) {
		switch (opt) {
		case 'u':
			username = optarg;
			break;
		case 'p':
			password = optarg;
			break;
		case 'd':
			port = atoi(optarg);
			break;
		case 's':
			settings_path = optarg;
			break;
		default:
			exit(1);
		}
	}

	if ((username && !password) || (!username && password) || port < 1 || port > 65535 || !settings_path) {
		usage(basename(argv[0]));
		exit(1);
	}

	// Check that settings path exists.
	struct stat buf;
	if (stat(settings_path, &buf) != 0) {
		printf("Settings path does not exists.\n");
		usage(basename(argv[0]));
		exit(1);
	}

	// Register signal and signal handler
	signal(SIGINT, int_handler);
	signal(SIGTERM, int_handler);

	scrobble_init();
	settings_init(settings_path);

	audio_init(&g_audiofifo);

	/* Create session */
	spconfig.application_key_size = g_appkey_size;
	
	err = sp_session_create(&spconfig, &sp);

	if (SP_ERROR_OK != err) {
		fprintf(stderr, "Unable to create session: %s\n",
			sp_error_message(err));
		exit(1);
	}
	g_sess = sp;

	// Init state.
	state_init();

	// Seed random.
	srand(time(NULL));
	
	pthread_mutex_init(&g_api_mutex, NULL);
	pthread_mutex_init(&g_notify_mutex, NULL);
	pthread_cond_init(&g_notify_cond, NULL);
	
	// Start web service.
	if (web_service_init(port, &process_client) < 0) {
		exit(1);
	}
	
	if (username && password)
		sp_session_login(sp, username, password, 0, NULL);
	else
		sp_session_relogin(sp);

	struct timespec ts;
	for (;;) {
		pthread_mutex_lock(&g_notify_mutex);
		
		if (next_timeout == 0) {
			while(!g_notify_do && !g_current_track_ended)
				pthread_cond_wait(&g_notify_cond, &g_notify_mutex);
		} else {
			clock_gettime(CLOCK_REALTIME, &ts);

			ts.tv_sec += next_timeout / 1000;
			ts.tv_nsec += (next_timeout % 1000) * 1000000;
			if(ts.tv_nsec > 1000000000) {
				ts.tv_sec++;
				ts.tv_nsec -= 1000000000;
			}
			
			while(!g_notify_do && !g_current_track_ended) {
				if (pthread_cond_timedwait(&g_notify_cond, &g_notify_mutex, &ts) == ETIMEDOUT)
					break;
			}
		}
		
		int track_ended = g_current_track_ended;
		g_notify_do = 0;
		g_current_track_ended = 0;
		pthread_mutex_unlock(&g_notify_mutex);
		
		pthread_mutex_lock(&g_api_mutex);
		if (track_ended)
			current_track_ended(g_sess, kEndOfPlayback);

		do {
			sp_session_process_events(sp, &next_timeout);
		} while (next_timeout == 0);
		
		pthread_mutex_unlock(&g_api_mutex);
	}

	return EXIT_SUCCESS;
}
