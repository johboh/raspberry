#include "commands.h"
#include "state.h"
#include "error.h"
#include "equal.h"
#include <string.h>

static void playlist_metadata_updated(sp_session *session, void *userdata) {
	// Check type.
	if (playback()->context != kPlayingPlaylist)
		return;

	// Is this our playlist?
	sp_playlist *playlist = (sp_playlist *)userdata;
	if (!playlist || playlist != playback()->playlist)
		return;

	int num_tracks = sp_playlist_num_tracks(playlist);
	
	// Regenerate mapping if new or track count changed.
	if (get_mapping_length() != num_tracks || playback()->si.new_browse) {
		generate_mapping(num_tracks);
		playback()->si.new_browse = 0;
	}		
	
	// No tracks?
	if (num_tracks == 0)
		return;

	// Or track not available or already started playback?
	if (num_tracks < playback()->si.index || playback()->si.index == -1)
		return;

	// Get track we want to play, create track.
	sp_track *track = sp_playlist_track(playlist, get_mapping(playback()->si.index));
	
	// Is track loaded?
	if (!track || !sp_track_is_loaded(track))
		return;
	
	playback()->loading = 0;
	
	// Already playing this track?
	if (playback()->current_track == track)
		return;

	// Try to start playback.
	if (load_and_play_next_available_track(session, playback()->si.index, 0) != -1) {
		// Clear index, never start playback here again.
		playback()->si.index = -1;
	}
}

static void playlist_tracks_added(sp_playlist *pl, sp_track * const *tracks, int num_tracks, int position, void *userdata) {
	playlist_metadata_updated(playback()->si.session, pl);
}

static void playlist_tracks_removed(sp_playlist *pl, const int *tracks, int num_tracks, void *userdata) {
	playlist_metadata_updated(playback()->si.session, pl);
}

static void playlist_tracks_moved(sp_playlist *pl, const int *tracks, int num_tracks, int new_position, void *userdata) {
	playlist_metadata_updated(playback()->si.session, pl);
}

static sp_playlist_callbacks pl_callbacks = {
    .tracks_added = &playlist_tracks_added,
    .tracks_removed = &playlist_tracks_removed,
    .tracks_moved = &playlist_tracks_moved,
};

int command_play_playlist(sp_session *session, client_t * client, sp_link *link, sp_playlist *playlist, uint32_t index) {

	// Eiter a link or a playlist (inbox/own starred)

	if (!link && !playlist)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type playlist (null).");
	
	if (!playlist && sp_link_type(link) != SP_LINKTYPE_PLAYLIST && sp_link_type(link) != SP_LINKTYPE_STARRED) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type playlist.");
	}

	// Minimum 0.
	index = index < 0 ? 0 : index;

	// We have eiter link or playlist here.
	// Free up resource if switching to new playback type and/or playlist.
	uint8_t is_same = isCurrentPlaylist(link, playlist);

	if (!is_same)
		stop_and_release_playback(session);

	playback()->si.index = index;
	playback()->si.session = session;
	playback()->si.new_browse = !is_same;
	playback()->loading = 1;
	
	if (!is_same || !playback()->playlist) {
		if (link) {
			playlist = sp_playlist_create(session, link);
			sp_link_release(link);
		}
		if (!playlist)
			return command_error(client, ERROR_RESOURCE_NOT_FOUND, "The playlist does not exist (null).");
		sp_playlist_add_callbacks(playlist, &pl_callbacks, playlist);
		metadata_updated_add_callback(playlist_metadata_updated, playlist);
		playback_set(kPlayingPlaylist, playlist);
		playback()->playlist_metadata_updated = &playlist_metadata_updated;
		playback()->playlist_callbacks = &pl_callbacks;
		playlist_metadata_updated(session, playback()->playlist);
	} else {
		if (link)
			sp_link_release(link);
		playlist_metadata_updated(session, playback()->playlist);
	}

	return command_status(session, client);
}
