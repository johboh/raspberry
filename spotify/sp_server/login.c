#include "commands.h"
#include <semaphore.h>
#include "state.h"
#include "error.h"

typedef struct {
	client_t *client;
	sp_error error;
} login_response_t;

static void logged_in_complete(sp_session *session, sp_error error, void *args) {
	login_response_t *response = (login_response_t *)args;
	response->error = error;
	sem_post(&response->client->sem);
}

int command_login(sp_session *session, client_t * client, const char * username, const char * password, int remember_me) {
	login_response_t *response = (login_response_t*)malloc(sizeof(login_response_t));
	response->client = client;
	logged_in_set_callback(&logged_in_complete, response);
	sp_session_login(session, username, password, remember_me, NULL);

	pthread_mutex_unlock(client->api_mutex);
	sem_wait(&client->sem);
	pthread_mutex_lock(client->api_mutex);
	logged_in_clear_callback();

	sp_error error = response->error;
	free(response);

	// Return error or success
	return command_error_sp(client, error);
}