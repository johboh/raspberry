#ifndef __COMMANDS_H__
#define __COMMANDS_H__

#include <stdint.h>
#include <libspotify/api.h>
#include "client.h"

#define COMMAND_API_VERSION 1

int command_parse(sp_session *session, client_t *client);

// JSON
int command_play(sp_session *session, client_t *client);
int command_play_track(sp_session *session, client_t *client, sp_link *link, uint32_t offset_ms, char * next_action);
int command_play_album(sp_session *session, client_t *client, sp_link *link, uint32_t index);
int command_play_search(sp_session *session, client_t *client, sp_link *link, uint32_t index);
int command_play_playlist(sp_session *session, client_t *client, sp_link *link, sp_playlist *playlist, uint32_t index);
int command_queue(sp_session *session, client_t *client, int explicit_limit, int future_limit);
int command_queue_add(sp_session *session, client_t * client, sp_link * link, uint32_t index, char * metadata);
int command_queue_remove(sp_session *session, client_t * client, int index);
int command_queue_clear(sp_session *session, client_t * client);
int command_pause(sp_session *session, client_t *client);
int command_next(sp_session *session, client_t *client);
int command_shuffle(sp_session *session, client_t *client, uint8_t enable);
int command_repeat(sp_session *session, client_t *client, uint8_t enable);
int command_previous(sp_session *session, client_t *client, uint8_t restart);
int command_status(sp_session *session, client_t *client);
int command_debug(sp_session *session, client_t *client, int clear);
int command_seek(sp_session *session, client_t *client, uint32_t offset_ms);
int command_browse_album(sp_session *session, client_t *client, sp_link *link);
int command_browse_playlist(sp_session *session, client_t *client, sp_link *link, sp_playlist *playlist, uint8_t no_tracks);
int command_browse_playlists(sp_session *session, client_t *client, uint8_t flat, const char * root);
int command_browse_track(sp_session *session, client_t *client, sp_link *link);
int command_browse_artist(sp_session *session, client_t *client, sp_link *link, sp_artistbrowse_type browse_type);
int command_search(sp_session *session, client_t *client, const char * query, uint32_t num_tracks, uint32_t num_albums, uint32_t num_artists, uint32_t num_playlists);
int command_login(sp_session *session, client_t *client, const char * username, const char * password, int remember_me);
int command_logout(sp_session *session, client_t *client);
int command_volume(sp_session *session, client_t * client, int volume);
int command_volume_normalization(sp_session *session, client_t * client, int on);
int command_bitrate(sp_session *session, client_t * client, int bitrate);
int command_history(sp_session *session, client_t * client, int limit);

// TEXT / BINARY
int command_docs(sp_session *session, client_t * client, const char * path);
int command_image(sp_session *session, client_t *client, sp_link * link, sp_image_size image_size);

#endif // __COMMANDS_H__
