	// Ajax handling.
	// Create Ajax object
	function createRequestObject()
	{
			var ro;
			var browser = navigator.appName;
			if(browser == "Microsoft Internet Explorer")
			{
					ro = new ActiveXObject("Microsoft.XMLHTTP");
			}
			else
			{
					ro = new XMLHttpRequest();
			}
			return ro;
	}

	var http = createRequestObject(); // for ajax
	var busy = false;
	var currentTarget = "";
	var currentEvalTarget = "";

	// When we have a response.
	function onAjaxResponse()
	{
		if (http.readyState == 4) // ok?
		{
			if (http.status == 200) // ok?
			{
				var result = http.responseText;
				document.getElementById(currentTarget).innerHTML = result; // set result.
				eval(document.getElementById(currentEvalTarget).innerHTML.replace(/&amp;/g,'&'));
			}
			busy = false;
		}
	}	
	
	// When we want to do a POST request.
	function ajaxPost(url, target, form)
	{
		if (true || !busy)
		{
			currentTarget = target;
			http.open("POST", url, true);
			http.onreadystatechange = onAjaxResponse;
			http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			var str = createQuery(form);
			http.send(str);
			busy = true;
		}
	}
  
	// When we want to do a GET request.
	function ajaxRequest(url, target, evalTarget)
	{
		if (true || !busy)
		{
			currentTarget = target;
			currentEvalTarget = evalTarget;
			http.open("GET", url, true);
			http.onreadystatechange = onAjaxResponse;
			http.send(null);
			busy = true;
		}
	}
	
	// create a query out of a form.
	function createQuery(form)
	{
		var elements = form.elements;
		var pairs = new Array();

		// For each element in form.
		for (var i = 0; i < elements.length; i++) 
		{
			// If we have a name and value...
			if ((name = elements[i].name) && (value = elements[i].value))
			{
				// Push to array.
				pairs.push(name + "=" + encodeURIComponent(value));
			}
		}

		// Joing them togheter.
		return pairs.join("&");
	}