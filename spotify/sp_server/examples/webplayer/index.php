<html>
<head>
<title>Example webplayer</title>
<script type="text/javascript" src="ajax.js"></script>
</head>
<body>
<script language="javascript">
	function browse() {
		var uri = document.getElementById('uri').value;
		document.getElementById('result').innerHTML = 'Browsing, please wait!';
		ajaxRequest('browse.php?uri='+uri, 'result');
	}
	function login(form) {
		document.getElementById('result').innerHTML = 'Logging in, please wait!';
		ajaxPost('login.php', 'result', form);
	}
	function play(uri, index) {
		document.getElementById('result').innerHTML = 'Playing, please wait!';
		ajaxRequest('status.php?play=1&uri='+uri+'&index='+index, 'result');
	}
	function go(url) {
		ajaxRequest(url, 'result');
	}
</script>
Input playlist uri: <input type="text" name="uri" id="uri" size="60" value="spotify:user:topsify:playlist:563hgiea4oqroeE0LXMnor"> <input type="button" name="browse" value="browse" onClick="browse();"><br>
<div id="result">
<?php include('status.php'); ?>
</div>
</body>