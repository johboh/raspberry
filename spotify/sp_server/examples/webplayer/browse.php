<?php 
	require_once("config.php");

	$str = file_get_contents("http://$host:$port/$api_version/{$_GET['uri']}");
	$json = json_decode($str);
	if ($json && $json->error->code == 1005) {
		echo "You must login first.<br>";
		echo "<form id=\"login_frm\" action=\"#\">";
		echo "Username: <input type=\"text\" name=\"username\" value=\"\"><br>";
		echo "Password: <input type=\"password\" name=\"password\" value=\"\"><br>";
		echo "<input type=\"button\" name=\"Login\" value=\"Login\" onClick=\"login(document.getElementById('login_frm'));\"><br>";
		echo "</form>";
		exit;
	} else if ($json && $json->error->code != 1) {
		print_r($json);
	} else if ($json) {
		echo "Name: {$json->name}<br>";
		echo "Tracks: {$json->num_tracks}<br>";
		foreach($json->tracks as $track) {
			echo "<a href=\"javascript:void(0);\" onClick=\"play('{$json->uri}', {$track->index});\">#{$track->index} {$track->name} - {$track->artist->name}</a><br>";
		}
	}
?>