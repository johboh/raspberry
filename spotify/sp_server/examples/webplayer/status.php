<?php 
	require_once("config.php");

	if ($_GET['play'] && isset($_GET['uri'])) {
		$str = file_get_contents("http://$host:$port/$api_version/{$_GET['uri']}/play".(isset($_GET['index']) ? "/{$_GET['index']}": ""));
	} else if (isset($_GET['pause']) && $_GET['pause'] == 1) {
		$str = file_get_contents("http://$host:$port/$api_version/pause");
	} else if (isset($_GET['pause']) && $_GET['pause'] == 0) {
		$str = file_get_contents("http://$host:$port/$api_version/play");
	} else if (isset($_GET['previous'])) {
		$str = file_get_contents("http://$host:$port/$api_version/previous");
	} else if (isset($_GET['next'])) {
		$str = file_get_contents("http://$host:$port/$api_version/next");
	} else {
		$str = file_get_contents("http://$host:$port/$api_version/status");
	}
	$json = json_decode($str);
	echo "<br><img width=\"128\" height=\"128\" src=\"image.php?uri={$json->track->album->uri}\"><br>";
	echo "Playing: {$json->playing}<br>";
	echo "Context: {$json->context}<br>";
	echo "Index: {$json->index}<br>";
	echo "Loading: {$json->loading}<br>";
	echo "Track: {$json->track->name} - {$json->track->artist->name}<br>";
	echo "<a href=\"javascript:void(0);\" onClick=\"go('status.php');\">refresh</a> - <a href=\"javascript:void(0);\" onClick=\"go('status.php?previous=1');\">previous</a> - <a href=\"javascript:void(0);\" onClick=\"go('status.php?pause={$json->playing}');\">play/pause</a> - <a href=\"javascript:void(0);\" onClick=\"go('status.php?next=1');\">next</a>";
?>