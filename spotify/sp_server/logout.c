#include "commands.h"
#include "state.h"
#include "error.h"
#include "settings.h"

void logged_out_complete(sp_session *session, void *args) {
	client_t *client = (client_t *)args;
	sem_post(&client->sem);
}

int command_logout(sp_session *session, client_t * client) {

	// Save settings for current user.
	sp_user * user = sp_session_user(session);
	if (user)
		settings_save(sp_user_canonical_name(user));

	logged_out_set_callback(&logged_out_complete, client);
	sp_session_logout(session);

	pthread_mutex_unlock(client->api_mutex);
	sem_wait(&client->sem);
	pthread_mutex_lock(client->api_mutex);
	logged_out_clear_callback();

	return command_error(client, ERROR_SUCCESS, "Logout success.");
}
