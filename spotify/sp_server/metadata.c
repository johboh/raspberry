#include "metadata.h"
#include <string.h>

json_t * track_to_json(sp_session * session, sp_track * track, uint8_t include_artist, uint8_t include_album) {
	if (!track)
		return NULL;

	json_t *obj = json_object();
	char buffer[256];
	sp_link * link = sp_link_create_from_track(track, 0);
	sp_link_as_string(link, buffer, sizeof(buffer));
	sp_link_release(link);
	json_object_set_new(obj, "uri", json_string(buffer));
	json_object_set_new(obj, "name", json_string(sp_track_name(track)));
	
	// Availability
	switch(sp_track_get_availability(session, track)) {
		case SP_TRACK_AVAILABILITY_UNAVAILABLE:
			json_object_set_new(obj, "availability", json_string("unavailable"));
			break;
		case SP_TRACK_AVAILABILITY_AVAILABLE :
			json_object_set_new(obj, "availability", json_string("available"));
			break;
		case SP_TRACK_AVAILABILITY_NOT_STREAMABLE :
			json_object_set_new(obj, "availability", json_string("not_streamable"));
			break;
		case SP_TRACK_AVAILABILITY_BANNED_BY_ARTIST :
			json_object_set_new(obj, "availability", json_string("banned_by_artist"));
			break;
	}
	
	json_object_set_new(obj, "starred", json_integer(sp_track_is_starred(session, track)));
	json_object_set_new(obj, "duration", json_integer(sp_track_duration(track)));
	json_object_set_new(obj, "popularity", json_integer(sp_track_popularity(track)));
	
	if (include_artist) {
		json_object_set_new(obj, "artist", artist_to_json(sp_track_artist(track, 0)));
		// Include all artists for this track. It can be many.
		if (sp_track_num_artists(track) > 1) {
			json_t *artists = json_array();
			uint32_t i = 0;
			for (i = 0; i < sp_track_num_artists(track); i++)
				json_array_append_new(artists, artist_to_json(sp_track_artist(track, i)));
			json_object_set_new(obj, "artists", artists);
		}
	}
	
	if (include_album)
		json_object_set_new(obj, "album", album_to_json(sp_track_album(track), 0));
	
	return obj;
}

json_t * album_to_json(sp_album * album, uint8_t include_artist) {
	if (!album)
		return NULL;

	json_t *obj = json_object();
	char buffer[256];
	sp_link *link = sp_link_create_from_album(album);
	sp_link_as_string(link, buffer, sizeof(buffer));
	sp_link_release(link);
	json_object_set_new(obj, "uri", json_string(buffer));
	json_object_set_new(obj, "name", json_string(sp_album_name(album)));
	json_object_set_new(obj, "year", json_integer(sp_album_year(album)));
	
	// Album type.
	switch(sp_album_type(album)) {
		case SP_ALBUMTYPE_ALBUM:
			json_object_set_new(obj, "type", json_string("normal"));
			break;
		case SP_ALBUMTYPE_SINGLE:
			json_object_set_new(obj, "type", json_string("single"));
			break;
		case SP_ALBUMTYPE_COMPILATION:
			json_object_set_new(obj, "type", json_string("compilation"));
			break;
		default:
			json_object_set_new(obj, "type", json_string("unknown"));
			break;
	}
	
	// Availability
	if (sp_album_is_available(album))
		json_object_set_new(obj, "availability", json_string("available"));
	else
		json_object_set_new(obj, "availability", json_string("unavailable"));
	
	if (include_artist)
		json_object_set_new(obj, "artist", artist_to_json(sp_album_artist(album)));
	
	return obj;
}

json_t * artist_to_json(sp_artist * artist) {
	if (!artist)
		return NULL;

	json_t *obj = json_object();
	char buffer[256];
	sp_link *link = sp_link_create_from_artist(artist);
	sp_link_as_string(link, buffer, sizeof(buffer));
	sp_link_release(link);
	json_object_set_new(obj, "uri", json_string(buffer));
	json_object_set_new(obj, "name", json_string(sp_artist_name(artist)));
	return obj;
}

json_t * playlist_to_json(sp_session * session, sp_playlist * playlist) {
	if (!playlist)
		return NULL;
		
	json_t *obj = json_object();
	char buffer[512];
	sp_link *link = sp_link_create_from_playlist(playlist);
	if (link) {
		sp_link_as_string(link, buffer, sizeof(buffer));
		sp_link_release(link);
		json_object_set_new(obj, "uri", json_string(buffer));
	}
	if (sp_playlist_name(playlist))
		json_object_set_new(obj, "name", json_string(sp_playlist_name(playlist)));
	json_object_set_new(obj, "num_tracks", json_integer(sp_playlist_num_tracks(playlist)));
	json_object_set_new(obj, "collaborative", json_integer(sp_playlist_is_collaborative(playlist)));
	json_object_set_new(obj, "num_subscribers", json_integer(sp_playlist_num_subscribers(playlist)));
	json_object_set_new(obj, "type", json_string("playlist"));

	sp_user * user = sp_playlist_owner(playlist);
	sp_user * current_user = sp_session_user(session);
	if (user) {
		json_object_set_new(obj, "owner_username", json_string(sp_user_canonical_name(user)));
		json_object_set_new(obj, "owner_name", json_string(sp_user_display_name(user)));
	}
	if(user && current_user) {
		json_object_set_new(obj, "is_own", json_integer(!strcmp(sp_user_canonical_name(user), sp_user_canonical_name(current_user))));
	} else {
		json_object_set_new(obj, "is_own", json_integer(0));
	}
	const char * description = sp_playlist_get_description(playlist);
	if (description)
		json_object_set_new(obj, "description", json_string(description));
	return obj;
}
