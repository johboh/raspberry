#ifndef __WEB_SERVICE_H__
#define __WEB_SERVICE_H__

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "client.h"

int web_service_init(uint16_t port, void (*process_client)(client_t * client));
void web_service_close(void);

#endif // __WEB_SERVICE_H__