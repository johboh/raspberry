#include "playlists_actions.h"
#include "error.h"
#include "metadata.h"
#include <string.h>

int playlists_action(sp_session *session, client_t * client, char **args, uint32_t num_args) {

	sp_playlistcontainer *pc = sp_session_playlistcontainer(session);
	if (!pc)
		return command_error(client, ERROR_BROWSE_FAILED, "Failed to create playlist container.");
	sp_playlistcontainer_add_ref(pc);

	// Create new playlist.
	if (num_args == 2 && !strcmp("create", args[0])) {
		// TODO: Maybe check for trim(str).length() != 0 and str.length() < 256.
		sp_playlist * playlist = sp_playlistcontainer_add_new_playlist(pc, args[1]);
		sp_playlistcontainer_release(pc);
		if (playlist) {
			json_t * result = playlist_to_json(session, playlist);
			return command_success(client, result);
		} else {
			return command_error(client, ERROR_UNKNOWN, "Unable to create playlist.");
		}
	}
	
	// Add existing playlist to root.
	if (num_args == 2 && !strcmp("add", args[0])) {
		// Create and validate link.
		sp_link *link = sp_link_create_from_string(args[1]);
		if (!link) {
			sp_playlistcontainer_release(pc);
			return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type playlist (null).");
		}
	
		if (sp_link_type(link) != SP_LINKTYPE_PLAYLIST && sp_link_type(link) != SP_LINKTYPE_STARRED) {
			sp_link_release(link);
			sp_playlistcontainer_release(pc);
			return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type playlist.");
		}
		
		// Add playlist.
		sp_playlist * playlist = sp_playlistcontainer_add_playlist(pc, link);
		sp_playlistcontainer_release(pc);
		sp_link_release(link);
		if (playlist) {
			json_t * result = playlist_to_json(session, playlist);
			return command_success(client, result);
		} else {
			return command_error(client, ERROR_UNKNOWN, "Unable to add playlist.");
		}
	}

	// Remove playlist (unsubscribe/delete).
	if (num_args == 2 && !strcmp("remove", args[0])) {
		sp_error error = sp_playlistcontainer_remove_playlist(pc, atoi(args[1]));
		sp_playlistcontainer_release(pc);
		if (error == SP_ERROR_INDEX_OUT_OF_RANGE)
			return command_error(client, ERROR_INDEX_OUT_OF_BOUNDS, "Index out out of bounds.");
		else if (error == SP_ERROR_OK)
			return command_success(client, NULL);
	}
	
	// Move playlist.
	if (num_args == 3 && !strcmp("move", args[0])) {
		sp_error error = sp_playlistcontainer_move_playlist(pc, atoi(args[1]), atoi(args[2]), 0);
		sp_playlistcontainer_release(pc);
		if (error == SP_ERROR_INDEX_OUT_OF_RANGE)
			return command_error(client, ERROR_INDEX_OUT_OF_BOUNDS, "Index out out of bounds.");
		else if (error == SP_ERROR_INVALID_INDATA)
			return command_error(client, ERROR_INVALID_INDATA, "Invalid data. Trying to move folder to itself?");
		else if (error == SP_ERROR_OK)
			return command_success(client, NULL);
	}
	
	sp_playlistcontainer_release(pc);
	return command_error(client, ERROR_UNKNOWN_COMMAND, "Command for playlists not implemented.");
}