#ifndef __METADATA_H__
#define __METADATA_H__

#include <stdint.h>
#include <libspotify/api.h>
#include <jansson.h>

json_t * track_to_json(sp_session * session, sp_track * track, uint8_t include_artist, uint8_t include_album);
json_t * album_to_json(sp_album * album, uint8_t include_artist);
json_t * artist_to_json(sp_artist * artist);
json_t * playlist_to_json(sp_session * session, sp_playlist * playlist);

#endif // __METADATA_H__
