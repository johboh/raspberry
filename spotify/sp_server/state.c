#include "state.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "scrobble.h"
#include "main.h"

/**
* Mapping between the track index in a
* playlist, album och search context,
* and the row that is currently playing.
* Index ranges from 0 to number of tracks
* in the context, and always coresponds
* to the unsorted, unshuffled list.
* Row ranges from 0 to number of tracks
* and corresponds to the shuffled index.
* uint32_t index = g_index_mapping[index];
*/
static uint32_t g_index_mapping[MAX_NUM_TRACKS] = {0};
static uint32_t g_mapping_length = 0;

static queue_entry_t g_queue[MAX_NUM_TRACKS];
static uint32_t g_queue_length = 0;

static queue_entry_t g_history[MAX_NUM_TRACKS];
static uint32_t g_history_length = 0;

int history_add(sp_track * track, char *metadata);

typedef struct {
	void (*callback)(sp_session *session, void * args);
	void *args;
} callback_t;

typedef struct {
	void (*callback)(sp_session *session, sp_error error, void * args);
	void *args;
} callback_werror_t;

#define MAX_NUM_CALLBACKS 100
static callback_t g_metadata_updated_callbacks[MAX_NUM_CALLBACKS];

/// If shuffle is enabled.
static uint8_t g_shuffle_enabled;

/// If repeat is enabled.
static uint8_t g_repeat_enabled;

static playback_t g_playback;
static playonce_t g_playonce;
static sp_track *g_playonce_track;
static uint32_t g_playonce_previous_offset_ms;
static uint8_t g_playonce_previous_state_was_playing;

static callback_werror_t g_logged_in_callback;

static callback_t g_logged_out_callback;

static sp_error g_last_error;

void state_init(void) {
	g_shuffle_enabled = 0;
	g_repeat_enabled = 0;
	playback_set(kPlayingNone, NULL);
	
	uint32_t i = 0;
	for(i = 0; i < MAX_NUM_CALLBACKS; i++)
		g_metadata_updated_callbacks[i].callback = NULL;
		
	g_logged_in_callback.callback = NULL;
	g_logged_out_callback.callback = NULL;

	g_playonce = kPlayonceNone;
	g_playonce_track = NULL;

	for (i = 0; i < MAX_NUM_TRACKS; ++i) {
		g_queue[i].metadata = NULL;
		g_queue[i].track = NULL;
		g_queue[i].timestamp = 0;
		g_history[i].metadata = NULL;
		g_history[i].track = NULL;
		g_history[i].timestamp = 0;
	}
}

int metadata_updated_add_callback(void (*callback)(sp_session *, void *), void *args) {
	// Add if not already in list.
	uint32_t i = 0;
	for(i = 0; i < MAX_NUM_CALLBACKS; i++) {
		callback_t *callback_entry = &g_metadata_updated_callbacks[i];
		if (callback_entry->callback == callback && callback_entry->args == args)
			return 0;
			
		// Found free entry.
		if (callback_entry->callback == NULL)
			break;
	}
	
	// No free.
	if (i >= MAX_NUM_CALLBACKS)
		return 0;
	
	callback_t callback_entry = {callback, args};
	g_metadata_updated_callbacks[i] = callback_entry;
	return 1;
}
int metadata_updated_remove_callback(void (*callback)(sp_session *, void *), void *args) {
	uint32_t i = 0;
	for(i = 0; i < MAX_NUM_CALLBACKS; i++) {
		callback_t *callback_entry = &g_metadata_updated_callbacks[i];
		if (callback_entry->callback == callback && callback_entry->args == args) {
			// Found.
			callback_entry->callback = NULL;
			callback_entry->args = NULL;
			return 1;
		}
	}
	
	return 0;
}

void metadata_updated_notify(sp_session *session) {
	uint32_t i = 0;
	for(i = 0; i < MAX_NUM_CALLBACKS; i++) {
		callback_t *callback_entry = &g_metadata_updated_callbacks[i];
		if (callback_entry->callback != NULL)
			callback_entry->callback(session, callback_entry->args);
	}
}

void repeat_set(uint8_t enable) {
	g_repeat_enabled = enable;
}

uint8_t repeat(void) {
	return g_repeat_enabled;
}

void shuffle_set(uint8_t enable) {
	uint8_t was_enabled = g_shuffle_enabled;
	g_shuffle_enabled = enable;
	// Regenerate mapping if off->on or off->on
	if ((!was_enabled && enable) || (was_enabled && !enable))
		generate_mapping(-1);
}

uint8_t shuffle(void) {
	return g_shuffle_enabled;
}

playback_t * playback() {
	return &g_playback;
}

void playback_set(context_t context, void * resource) {
	g_playback.context = context;
	g_playback.resource = resource;
	if (context == kPlayingNone) {
		g_playback.loading = 0;
		g_playback.paused = 1;
		g_playback.current_track = NULL;
		g_playback.current_index = 0;
		g_playback.resource = NULL;
		g_playback.si.index = 0;
		g_playback.si.session = NULL;
		g_playback.si.new_browse = 0;
		g_playback.playlist_metadata_updated = NULL;
		g_playback.playlist_callbacks = NULL;
	}
}

void playback_set_current(sp_track * current_track, uint32_t current_index, uint8_t playing) {
	g_playback.paused = !playing;
	g_playback.current_track = current_track;
	g_playback.current_index = current_index;
}

void logged_in_set_callback(void (*callback)(sp_session *, sp_error error, void *), void *args) {
	g_logged_in_callback.callback = callback;
	g_logged_in_callback.args = args;
}

void logged_in_clear_callback() {
	g_logged_in_callback.callback = NULL;
	g_logged_in_callback.args = NULL;
}

void logged_out_set_callback(void (*callback)(sp_session *, void *), void *args) {
	g_logged_out_callback.callback = callback;
	g_logged_out_callback.args = args;
}

void logged_out_clear_callback() {
	g_logged_out_callback.callback = NULL;
	g_logged_out_callback.args = NULL;
}

void logged_in_notify(sp_session *session, sp_error error) {
	if (g_logged_in_callback.callback)
		g_logged_in_callback.callback(session, error, g_logged_in_callback.args);
}

void logged_out_notify(sp_session *session) {
	if (g_logged_out_callback.callback)
		g_logged_out_callback.callback(session, g_logged_out_callback.args);
}

uint8_t is_logged_in(sp_session *session) {
	return sp_session_connectionstate(session) == SP_CONNECTION_STATE_LOGGED_IN || sp_session_connectionstate(session) == SP_CONNECTION_STATE_OFFLINE;
}

void stop_no_release(sp_session *session) {
	// Stop current playback
	audio_flush();
	sp_session_player_play(session, 0);
	sp_session_player_unload(session);
}

void stop_and_release_playback(sp_session *session) {
	stop_no_release(session);

	if (g_playonce != kPlayonceNone && g_playonce_track) {
		sp_track_release(g_playonce_track);
		g_playonce = kPlayonceNone;
		g_playonce_track = NULL;
	}
	
	// Release resources for current context.
	switch(playback()->context) {
		case kPlayingTrack:
			// Free track, not current track? the same.
			if (playback()->track)
				sp_track_release(playback()->track);
			break;
		case kPlayingAlbum:
			// TODO: remove callbacks, abort. Free album.
			if (playback()->album_browse)
				sp_albumbrowse_release(playback()->album_browse);
			break;
		case kPlayingPlaylist:
			// TODO: remove callbacks, abort. Free playlist.
			if (playback()->playlist) {
				if (playback()->playlist_callbacks)
					sp_playlist_remove_callbacks(playback()->playlist, playback()->playlist_callbacks, playback()->playlist);
				if (playback()->playlist_metadata_updated)
					metadata_updated_remove_callback(playback()->playlist_metadata_updated, playback()->playlist);
				sp_playlist_release(playback()->playlist);
			}
			break;
		case kPlayingSearch:
			// TODO: handle search.
			if (playback()->search)
				sp_search_release(playback()->search);
			break;
		default:
			break;
	}
	
	// Free current track.
	if (playback()->current_track)
		sp_track_release(playback()->current_track);
		
	playback_set(kPlayingNone, NULL); // No context anymore.
}

static int try_load_track(sp_session * session, sp_track *track, uint32_t index) {
	g_last_error = sp_session_player_load(session, track);
	if (g_last_error == SP_ERROR_OK) {
		playback_set_current(track, index, 1);
		sp_track_add_ref(playback()->current_track);
		set_position(0);
		sp_session_player_play(session, 1);
		return 0;
	}
	return -1;
}

int load_and_play_next_available_track(sp_session *session, uint32_t start_index, int previous) {
	int num_tracks = get_context_num_tracks();
	int i = 0;
	
	// Next or previous?
	if (!previous) {
		// Next. Consider play queue first.
		queue_entry_t *entry = queue_get_entry(0);
		while (entry && entry->track) {
			if (try_load_track(session, entry->track, start_index) != -1)
				return 0;
			// Failed to load. Remove track and try next in queue.
			queue_remove(0);
			entry = queue_get_entry(0);
		}
		// Nothing in queue to play.
		
		// If not repeat, is there any available tracks left until end?
		for(i = start_index; i < num_tracks; i++) {
			sp_track * track = get_context_track(g_index_mapping[i]);
			if (try_load_track(session, track, i) != -1)
				return 0;
		}
		// If repeat, is there any tracks left at all?
		for(i = 0; repeat() && i < start_index; i++) {
			sp_track * track = get_context_track(g_index_mapping[i]);
			if (try_load_track(session, track, i) != -1)
				return 0;
		}
	} else {
		// Previous.
		// If not repeat, is there any available tracks left until start?
		for(i = start_index; i >= 0; i--) {
			sp_track * track = get_context_track(g_index_mapping[i]);
			if (try_load_track(session, track, i) != -1)
				return 0;
		}
		// If repeat, is there any tracks left at all?
		for(i = num_tracks-1; repeat() && i > start_index; i--) {
			sp_track * track = get_context_track(g_index_mapping[i]);
			if (try_load_track(session, track, i) != -1)
				return 0;
		}
	}
	return -1;
}

int load_and_playonce(sp_session *session, sp_track * track, uint32_t offset_ms, playonce_t next_action) {
	if (g_playonce == kPlayonceNone) {
		g_playonce_previous_offset_ms = get_position();
		g_playonce_previous_state_was_playing = !g_playback.paused;
	}
	g_last_error = sp_session_player_load(session, track);
	if (g_last_error == SP_ERROR_OK) {
		g_playonce = next_action;
		g_playback.paused = 0;
		g_playonce_track = track;
		sp_track_add_ref(g_playonce_track);
		set_position(0);
		sp_session_player_play(session, 1);
		if (offset_ms > 0 && offset_ms < sp_track_duration(track)) {
			sp_session_player_seek(session, offset_ms);
		}
		return 0;
	}
	return -1;
}

void current_track_ended(sp_session * session, end_reason_t reason) {

	queue_entry_t *queue_entry = queue_get_entry(0);

	// Add track that ended to history.
	if (g_playonce != kPlayonceNone) {
		history_add(g_playonce_track, NULL);
	} else if (queue_entry != NULL && queue_entry->track == playback()->current_track) {
		history_add(queue_entry->track, queue_entry->metadata);
	} else {
		history_add(playback()->current_track, NULL);
	}


	if (g_playonce != kPlayonceNone) {
		// Free playonce track.
		stop_no_release(session);
		if (g_playonce_track) {
			sp_track_release(g_playonce_track);
			g_playonce_track = NULL;
		}
		// Continue from where we left of, if there is any.
		if (playback()->current_track && playback()->context != kPlayingNone) {
			g_last_error = sp_session_player_load(session, playback()->current_track);
			if (g_last_error == SP_ERROR_OK) {
				g_playback.paused = (g_playonce == kPlayoncePause || (g_playonce == kPlayonceKeep && !g_playonce_previous_state_was_playing) ? 1 : 0);
				sp_session_player_play(session, !g_playback.paused);
				if (g_playonce_previous_offset_ms > 0) {
					sp_session_player_seek(session, g_playonce_previous_offset_ms);
					set_position(g_playonce_previous_offset_ms);
				}
			}
		} else {
			g_playback.paused = 1;
		}
		g_playonce = kPlayonceNone;
		return;
	}

	int new_index = playback()->current_index;
	int playing_from_queue = queue_entry != NULL && queue_entry->track == playback()->current_track;
	
	// First in list, trying to play previous track and repeat?
	if (playback()->current_index == 0 && reason == kEndPrevious) {
		// repeat(): Goto last track.
		// !repeat(): Stay on first track if not repeat and trying to play previous.
		new_index = repeat() ? g_mapping_length-1 : 0;
	} else if (reason != kEndPrevious && playback()->current_index+1 >= g_mapping_length && !playing_from_queue) {
		// repeat(): End of list and repeat, goto first track.
		// !repeat(): End of list and not repeat, end playback.
		if (repeat())
			new_index = 0;
		else
			stop_and_release_playback(session);
	} else if (reason == kEndPrevious) {
		// Go to previous.
		new_index = playback()->current_index-1;
	} else if (reason == kEndRestart) {
		// Stay on same track.
		new_index = playback()->current_index;
	} else if (!playing_from_queue) {
		// Goto next.
		new_index = playback()->current_index+1;
	}

	// Free old current track.
	if (playback()->current_track) {
		sp_track_release(playback()->current_track);
		audio_flush();
		sp_session_player_unload(session);
	}
	playback()->current_track = NULL;
	
	// If playing from play queue, remove track from play queue (if not restarting)
	if (playing_from_queue && reason != kEndRestart)
		queue_remove(0);
	
	// Try to load a new one.
	if (load_and_play_next_available_track(session, new_index, (reason == kEndPrevious)) == -1) {
		// No playable available track found, stop playback.
		stop_and_release_playback(session);
	}
}

/**
* True random, yep! Using the high bits.
*/
uint32_t true_random(uint32_t max) {
	return (int) ((double)max * (rand() / (RAND_MAX + 1.0))); 
}

void generate_mapping(int32_t length) {
	// If length = -1, use old length (used from shuffle command)
	if (length < 0)
		length = g_mapping_length;

	if (length >= MAX_NUM_TRACKS)
		length = MAX_NUM_TRACKS;
		
	// Build track list index mapping.
	uint32_t i = 0;
	for(i = 0; i < length; i++)
		g_index_mapping[i] = i;

	if (shuffle() && length > 0) {
		for (i = 0; i < length - 1; i++) {
			uint32_t j = i + rand() / (RAND_MAX / (length - i) + 1);
			// Don't move the one that is currently playing.
			if (i == playback()->current_index)
				continue;
			uint32_t t = g_index_mapping[j];
			g_index_mapping[j] = g_index_mapping[i];
			g_index_mapping[i] = t;
		}
	}
	
	g_index_mapping[length] = 0;
	g_mapping_length = length;
}

uint32_t get_mapping(uint32_t index) {
	return index < g_mapping_length && index < MAX_NUM_TRACKS ? g_index_mapping[index] : 0;
}

uint32_t get_mapping_length() {
	return g_mapping_length > MAX_NUM_TRACKS ? MAX_NUM_TRACKS : g_mapping_length;
}

uint32_t get_context_num_tracks() {
	switch(playback()->context) {
		case kPlayingTrack:
			if (!playback()->track)
				return 0;
			return 1;

		case kPlayingAlbum:
			if (!playback()->album_browse || !sp_albumbrowse_is_loaded(playback()->album_browse))
				return 0;
			return sp_albumbrowse_num_tracks(playback()->album_browse);

		case kPlayingPlaylist:
			if (!playback()->playlist || !sp_playlist_is_loaded(playback()->playlist))
				return 0;
			return sp_playlist_num_tracks(playback()->playlist);

		case kPlayingSearch:
			if (!playback()->search || !sp_search_is_loaded(playback()->search))
				return 0;
			return sp_search_num_tracks(playback()->search);

		default:
			break;
	}
	return 0;
}

sp_track * get_context_track(uint32_t index) {
	switch(playback()->context) {
		case kPlayingTrack:
			return playback()->track;

		case kPlayingAlbum:
			if (!playback()->album_browse || !sp_albumbrowse_is_loaded(playback()->album_browse) || index >= sp_albumbrowse_num_tracks(playback()->album_browse))
				return NULL;
			return sp_albumbrowse_track(playback()->album_browse, index);

		case kPlayingPlaylist:
			if (!playback()->playlist || !sp_playlist_is_loaded(playback()->playlist) || index >= sp_playlist_num_tracks(playback()->playlist))
				return NULL;
			return sp_playlist_track(playback()->playlist, index);

		case kPlayingSearch:
			if (!playback()->search || !sp_search_is_loaded(playback()->search) || index >= sp_search_num_tracks(playback()->search))
				return NULL;
			return sp_search_track(playback()->search, index);

		default:
			break;
	}
	return NULL;
}

sp_error get_last_error() {
	return g_last_error;
}

int queue_add(sp_track * track, uint32_t index, char *metadata) {
	if (!track)
		return -1;

	// At limit, no more entries allowed.
	if (g_queue_length >= MAX_NUM_TRACKS) {
		return -2;
	}

	// if index way to high, pretend to add it last in queue.
	if (index >= g_queue_length) {
		index = g_queue_length;
	}

	// if index is current playing track, add after.
	if (index == 0 && g_queue_length > 0) {
		queue_entry_t *entry = &g_queue[index];
		sp_track *track = entry->track;
		if (track && playback()->current_track == track) {
			index = 1;
		}
	}

	// Move all entries down one step.
	uint32_t i = 0;
	for (i = g_queue_length; i > index; --i) {
		g_queue[i] = g_queue[i-1];
	}
	// Insert new item.
	g_queue[i].track = track;
	// Have metadata?
	if (metadata) {
		g_queue[i].metadata = malloc(MAX_QUEUE_METADATA_BYTES);
		strncpy(g_queue[i].metadata, metadata, MAX_QUEUE_METADATA_BYTES);
	}
	g_queue[i].timestamp = (uint32_t)time(NULL);

	g_queue_length++;
	sp_track_add_ref(track);
	return 0;
}

int queue_remove(uint32_t index) {
	if (index >= g_queue_length)
		return -1;
	
	queue_entry_t *entry = &g_queue[index];
	sp_track *track = entry->track;
	
	// Cannot remove current playing track.
	if (track && playback()->current_track == track)
		return -2;
	
	if (track)
		sp_track_release(track);

	// Free metadata
	if (entry->metadata) {
		free(entry->metadata);
	}

	// Move all track up in queue.
	uint32_t i = 0;
	for (i = index; i < g_queue_length-1; i++) {
		g_queue[i] = g_queue[i+1];
	}
	g_queue[i].track = NULL;
	g_queue[i].metadata = NULL;
	g_queue[i].timestamp = 0;

	g_queue_length--;
	return 0;
}

uint32_t queue_num_tracks(void) {
	return g_queue_length;
}

queue_entry_t *queue_get_entry(uint32_t index) {
	if (index >= g_queue_length)
		return NULL;
		
	return &g_queue[index];
}

void queue_clear() {
	uint32_t i = 0;
	uint32_t tracks_left = g_queue_length;
	for (i = 0; i < g_queue_length; i++) {
		queue_entry_t *entry = &g_queue[i];
		
		// Cannot remove current playing track.
		if (entry && entry->track && playback()->current_track == entry->track)
			continue;
		
		if (entry->track)
			sp_track_release(entry->track);
		// Free metadata
		if (entry->metadata) {
			free(entry->metadata);
		}
		g_queue[i].track = NULL;
		g_queue[i].metadata = NULL;
		g_queue[i].timestamp = 0;
		tracks_left--;
	}
	g_queue_length = tracks_left;
}

uint32_t history_num_tracks(void) {
	return g_history_length;
}

queue_entry_t *history_get_entry(uint32_t index) {
	if (index >= g_history_length)
		return NULL;
		
	return &g_history[index];
}

void history_clear(void) {
	uint32_t i = 0;
	for (i = 0; i < g_history_length; i++) {
		queue_entry_t *entry = &g_history[i];
		if (entry->track) {
			sp_track_release(entry->track);
		}
		// Free metadata
		if (entry->metadata) {
			free(entry->metadata);
		}
		g_history[i].track = NULL;
		g_history[i].metadata = NULL;
		g_history[i].timestamp = 0;
	}
	g_history_length = 0;
}

int history_add(sp_track * track, char *metadata) {
	if (!track)
		return -1;

	// At limit, no more entries allowed.
	if (g_history_length >= MAX_NUM_TRACKS) {
		return -2;
	}

	uint32_t index = g_history_length;

	// Insert new item.
	g_history[index].track = track;
	// Have metadata?
	if (metadata) {
		g_history[index].metadata = malloc(MAX_QUEUE_METADATA_BYTES);
		strncpy(g_history[index].metadata, metadata, MAX_QUEUE_METADATA_BYTES);
	}
	g_history[index].timestamp = (uint32_t)time(NULL);

	g_history_length++;
	sp_track_add_ref(track);
	return 0;
}
