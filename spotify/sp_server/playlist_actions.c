#include "playlist_actions.h"
#include "error.h"
#include <string.h>

int playlist_action(sp_session *session, client_t * client, sp_link *link, sp_playlist *playlist, char **args, uint32_t num_args) {

	// Check playlist and link validity.
	if (!link && !playlist)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type playlist (null).");
	
	if (!playlist && sp_link_type(link) != SP_LINKTYPE_PLAYLIST && sp_link_type(link) != SP_LINKTYPE_STARRED) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type playlist.");
	}

	if (link) {
		playlist = sp_playlist_create(session, link);
		sp_link_release(link);
	} 
	if (!playlist)
		return command_error(client, ERROR_RESOURCE_NOT_FOUND, "The playlist does not exist (null).");
	
	// Set collaborative on or off.
	if (num_args == 2 && !strcmp("collaborative", args[0])) {
		int collaborative = atoi(args[1]) ? 1 : 0;
		sp_playlist_set_collaborative(playlist, collaborative);
		return command_success(client, NULL);
	}

	// Get list of subscribers.
	if (num_args == 1 && !strcmp("subscribers", args[0])) {
		// Get subscribers for next time.
		sp_playlist_update_subscribers(session, playlist);
	
		sp_subscribers * subscribers = sp_playlist_subscribers(playlist);
		uint32_t i = 0;
		json_t *result = json_object();
		json_t *subscribers_array = json_array();
		
		for (i = 0; i < subscribers->count; i++)
			json_array_append_new(subscribers_array, json_string(subscribers->subscribers[i]));

		json_object_set_new(result, "subscribers", subscribers_array);
		
		sp_playlist_subscribers_free(subscribers);
		return command_success(client, result);
	}
	
	// Rename playlist.
	if (num_args == 2 && !strcmp("rename", args[0])) {
		sp_error error = sp_playlist_rename(playlist, args[1]);
		if (error == SP_ERROR_INVALID_INDATA)
			return command_error(client, ERROR_INVALID_INDATA, "String is invalid.");
		else if (error == SP_ERROR_PERMISSION_DENIED)
			return command_error(client, ERROR_PERMISSION_DENIED, "Permission denied. You can only rename playlists that you own.");
		else if (error == SP_ERROR_OK)
			return command_success(client, NULL);
	}
	
	// Set seen or unseen for a track (in e.g inbox)
	if (num_args == 3 && !strcmp("seen", args[0])) {
		int index = atoi(args[1]);
		int seen = atoi(args[2]) ? 1 : 0;
		
		sp_error error = sp_playlist_track_set_seen(playlist, index, seen);
		if (error == SP_ERROR_INDEX_OUT_OF_RANGE)
			return command_error(client, ERROR_INDEX_OUT_OF_BOUNDS, "Index out out of bounds.");
		else if (error == SP_ERROR_OK)
			return command_success(client, NULL);
	}
	
	json_t * json = NULL;
	json_error_t error;
	if (client->payload && client->payload_length > 0)
		json = json_loads((const char *)client->payload, JSON_DISABLE_EOF_CHECK, &error);

	// Add tracks to playlist.
	// indata: json["track uri", "track uri", "track uri", ...]
	if (num_args >= 1 && !strcmp("add", args[0])) {
		if (!json || !json_is_array(json))
			return command_error(client, ERROR_INVALID_INDATA, "Unable to parse payload json data. Not an array [\"track uri\", \"track uri\", \"track uri\", ...]?");
		
		int position = sp_playlist_num_tracks(playlist);
		if (num_args == 2)
			position = atoi(args[1]);
		int num_tracks = json_array_size(json);
		int actual_num_tracks = 0;
		sp_track ** tracks = malloc(sizeof(sp_track*)*num_tracks);
		
		uint32_t i = 0;
		for(i = 0; i < num_tracks; i++) {
			json_t *track_obj = json_array_get(json, i);
			if (!track_obj || !json_is_string(track_obj))
				continue;
			// Valid track link?
			sp_link* link = sp_link_create_from_string(json_string_value(track_obj));
			if (!link)
				continue;
			if (sp_link_type(link) != SP_LINKTYPE_TRACK) {
				sp_link_release(link);
				continue;
			}
			// Valid track?
			sp_track * track = sp_link_as_track(link);
			if (!track) {
				sp_link_release(link);
				continue;
			}
			// All good.
			sp_track_add_ref(track);
			sp_link_release(link);
			tracks[actual_num_tracks++] = track;
		}
		json_decref(json);
		
		// Add tracks.
		sp_error error = sp_playlist_add_tracks(playlist, tracks, actual_num_tracks, position, session);
		// Free track references and array.
		for (i = 0; i < actual_num_tracks; i++)
			sp_track_release(tracks[i]);
		free(tracks);
		// Check for error.
		if (error == SP_ERROR_INVALID_INDATA)
			return command_error(client, ERROR_INVALID_INDATA, "Position is > current playlist length.");
		else if (error == SP_ERROR_PERMISSION_DENIED)
			return command_error(client, ERROR_PERMISSION_DENIED, "Permission denied. You can only add tracks to a playlist you own or is collaborative.");
		else if (error == SP_ERROR_OK) {
			json_t * result = json_object();
			json_object_set_new(result, "tracks_added", json_integer(actual_num_tracks));
			return command_success(client, result);
		}
	}

	// Move tracks within playlist.
	// indata: json[0, 1, 2, ...]
	if (num_args == 2 && !strcmp("move", args[0])) {
		if (!json || !json_is_array(json))
			return command_error(client, ERROR_INVALID_INDATA, "Unable to parse payload json data. Not an array [int, int, int, ...]?");
			
		int new_position = atoi(args[1]);
		int num_tracks = json_array_size(json);
		int actual_num_tracks = 0;
		int * tracks = malloc(sizeof(int)*num_tracks);
		
		uint32_t i = 0;
		for(i = 0; i < num_tracks; i++) {
			json_t *track_obj = json_array_get(json, i);
			if (!track_obj || !json_is_integer(track_obj))
				continue;

			tracks[actual_num_tracks++] = json_integer_value(track_obj);
		}
		json_decref(json);
		
		// Move tracks.
		sp_error error = sp_playlist_reorder_tracks(playlist, tracks, actual_num_tracks, new_position);
		// Free array.
		free(tracks);
		// Check for error.
		if (error == SP_ERROR_INVALID_INDATA)
			return command_error(client, ERROR_INVALID_INDATA, "Position is > current playlist length.");
		else if (error == SP_ERROR_PERMISSION_DENIED)
			return command_error(client, ERROR_PERMISSION_DENIED, "Permission denied. You can only move tracks within a playlist you own or is collaborative.");
		else if (error == SP_ERROR_OK) {
			json_t * result = json_object();
			json_object_set_new(result, "tracks_moved", json_integer(actual_num_tracks));
			return command_success(client, result);
		}
	}
	
	// Remove tracks from playlist.
	// indata: [0, 1, 2, ...]
	if (num_args == 1 && !strcmp("remove", args[0])) {
		if (!json || !json_is_array(json))
			return command_error(client, ERROR_INVALID_INDATA, "Unable to parse payload json data. Not an array [int, int, int, ...]?");
			
		int num_tracks = json_array_size(json);
		int actual_num_tracks = 0;
		int * tracks = malloc(sizeof(int)*num_tracks);
		
		uint32_t i = 0;
		for(i = 0; i < num_tracks; i++) {
			json_t *track_obj = json_array_get(json, i);
			if (!track_obj || !json_is_integer(track_obj))
				continue;

			tracks[actual_num_tracks++] = json_integer_value(track_obj);
		}
		json_decref(json);
		
		// Remove tracks.
		sp_error error = sp_playlist_remove_tracks(playlist, tracks, actual_num_tracks);
		// Free array.
		free(tracks);
		// Check for error.
		if (error == SP_ERROR_PERMISSION_DENIED)
			return command_error(client, ERROR_PERMISSION_DENIED, "Permission denied. You can only remove tracks in a playlist you own or is collaborative.");
		else if (error == SP_ERROR_OK) {
			json_t * result = json_object();
			json_object_set_new(result, "tracks_removed", json_integer(actual_num_tracks));
			return command_success(client, result);
		}
	}

	if (json)
		json_decref(json);
	return command_error(client, ERROR_UNKNOWN_COMMAND, "Command for playlist not implemented.");
}