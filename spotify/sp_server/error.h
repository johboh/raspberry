#ifndef __ERROR_H__
#define __ERROR_H__

#include <stdint.h>
#include <jansson.h>
#include <libspotify/api.h>
#include "client.h"

#define ERROR_UNKNOWN                  -1

#define ERROR_SUCCESS                   1

#define ERROR_WRONG_ARGUMENTS_COUNT  1001
#define ERROR_WRONG_API_VERSION      1002
#define ERROR_UNKNOWN_COMMAND        1003
#define ERROR_INVALID_LINK_TYPE      1004
#define ERROR_NOT_LOGGED_IN          1005
#define ERROR_SEARCH_FAILED          1006
#define ERROR_BROWSE_FAILED          1007
#define ERROR_RESOURCE_NOT_FOUND     1008
#define ERROR_RESOURCE_UNAVAILABLE   1009
#define ERROR_INDEX_OUT_OF_BOUNDS    1010
#define ERROR_TRACK_IS_PLAYING       1011
#define ERROR_PERMISSION_DENIED      1012
#define ERROR_INVALID_INDATA         1013

#define ERROR_LOGIN_CLIENT_TOO_OLD            2001
#define ERROR_LOGIN_UNABLE_TO_CONTACT_SERVER  2002
#define ERROR_LOGIN_BAD_USERNAME_OR_PASSWORD  2003
#define ERROR_LOGIN_USER_BANNED               2004
#define ERROR_LOGIN_USER_NEEDS_PREMIUM        2005
#define ERROR_LOGIN_OTHER_TRANSIENT           2006
#define ERROR_LOGIN_OTHER_PERMANENT           2007

int command_error(client_t * client, int32_t code, const char * string);
int command_error_sp(client_t * client, sp_error code);
int command_success(client_t * client, json_t * object);

#endif // __ERROR_H__
