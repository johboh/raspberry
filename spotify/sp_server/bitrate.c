#include "commands.h"
#include "state.h"
#include "error.h"

int command_bitrate(sp_session *session, client_t * client, int bitrate) {
	sp_bitrate br = SP_BITRATE_160k;
	if (bitrate == 96)
		br = SP_BITRATE_96k;
	else if (bitrate == 320)
		br = SP_BITRATE_320k;

	sp_session_preferred_bitrate(session, br);
	return command_status(session, client);
}
