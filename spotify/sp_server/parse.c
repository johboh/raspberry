#include "commands.h"
#include "error.h"
#include "state.h"
#include "track_actions.h"
#include "playlist_actions.h"
#include "playlists_actions.h"
#include "scrobble.h"

#include <string.h>
#include <stdlib.h>

#define NUM_TRACKS 100
#define NUM_ARTISTS 100
#define NUM_ALBUMS 100
#define NUM_PLAYLISTS 50

int command_parse(sp_session *session, client_t *client) {

	if (!client)
		return -1;

	// No arguments, error.
	if (client->num_args < 1)
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "No argument specified.");
	
	// Requesting specific docs site, return docs.
	if (client->num_args >= 1 && !strcmp("docs", client->args[0]))
		return command_docs(session, client, client->args[1]);
	
	// Wrong api version, error.
	if (atoi(client->args[0]) != COMMAND_API_VERSION)
		return command_error(client, ERROR_WRONG_API_VERSION, "Wrong API version.");

	// If not logged in, only allow status and login command.
	uint8_t is_login = client->num_args >= 2 && !strcmp("login", client->args[1]);
	if (!is_logged_in(session) && !is_login)
		return command_error(client, ERROR_NOT_LOGGED_IN, "Not logged in.");
	else if (is_login) {
		if (client->num_args == 4)
			return command_login(session, client, client->args[2], client->args[3], 0);  
		else if (client->num_args == 5)
			return command_login(session, client, client->args[2], client->args[3], atoi(client->args[4]));
		return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to login. expected /login/<username>/<password> or /login/<username>/<password>/1|0");
	}

	// No additional argument than api version, return status.
	if (client->num_args < 2 || !strcmp("status", client->args[1]))
		return command_status(session, client);	

	// Check second argument for command.
	if (!strcmp("shuffle", client->args[1])) {
		if (client->num_args != 3)
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to shuffle. Expected /shuffle/<1|0>");
		return command_shuffle(session, client, atoi(client->args[2]));    
	}
	else if (!strcmp("repeat", client->args[1])) {
		if (client->num_args != 3)
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to repeat. Expected /repeat/<1|0>");
		return command_repeat(session, client, atoi(client->args[2]));
	}
	else if (!strcmp("seek", client->args[1])) {
		if (client->num_args != 3)
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to seek. Expected /seek/<position ms>");
		return command_seek(session, client, atoi(client->args[2]));
	}
	else if (!strcmp("volume", client->args[1])) {
		if (client->num_args != 3)
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to volume. Expected /volume/<volume 0-100>");
		return command_volume(session, client, atoi(client->args[2]));
	}
	else if (!strcmp("volume_normalization", client->args[1])) {
		if (client->num_args != 3)
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to normalization. Expected /normalization/<1|0>");
		return command_volume_normalization(session, client, atoi(client->args[2]));
	}
	else if (!strcmp("bitrate", client->args[1])) {
		if (client->num_args != 3)
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to bitrate. Expected /bitrate/<96, 160 or 320>");
		return command_bitrate(session, client, atoi(client->args[2]));
	}
	else if (!strcmp("search", client->args[1])) {
		if (client->num_args == 3)
			return command_search(session, client, client->args[2], NUM_TRACKS, NUM_ALBUMS, NUM_ARTISTS, NUM_PLAYLISTS);
		else if (client->num_args == 7)
			return command_search(session, client, client->args[2], atoi(client->args[3]), atoi(client->args[4]), atoi(client->args[5]), atoi(client->args[6]));
		return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to search. Expected /search/<query> or /search/<query>/<num tracks>/<num albums>/<num artists>/<num playlists>");
	} 
	else if (client->num_args >= 2 && !strcmp("inbox", client->args[1])) {
		sp_playlist *playlist = sp_session_inbox_create(session);
		if (client->num_args == 2)
			return command_browse_playlist(session, client, NULL, playlist, 0);
		else if (client->num_args == 3 && !strcmp("no_tracks", client->args[2]))
			return command_browse_playlist(session, client, NULL, playlist, 1);
		else if (client->num_args == 3 && !strcmp("play", client->args[2]))
			return command_play_playlist(session, client, NULL, playlist, 0);
		else if (client->num_args == 4 && !strcmp("play", client->args[2]))
			return command_play_playlist(session, client, NULL, playlist, atoi(client->args[3]));
		else if(client->num_args >= 3)
			return playlist_action(session, client, NULL, playlist, &client->args[2], client->num_args-2);
		return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /inbox or /inbox/no_tracks or /inbox/play or /inbox/play/<track index> or /inbox/rename|subscribers..etc.");
	} 
	else if (client->num_args >= 2 && !strcmp("starred", client->args[1])) {
		sp_playlist *playlist = sp_session_starred_create(session);
		if (client->num_args == 2)
			return command_browse_playlist(session, client, NULL, playlist, 0);
		else if (client->num_args == 3 && !strcmp("no_tracks", client->args[2]))
			return command_browse_playlist(session, client, NULL, playlist, 1);
		else if (client->num_args == 3 && !strcmp("play", client->args[2]))
			return command_play_playlist(session, client, NULL, playlist, 0);
		else if (client->num_args == 4 && !strcmp("play", client->args[2]))
			return command_play_playlist(session, client, NULL, playlist, atoi(client->args[3]));
		else if(client->num_args >= 3)
			return playlist_action(session, client, NULL, playlist, &client->args[2], client->num_args-2);
		return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /starred or /starred/no_tracks or /starred/play or /starred/play/<track index> or /starred/rename|subscribers..etc.");
	}
	else if (client->num_args >= 2 && !strcmp("queue", client->args[1])) {
		if (client->num_args == 2) {
			return command_queue(session, client, -1, -2);
		}
		else if (client->num_args == 4 && !strcmp("limit", client->args[2])) {
			return command_queue(session, client, atoi(client->args[3]), -2);
		}
		else if (client->num_args == 5 && !strcmp("limit", client->args[2])) {
			return command_queue(session, client, atoi(client->args[3]), atoi(client->args[4]));
		}
		else if (client->num_args == 3 && !strcmp("clear", client->args[2])) {
			return command_queue_clear(session, client);
		}
		else if ((client->num_args == 4 || client->num_args == 6) && !strcmp("add", client->args[2])) {
			sp_link * link = sp_link_create_from_string(client->args[3]);
			if (!link || sp_link_type(link) != SP_LINKTYPE_TRACK)
				return command_error(client, ERROR_INVALID_LINK_TYPE, "The track uri in /queue/add/<track uri> is not a track.");
			if (client->num_args == 4)
				return command_queue_add(session, client, link, 0, NULL);
			else
				return command_queue_add(session, client, link, atoi(client->args[4]), client->args[5]);
		}
		else if (client->num_args == 4 && !strcmp("remove", client->args[2])) {
			return command_queue_remove(session, client, atoi(client->args[3]));
		}
		else {
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /queue or /queue/limit/<max num tracks> or /queue/add/<track uri> or /queue/add/<track uri>/<index> or /queue/add/<track uri>/index/<metadata> or /queue/remove/<track index> or /queue/clear");
		}
	}
	else if (client->num_args >= 2 && !strcmp("history", client->args[1])) {
		if (client->num_args == 4 && !strcmp("limit", client->args[2])) {
			return command_history(session, client, atoi(client->args[3]));
		} else if (client->num_args == 2) {
			return command_history(session, client, -1);
		} else {
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /history or /history/limit/<max num tracks>");
		}
	}
	else if (client->num_args >= 2 && !strcmp("playlists", client->args[1])) {
		if (client->num_args == 2)
			return command_browse_playlists(session, client, 0, NULL);
		else if (client->num_args == 3 && !strcmp("flat", client->args[2]))
			return command_browse_playlists(session, client, 1, NULL);
		else if(client->num_args >= 3)
			return playlists_action(session, client, &client->args[2], client->num_args-2);
		else {
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /playlists, /playlists/flat or /playlists/add|remove...etc.");
		}
	}
	else if (client->num_args >= 2 && !strcmp("folder", client->args[1])) {
		if (client->num_args == 3)
			return command_browse_playlists(session, client, 0, client->args[2]); 
		else if (client->num_args == 4 && !strcmp("flat", client->args[3]))
			return command_browse_playlists(session, client, 1, client->args[2]);
		else if(client->num_args >= 4)
			return playlists_action(session, client, &client->args[3], client->num_args-3);
		else {
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /folder/<folderid>, /folder/<folderid>/flat or /folder/<folderid>/add|remove...etc.");
		}
	}
	else if (client->num_args >= 2 && !strcmp("previous", client->args[1])) {
		if (client->num_args == 2)
			return command_previous(session, client, 0);
		else if (client->num_args == 3 && !strcmp("restart", client->args[2]))
			return command_previous(session, client, 1);
		else
			return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /previous or /previous/restart.");
	}
	else if (client->num_args >= 2 && !strcmp("lastfm", client->args[1])) {
		if (client->num_args == 5 && !strcmp("login", client->args[2]))
			return command_lastfm_login(session, client, client->args[3], client->args[4]);
		else if (client->num_args == 4 && !strcmp("enable", client->args[2]))
			return command_lastfm_enable(session, client, atoi(client->args[3]));
		else if (client->num_args == 3 && !strcmp("logout", client->args[2]))
			return command_lastfm_logout(session, client);
		return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified to lastfm. Expected /lastfm/login/<username>/<password>, /lastfm/enable/<1|0> or /lastfm/logout");
	}
	else if (client->num_args == 2 && !strcmp("play", client->args[1]))
		return command_play(session, client);
	else if (client->num_args == 2 && !strcmp("pause", client->args[1]))
		return command_pause(session, client);
	else if (client->num_args == 2 && !strcmp("next", client->args[1]))
		return command_next(session, client);
	else if (client->num_args == 2 && !strcmp("logout", client->args[1]))
		return command_logout(session, client);
	else if (client->num_args == 2 && !strcmp("debug", client->args[1]))
		return command_debug(session, client, 0);
	else if (client->num_args == 3 && !strcmp("debug", client->args[1]) && !strcmp("clear", client->args[2]))
		return command_debug(session, client, 1);
	else {
		sp_link * link = sp_link_create_from_string(client->args[1]);
		if (!link)
			goto unknown;

		sp_linktype type = sp_link_type(link);
		
		// We have a spotify link.
		switch(type) {
			case SP_LINKTYPE_ALBUM:
				if (client->num_args == 2)
					return command_browse_album(session, client, link);
				else if (client->num_args >= 3 && !strcmp("image", client->args[2])) {
					if (client->num_args == 4 && !strcmp("small", client->args[3]))
						return command_image(session, client, link, SP_IMAGE_SIZE_SMALL);
					else if (client->num_args == 4 && !strcmp("large", client->args[3]))
						return command_image(session, client, link, SP_IMAGE_SIZE_LARGE);
					return command_image(session, client, link, SP_IMAGE_SIZE_NORMAL);
				} else if (client->num_args == 3 && !strcmp("play", client->args[2]))
					return command_play_album(session, client, link, 0);
				else if (client->num_args == 4 && !strcmp("play", client->args[2]))
					return command_play_album(session, client, link, atoi(client->args[3]));
				return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /<album uri> or /<album uri>/image or /<album uri>/play or /<album uri>/play/<track index>");
				
			case SP_LINKTYPE_TRACK:
				if (client->num_args == 2)
					return command_browse_track(session, client, link);
				else if (client->num_args == 3 && !strcmp("play", client->args[2]))
					return command_play_track(session, client, link, 0, NULL);
				else if (client->num_args == 4 && !strcmp("playonce", client->args[2]))
					return command_play_track(session, client, link, 0, client->args[3]);
				else if (client->num_args == 5 && !strcmp("playonce", client->args[2]))
					return command_play_track(session, client, link, atoi(client->args[3]), client->args[4]);
				else if(client->num_args >= 3)
					return track_action(session, client, link, &client->args[2], client->num_args-2);
				return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /<track uri> or /<track uri>/<action...> or /<track uri>/play");
				
			case SP_LINKTYPE_ARTIST:
				if (client->num_args == 2)
					return command_browse_artist(session, client, link, SP_ARTISTBROWSE_FULL);
				else if (client->num_args == 3 && !strcmp("image", client->args[2])) {
					if (client->num_args == 4 && !strcmp("small", client->args[3]))
						return command_image(session, client, link, SP_IMAGE_SIZE_SMALL);
					else if (client->num_args == 4 && !strcmp("large", client->args[3]))
						return command_image(session, client, link, SP_IMAGE_SIZE_LARGE);
					return command_image(session, client, link, SP_IMAGE_SIZE_NORMAL);
				} else if (client->num_args == 3 && !strcmp("no_tracks", client->args[2]))
					return command_browse_artist(session, client, link, SP_ARTISTBROWSE_NO_TRACKS);
				else if (client->num_args == 3 && !strcmp("no_albums_or_tracks", client->args[2]))
					return command_browse_artist(session, client, link, SP_ARTISTBROWSE_NO_ALBUMS);
				return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /<artist uri> or /<artist uri>/image or /<artist uri>/no_tracks or /<artist uri>/no_albums_or_tracks");
				
			case SP_LINKTYPE_STARRED:
			case SP_LINKTYPE_PLAYLIST:
				if (client->num_args == 2)
					return command_browse_playlist(session, client, link, NULL, 0);
				else if (client->num_args == 3 && !strcmp("no_tracks", client->args[2]))
					return command_browse_playlist(session, client, link, NULL, 1);
				else if (client->num_args == 3 && !strcmp("play", client->args[2]))
					return command_play_playlist(session, client, link, NULL, 0);
				else if (client->num_args == 4 && !strcmp("play", client->args[2]))
					return command_play_playlist(session, client, link, NULL, atoi(client->args[3]));
				else if(client->num_args >= 3)
					return playlist_action(session, client, link, NULL, &client->args[2], client->num_args-2);
				return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /<playlist uri> or /<playlist uri>/no_tracks or /<playlist uri>/<action...> or /<playlist uri>/play or /<playlist uri>/play/<track index> or /<playlist uri>/rename|subscribers..etc.");
				
			case SP_LINKTYPE_SEARCH: {
				// TODO: How to get query from search uri?
				int pre_length = strlen("spotify:search:");
				if (client->num_args == 2)
					return command_search(session, client, client->args[1] + pre_length, NUM_TRACKS, NUM_ALBUMS, NUM_ARTISTS, NUM_PLAYLISTS);
				else if (client->num_args == 3 && !strcmp("play", client->args[2]))
					return command_play_search(session, client, link, 0);
				else if (client->num_args == 4 && !strcmp("play", client->args[2]))
					return command_play_search(session, client, link, atoi(client->args[3]));
				else if (client->num_args == 6)
					return command_search(session, client, client->args[1] + pre_length, atoi(client->args[2]), atoi(client->args[3]), atoi(client->args[4]), atoi(client->args[5]));
				return command_error(client, ERROR_WRONG_ARGUMENTS_COUNT, "Wrong number of argument specified. Expected /<search uri> or /<search uri>/<num tracks>/<num albums>/<num artists>/<num playlist> or /<search uri>/play or /<search uri>/play/<track index>");
				break;
			}

			case SP_LINKTYPE_PROFILE:
				return command_image(session, client, link, SP_IMAGE_SIZE_NORMAL);
			
			case SP_LINKTYPE_IMAGE:
				return command_image(session, client, link, SP_IMAGE_SIZE_NORMAL);
				
			// Not handled.
			case SP_LINKTYPE_INVALID:
			case SP_LINKTYPE_LOCALTRACK:
			case SP_LINKTYPE_TOPLIST:
				break;
		}
	}

	// Unknown command.
unknown:
	return command_error(client, ERROR_UNKNOWN_COMMAND, "Unknown command.");
}
