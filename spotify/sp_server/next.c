#include "commands.h"
#include "state.h"
#include "error.h"

int command_next(sp_session *session, client_t * client) {
	current_track_ended(session, kEndNext);
	return command_status(session, client);
}
