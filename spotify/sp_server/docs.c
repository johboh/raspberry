#include "commands.h"
#include "error.h"
#include "client.h"
#include <string.h>
#include <unistd.h>

char* get_document(const char *base_path, const char *path) {
		char * str = malloc(strlen(base_path) + strlen(path) + strlen("/docs/") + 1);
		strcpy(str, base_path);
		strcat(str, "/docs/");
		strcat(str, path);
		return str;
}

int command_docs(sp_session *session, client_t * client, const char * path) {
	FILE *fp = NULL;
	client->response_length = 0;

	// Get path to executable.
	char base_path[1024];
	readlink("/proc/self/exe", base_path, sizeof(base_path));
	// Remove executable from path.
	char *pos = strrchr(base_path, '/');
	if (pos != NULL)
		*pos = '\0';

	// Try to open file in the docs/ directory.
	if (path && !strstr(path, "..")) {
		char * str = get_document(base_path, path);
		fp = fopen(str, "r");
		free(str);
		// Check file ending for content type.
		const char *dot = strrchr(path, '.');
		client->content_type = kContentTypeText;
		if (dot && dot != path) {
			if(!strcmp(".html", dot))
				client->content_type = kContentTypeHtml;
			else if (!strcmp(".jpg", dot))
				client->content_type = kContentTypeJpeg;
		}
	}
	
	if(!fp) {
		char *str = get_document(base_path, "index.html");
		fp = fopen(str, "r");
		free(str);
		client->content_type = kContentTypeHtml;
	}
	
	if (!fp)
		return -1;
	
	// Find out size of file and open it.
	fseek(fp, 0L, SEEK_END);
	client->response_length = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	client->response = malloc(client->response_length);
	fread(client->response, 1, client->response_length, fp);
	fclose(fp);
	return 0;
}
