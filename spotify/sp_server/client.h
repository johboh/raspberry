#ifndef __CLIENT_H__
#define __CLIENT_H__

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>

#define MAX_ARGS 15

typedef enum {
	kContentTypeText,
	kContentTypeJson,
	kContentTypeJpeg,
	kContentTypeHtml,
} content_type_t;

typedef struct {
	char *args[MAX_ARGS];
	uint32_t num_args;
	uint8_t * payload;
	uint32_t payload_length;
	uint8_t * response;
	uint32_t response_length;
	content_type_t content_type;
	sem_t sem;
	pthread_mutex_t * api_mutex;
	void *ptr;
} client_t;

#endif // __CLIENT_H__