#ifndef __SETTINGS_H__
#define __SETTINGS_H__

void settings_init(const char *base_path);
void settings_load(const char *username);
void settings_save(const char *username);
void settings_destroy(void);

#endif // __SETTINGS_H__
