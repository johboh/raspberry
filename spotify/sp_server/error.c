#include "error.h"
#include <string.h>
#include <jansson.h>

static void json_to_client(json_t * json, client_t * client) {
	char * str = json_dumps(json, 0);
	client->content_type = kContentTypeJson;
	client->response_length = strlen(str);
	client->response = calloc(client->response_length + 1, sizeof(char));
	strncpy((char *)client->response, str, client->response_length);
	free(str);
	json_decref(json);
}

int command_error(client_t * client, int32_t code, const char * string) {
	json_t *error = json_object();
	if (!error)
		return -1;
	json_t *error_sub = json_object();
	json_object_set_new(error_sub, "code", json_integer(code));
	json_object_set_new(error_sub, "string", json_string(string));
	json_object_set_new(error, "error", error_sub);
	
	json_to_client(error, client);
	return 0;
}

int command_success(client_t * client, json_t * object) {
	json_t *error_sub = json_object();
	json_object_set_new(error_sub, "code", json_integer(ERROR_SUCCESS));
	json_object_set_new(error_sub, "string", json_string("Success."));

	if (object) {
		json_object_set_new(object, "error", error_sub);
		json_to_client(object, client);
	} else {
		json_to_client(error_sub, client);
	}

	return 0;
}


int command_error_sp(client_t * client, sp_error error) {
	switch(error) {
		case SP_ERROR_OK: 
			return command_error(client, ERROR_SUCCESS, "OK");
		case SP_ERROR_CLIENT_TOO_OLD : 
			return command_error(client, ERROR_LOGIN_CLIENT_TOO_OLD, "Client is to old.");
		case SP_ERROR_UNABLE_TO_CONTACT_SERVER: 
			return command_error(client, ERROR_LOGIN_UNABLE_TO_CONTACT_SERVER, "Unable to contact server.");
		case SP_ERROR_BAD_USERNAME_OR_PASSWORD:
			return command_error(client, ERROR_LOGIN_BAD_USERNAME_OR_PASSWORD, "Bad username/password..");
		case SP_ERROR_USER_BANNED:
			return command_error(client, ERROR_LOGIN_USER_BANNED, "User is banned.");
		case SP_ERROR_USER_NEEDS_PREMIUM:
			return command_error(client, ERROR_LOGIN_USER_NEEDS_PREMIUM, "User needs premium.");
		case SP_ERROR_OTHER_TRANSIENT:
			return command_error(client, ERROR_LOGIN_OTHER_TRANSIENT, "A transient error occured.");
		case SP_ERROR_OTHER_PERMANENT:
			return command_error(client, ERROR_LOGIN_OTHER_PERMANENT, "A permanent error occured.");
		default:
			return command_error(client, ERROR_UNKNOWN, "Unknown error occured.");
	}
}