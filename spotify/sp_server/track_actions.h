#ifndef __TRACK_ACTIONS_H__
#define __TRACK_ACTIONS_H__

#include <stdint.h>

#include <libspotify/api.h>
#include "client.h"

int track_action(sp_session *session, client_t * client, sp_link *link, char **args, uint32_t num_args);

#endif // __TRACK_ACTIONS_H__