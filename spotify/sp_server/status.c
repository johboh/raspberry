#include "status.h"
#include "commands.h"
#include "state.h"
#include "metadata.h"
#include "error.h"
#include "scrobble.h"
#include "main.h"
#include <string.h>

extern sp_error g_last_connection_error;
extern sp_error g_last_streaming_error;
extern char g_last_message_to_user[2048];

int command_status(sp_session *session, client_t * client) {
	json_t *stats = json_object();
	if (!stats)
		return -1;

	sp_link *link = current_context();
	if (link != NULL) {
		char str[512];
		sp_link_as_string(link, str, sizeof(str));
		json_object_set_new(stats, "context", json_string(str));
		sp_link_release(link);
	}
	if (playback()->current_track != NULL) {
		sp_track * track = playback()->current_track;
		json_object_set_new(stats, "track", track_to_json(session, track, 1, 1));
		json_object_set_new(stats, "position", json_integer(get_position()));
		json_object_set_new(stats, "index", json_integer(playback()->current_index));
	}

	json_object_set_new(stats, "playing", json_integer(!playback()->paused));
	json_object_set_new(stats, "loading", json_integer(playback()->loading));
	json_object_set_new(stats, "libspotify_version", json_integer(SPOTIFY_API_VERSION));

	char *lastfm_username = scrobble_lastfm_username();
	if (lastfm_username)
		json_object_set_new(stats, "lastfm_username", json_string(lastfm_username));
	sp_scrobbling_state lastfm = SP_SCROBBLING_STATE_LOCAL_DISABLED;
	sp_session_is_scrobbling(session, SP_SOCIAL_PROVIDER_LASTFM, &lastfm);
	json_object_set_new(stats, "lastfm", json_integer(lastfm == SP_SCROBBLING_STATE_LOCAL_ENABLED));

	sp_user * user = sp_session_user(session);
	if (user) {
		json_object_set_new(stats, "username", json_string(sp_user_canonical_name(user)));
		json_object_set_new(stats, "name", json_string(sp_user_display_name(user)));
	}
	json_object_set_new(stats, "shuffle", json_integer(shuffle()));
	json_object_set_new(stats, "repeat", json_integer(repeat()));
	json_object_set_new(stats, "volume_normalization", json_integer(sp_session_get_volume_normalization(session)));
	return command_success(client, stats);
}

int command_debug(sp_session *session, client_t * client, int clear) {
	json_t *stats = json_object();
	if (!stats)
		return -1;

	sp_user * user = sp_session_user(session);
	if (user) {
		json_object_set_new(stats, "username", json_string(sp_user_canonical_name(user)));
		json_object_set_new(stats, "name", json_string(sp_user_display_name(user)));
	}

	char state_str[128] = "unknown state";
	sp_connectionstate state = sp_session_connectionstate(session);
	switch(state) {
		case SP_CONNECTION_STATE_LOGGED_OUT:
			strcpy(state_str, "Logged out");
			break;
		case SP_CONNECTION_STATE_LOGGED_IN:
			strcpy(state_str, "Logged in");
			break;
		case SP_CONNECTION_STATE_DISCONNECTED:
			strcpy(state_str, "disconnected");
			break;
		case SP_CONNECTION_STATE_UNDEFINED:
			strcpy(state_str, "undefined");
			break;
		case SP_CONNECTION_STATE_OFFLINE:
			strcpy(state_str, "offline");
			break;
	}
	json_object_set_new(stats, "connectionstate", json_string(state_str));

	const char* err = sp_error_message(get_last_error());
	json_object_set_new(stats, "last_playback_error", json_string(err));

	err = sp_error_message(g_last_connection_error);
	json_object_set_new(stats, "last_connection_error", json_string(err));
	err = sp_error_message(g_last_streaming_error);
	json_object_set_new(stats, "last_streaming_error", json_string(err));
	json_object_set_new(stats, "last_message_to_user", json_string(g_last_message_to_user));

	if (clear) {
		g_last_connection_error = SP_ERROR_OK;
		g_last_streaming_error = SP_ERROR_OK;
		strcpy(g_last_message_to_user, "<no msg>");
	}

	return command_success(client, stats);
}

sp_link * current_context() {
	sp_link *link = NULL;
	switch(playback()->context) {
		case kPlayingTrack:
			if (playback()->track)
				link = sp_link_create_from_track(playback()->track, 0);
			break;
		case kPlayingAlbum:
			if (playback()->album_browse) {
				sp_album * album = sp_albumbrowse_album(playback()->album_browse);
				if (album)
					link = sp_link_create_from_album(album);
			}
			break;
		case kPlayingPlaylist:
			if (playback()->playlist)
				link = sp_link_create_from_playlist(playback()->playlist);
			break;
		case kPlayingSearch:
			if (playback()->search)
				link = sp_link_create_from_search(playback()->search);
			break;
		case kPlayingNone:
			// Nothing.
			break;
	}
	return link;
}
