#include "commands.h"
#include "error.h"
#include "metadata.h"
#include "state.h"
#include <semaphore.h>
#include <time.h>

static void track_metadata_updated(sp_session *session, void *args) {
	client_t *client = (client_t *)args;
	sem_post(&client->sem);
}

int command_browse_track(sp_session *session, client_t * client, sp_link *link) {

	if (!link)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type track (null).");
	
	if (sp_link_type(link) != SP_LINKTYPE_TRACK) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type track.");
	}

	sp_track *track = sp_link_as_track(link);
	if (!track) {
		sp_link_release(link);
		return command_error(client, ERROR_BROWSE_FAILED, "Failed to create track from link.");
	}
	sp_track_add_ref(track);
	sp_link_release(link);

	// If not loaded? wait for callback.
	if (sp_track_error(track) == SP_ERROR_IS_LOADING)
		metadata_updated_add_callback(&track_metadata_updated, client);

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
		
	// As long as track is not loaded, wait.
	while(sp_track_error(track) == SP_ERROR_IS_LOADING) {
		pthread_mutex_unlock(client->api_mutex);
		// Timeout 1 sec.
		ts.tv_sec += 1;
		sem_timedwait(&client->sem, &ts);
		pthread_mutex_lock(client->api_mutex);
	}

	// Remove callback.
	metadata_updated_remove_callback(&track_metadata_updated, client);

	// Loaded ok, or does track not exists?
	if (sp_track_error(track) != SP_ERROR_OK) {
		sp_track_release(track);
		return command_error(client, ERROR_RESOURCE_NOT_FOUND, "Track not found.");
	}
	
	json_t * result = track_to_json(session, track, 1, 1);
	sp_track_release(track);
	return command_success(client, result);
}
