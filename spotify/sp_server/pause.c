#include "commands.h"
#include "state.h"
#include "main.h"

int command_pause(sp_session *session, client_t * client) {
	playback_set_current(playback()->current_track, playback()->current_index, 0);
	audio_flush();
	sp_session_player_play(session, 0);
	return command_status(session, client);
}
