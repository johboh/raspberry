#include "commands.h"
#include "state.h"
#include "error.h"
#include "main.h"

int command_play(sp_session *session, client_t * client) {
	if (playback()->context != kPlayingNone && playback()->paused && !playback()->loading) {
		playback_set_current(playback()->current_track, playback()->current_index, 1);
		sp_session_player_play(session, 1);
	}
	return command_status(session, client);
}
