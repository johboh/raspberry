#!/usr/bin/env python2
 
import sys
from struct import *
import random
import time
import urllib
import urllib2
import json
import getopt

HOST = '127.0.0.1'
PORT = 666
APIV = 1

def command(cmd, apiv=APIV, dump=True, payload=None):
	url = 'http://%s:%d/%d/%s' % (HOST, PORT, apiv, urllib2.quote(cmd))
	print "%s..." % url
	if payload != None:
		print payload
		clen = len(payload)
		req = urllib2.Request(url, payload, {'Content-Type': 'application/json', 'Content-Length': clen})
	else:
		req = urllib2.Request(url)
	data = urllib2.urlopen(req).read()
	try:
		jsn = json.loads(data)
	except:
		print "Invalid json: %s" % data
		return
	if dump:
		print json.dumps(jsn, sort_keys=True, indent=2)
	else:
		return jsn
	
def print_queue(jsn):
	print "--- Queue"
	for track in jsn['tracks']:
		print "%d: %s%s%s" % (track['index'] if 'index' in track else -1, track['name'], " (playing)" if 'is_playing' in track else "", " (queued)" if 'is_queued' in track else "")
	print "Length: %d" % len(jsn['tracks'])
	
try:
	opts, args = getopt.getopt(sys.argv[1:], "u:p:")
except getopt.GetoptError, err:
	# print help information and exit:
	print str(err) # will print something like "option -a not recognized"
	usage()
	sys.exit(2)
username = 'empty1'
password = 'empty2'
for o, a in opts:
	if o == "-u":
		username = a
	elif o == "-p":
		password = a
	else:
		assert False, "unhandled option"
		
while True:
	n = raw_input(": ")
	
	strs = n.split(' ', 1)
	
	if n == "quit":
		print "Bye bye!"
		exit(1)
	elif n == "login" or n == "l":
		command('login/%s/%s' % (username, password))
	elif n == "status" or n == "s":
		command('status')
	elif n == "queue" or n == "q":
		print_queue(command('queue', APIV, False))
	elif n == "next" or n == "n":
		command('next')
	elif n == "prev" or n == "p":
		command('previous')
	elif n == "pause" or n == "pa":
		command('pause')
	elif n == "play" or n == "pl":
		command('play')
	elif (strs[0] == "play" or strs[0] == "pl") and len(strs) == 2:
		command('%s/play' % strs[1])
	elif (strs[0] == "play" or strs[0] == "pl") and len(strs) == 3:
		command('%s/play/%s' % (strs[1], strs[2]))
	elif (strs[0] == "repeat" or strs[0] == "r") and len(strs) == 2:
		command('repeat/%s' % strs[1])
	elif (strs[0] == "shuffle" or strs[0] == "su") and len(strs) == 2:
		command('shuffle/%s' % strs[1])
	elif (strs[0] == "browse" or strs[0] == "b") and len(strs) == 2:
		command(strs[1])
	elif n == "playlists":
		command('playlists')
	elif strs[0] == "raw" and len(strs) == 2:
		payl = strs[1].split(' ', 2)
		if len(payl) == 2:
			command(payl[0], APIV, True, payl[1])
		else:
			command(payl[0])
	else:
		print "Unknown command %s" % n