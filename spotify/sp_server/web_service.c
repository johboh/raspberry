#include "web_service.h"

#include <errno.h>
#include <unistd.h>          /*  for ssize_t data type  */
#include <string.h>
#include <ctype.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

#include <semaphore.h>
#include <pthread.h>
#include <signal.h>

#define BUFFER_LENGTH         (4096)
#define MAX_PAYLOAD_LENGTH    (21000) // around 512 track uris.

#define MIN(a, b) (a < b ? a : b)

// much code from http://www.paulgriffiths.net/program/c/srcs/webservsrc.html

/// Thread for server socket input.
static pthread_t g_pthread_server;
static void (*g_process_client)(client_t * client);
static int g_server;

typedef struct {
	pthread_t thread;
	int socket;
	int status;
	char * path;
	client_t client;
} thread_data_t;

/*  Read a line from a socket  */
ssize_t readline(int sockd, void *vptr, size_t maxlen) {
	ssize_t n, rc;
	char c, *buffer;

	buffer = vptr;
	
	for (n = 1; n < maxlen; n++) {
		if ( (rc = read(sockd, &c, 1)) == 1) {
			*buffer++ = c;
			if(c == '\n')
				break;
		}
		else if (rc == 0) {
			if (n == 1)
				return 0;
			else
				break;
		}
		else {
			if (errno == EINTR)
				continue;
			printf("Error in readline()\n");
			return 0;
		}
	}

	*buffer = 0;
	return n;
}

/*  Write a line to a socket  */
ssize_t writeline(int sockd, const void *vptr, size_t n) {
	size_t      nleft;
	ssize_t     nwritten;
	const char *buffer;

	buffer = vptr;
	nleft  = n;

	// Write in pages.
	while (nleft > 0) {
		if ( (nwritten = write(sockd, buffer, MIN(nleft, 2048))) <= 0) {
			if (errno == EINTR)
				nwritten = 0;
			else {
				printf("Error in writeline(): %s @ nwritten: %d, nleft: %d. buffer: %p, vptr: %p, size: %d\n", strerror(errno), nwritten, nleft, buffer, vptr, n);
				return 0;
			}
		}
		nleft  -= nwritten;
		buffer += nwritten;
	}

	return n;
}

/*  Removes trailing whitespace from a string  */
int trim(char * buffer) {
	int n = strlen(buffer) - 1;

	while (!isalnum(buffer[n]) && n >= 0)
		buffer[n--] = '\0';

	return 0;
}

// Decode an url, e.g. replace %xx with the corresponding ASCII character.
void urldecode(char * buffer) {
	char asciinum[3] = {0};
	int i = 0, c;

	while (buffer[i]) {
		if (buffer[i] == '+')
			buffer[i] = ' ';
		else if (buffer[i] == '%') {
			asciinum[0] = buffer[i+1];
			asciinum[1] = buffer[i+2];
			buffer[i] = strtol(asciinum, NULL, 16);
			c = i+1;
			do {
				buffer[c] = buffer[c+2];
			} while (buffer[2+(c++)]);
		}
		++i;
	}
}

static int parse_request_path(char *buffer, thread_data_t * data) {
	char *endptr;
	int len;

	/*  Get the request method, which is case-sensitive. This
	    version of the server only supports the GET and POST
	    request method.                                     */
	if (!strncmp(buffer, "GET ", 4)) {
		buffer += 4;
	} else if (!strncmp(buffer, "POST ", 5)) {
		buffer += 5;
	} else {
		data->status = 501;
		return -1;
	}
	
	// Skip to start of path 
	while (*buffer && isspace(*buffer))
		buffer++;
	
	// Calculate string length of path... 
	endptr = strchr(buffer, ' ');
	if (endptr == NULL)
		len = strlen(buffer);
	else
		len = endptr - buffer;
	if (len == 0) {
		data->status = 400;
		return -1;
	}

	/*  ...and store it in the request information structure.  */
	data->path = calloc(len + 1, sizeof(char));
	strncpy(data->path, buffer, len);
	// Decode url.
	urldecode(data->path);
	
	return 0;
}

static int parse_request_payload(thread_data_t * data, char * buffer) {
	// Read all headers first, find the content-length:
	const char * content_length_str = "Content-Length:";
	data->client.payload_length = 0;
	
	int r = 0;
	int max_headers = 25;
	do {
		// Read a header line.
		r = readline(data->socket, buffer, BUFFER_LENGTH - 1);
		trim(buffer);
		// Is this Content-Length:?
		if (!strncasecmp(buffer, content_length_str, strlen(content_length_str))) {
			// Yes!
			data->client.payload_length = atoi(buffer+strlen(content_length_str));
		}
		
		// Read until no more data to read or until a empty new line is found.
		if (strlen(buffer) == 0)
			break;
	} while(r > 0 && max_headers-- > 0);
	
	// No payload?
	if (r == -1 || data->client.payload_length < 1)
		return -1;
	
	// Limit payload size.
	data->client.payload_length = MIN(data->client.payload_length, MAX_PAYLOAD_LENGTH);
	
	// Read payload.
	data->client.payload = malloc(data->client.payload_length);
	int rc = 0;
	int n = 0;
	
	while (n < data->client.payload_length) {
		rc = read(data->socket, data->client.payload + n, data->client.payload_length - n);
		if (rc < 0 && errno != EINTR) {
			// Failure.
			free(data->client.payload);
			data->client.payload_length = 0;
			return -1;
		}
	
		if (rc > 0)
			n += rc;
	}

	return 0;
}

static void handle_client(void *ptr) {
	thread_data_t * data = (thread_data_t *)ptr;
	char buffer[BUFFER_LENGTH] = {0};
	
	data->status = 200;

	// Read first request line.
	readline(data->socket, buffer, BUFFER_LENGTH - 1);
	trim(buffer);
	if (parse_request_path(buffer, data) == -1) {
		// Failure.
		goto end;
	}
	
	if (!strcmp(data->path, "/favicon.ico")) {
		data->status = 404;
		goto end;
	}
	
	// parse
	char * pch = strtok (data->path, "/\\");
	uint32_t i = 0;
	while (pch != NULL)
	{
		data->client.args[i++] = pch;
		pch = strtok (NULL, "/\\");
	}
	data->client.num_args = i;
	data->client.response_length = 0;

	// Read payload.
	parse_request_payload(data, buffer);
	
	// Setup semaphore for the client.
	sem_init(&data->client.sem, 0, 0);
	
	// Process client.
	g_process_client(&data->client);
	
	sem_destroy(&data->client.sem);
	
end:
	// Write back
	// TODO: First check that socket is still available.
	snprintf(buffer, sizeof(buffer), "HTTP/1.0 %d OK\r\n", data->status);
	writeline(data->socket, buffer, strlen(buffer));

	if (data->client.content_type == kContentTypeJson)
		writeline(data->socket, "Content-Type: application/json\r\n", 32);
	else if (data->client.content_type == kContentTypeJpeg)
		writeline(data->socket, "Content-Type: image/jpeg\r\n", 26);
	else if (data->client.content_type == kContentTypeHtml)
		writeline(data->socket, "Content-Type: text/html\r\n", 25);
	else
		writeline(data->socket, "Content-Type: text/plain\r\n", 26);
	writeline(data->socket, "Server: spotirest 1.0\r\n", 23);
	writeline(data->socket, "\r\n", 2);
	writeline(data->socket, data->client.response, data->client.response_length);
	
	// Close/free up
	shutdown(data->socket, SHUT_WR);
	close(data->socket);
	free(data->path);
	if(data->client.response && data->client.response_length > 0)
		free(data->client.response);
	if (data->client.payload && data->client.payload_length > 0)
		free(data->client.payload);
	free(data);
	pthread_exit(NULL);
}

static void server_listen(void *ptr) {
	struct sockaddr_in sa_client;
	socklen_t length;
    struct timeval tv;
    fd_set fds;
    int rc;

	/* Loop forever. */
	while(1) {
		FD_ZERO(&fds);
		FD_SET(g_server, &fds);
		tv.tv_sec  = 3600;
		tv.tv_usec = 0;
		if ((rc = select(FD_SETSIZE, &fds, 0, 0, &tv)) < 0) {
			printf("server: select() failed.\n");
			break;
		}

		if (rc == 0 || FD_ISSET(g_server, &fds) == 0) {
			continue;
		}

		/* Accept a new connection. */
		length = sizeof(sa_client);
		thread_data_t * data = (thread_data_t *)malloc(sizeof(thread_data_t));
		memset(data, 0, sizeof(thread_data_t));
		data->socket = accept(g_server, (struct sockaddr *) &sa_client, &length);
		if (data->socket == EINTR) {
			printf("Server accept interrupted.\n");
			free(data);
			break;
		} else if (data->socket == -1) {
			free(data);
			continue;
		}

		// Create thread deattached as we are not waiting for it.
		pthread_attr_t tattr;
		pthread_attr_init(&tattr);
		pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_DETACHED);
		
		int r = pthread_create(&data->thread, &tattr, (void *) &handle_client, data);
		if (r == 0)
			pthread_setname_np(data->thread, "client socket");
		else {
			close(data->socket);
			free(data);
			printf("Error when creating thread: %s\n", strerror(errno));
		}
	}
	close(g_server);
}

int web_service_init(uint16_t port, void (*process_client)(client_t * client)) {
	g_process_client = process_client;
	
	struct sockaddr_in sa_server;
	struct in_addr addr_local;
	
	// Create a new TCP connection handle.
	if ((g_server = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		printf("server: Failed to create socket.\n");
		return -1;
	}

	addr_local.s_addr = INADDR_ANY;
	sa_server.sin_family = AF_INET;
	sa_server.sin_port = htons(port);
	sa_server.sin_addr = addr_local;
	
	/* Bind the connection to port on any
	local IP address. */
	if (bind(g_server, (struct sockaddr *) &sa_server, sizeof(sa_server)) < 0) {
		printf("server: Failed to bind socket.\n");
		return -1;
	}
	
	/* Put the connection into LISTEN state. */
	if (listen(g_server, 2) != 0) {
		printf("server: Failed to listen on socket.\n");
		return -1;
	}
	printf("server: Server listening on %d...\n", port);

	pthread_create(&g_pthread_server, NULL, (void *) &server_listen, NULL);
	pthread_setname_np(g_pthread_server, "server socket");
	return 0;
}

void web_service_close() {
	printf("server: exiting...\n");
	pthread_kill(g_pthread_server, SIGINT);
	pthread_join(g_pthread_server, NULL);
}