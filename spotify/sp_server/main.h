#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdint.h>

void audio_flush(void);

void set_position(uint32_t position_ms);
uint32_t get_position(void);

#endif // __MAIN_H__
