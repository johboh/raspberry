#include "commands.h"
#include "state.h"
#include "error.h"
#include "equal.h"
#include <string.h>

static void playback_album_browse_complete(sp_albumbrowse *browse, void * userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

int command_play_album(sp_session *session, client_t * client, sp_link *link, uint32_t index) {

	if (!link)
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type album (null).");
	
	if (sp_link_type(link) != SP_LINKTYPE_ALBUM) {
		sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "Link type is not of type album.");
	}

	// Free up resource if switching to new playback type and/or album.
	uint8_t is_same = isCurrentAlbum(link);
	if (!is_same)
		stop_and_release_playback(session);
	
	playback()->loading = 1;
	
	// Create new browse request.
	if (!is_same || !playback()->album_browse) {
		sp_albumbrowse * album_browse = sp_albumbrowse_create(session, sp_link_as_album(link), &playback_album_browse_complete, client);
		if (!album_browse) {
			sp_link_release(link);
			return command_error(client, ERROR_BROWSE_FAILED, "Failed to create album browse request.");
		}
		playback_set(kPlayingAlbum, album_browse);
	}
	sp_link_release(link);
	sp_albumbrowse * browse = playback()->album_browse;
	
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	
	// If not loaded? wait for callback.
	while(sp_albumbrowse_error(browse) == SP_ERROR_IS_LOADING) {
		pthread_mutex_unlock(client->api_mutex);
		// Timeout 1 sec.
		ts.tv_sec += 1;
		sem_timedwait(&client->sem, &ts);
		pthread_mutex_lock(client->api_mutex);
	}
	
	// Check for browse error, like non existing resource.
	sp_error err = sp_albumbrowse_error(browse);
	if (err != SP_ERROR_OK) {
		stop_and_release_playback(session);
		if (err == SP_ERROR_OTHER_PERMANENT)
			return command_error(client, ERROR_RESOURCE_NOT_FOUND, sp_error_message(err));
		else
			return command_error(client, ERROR_BROWSE_FAILED, sp_error_message(err));
	}

	int num_tracks = sp_albumbrowse_num_tracks(browse);
	
	// Minimum 0 and maximum num_tracks.
	if (index < 0)
		index = 0;
	else if (index >= num_tracks)
		index = num_tracks;
	
	// Free old track, if any.
	if (is_same && playback()->current_track) {
		sp_track_release(playback()->current_track);
		playback()->current_track = NULL;
	}

	if (!is_same)
		generate_mapping(num_tracks);
	
	// Try to start playback.
	load_and_play_next_available_track(session, index, 0);
	playback()->loading = 0;
	
	return command_status(session, client);
}
