#include "commands.h"
#include "state.h"
#include "settings.h"

int command_repeat(sp_session *session, client_t * client, uint8_t enable) {
	repeat_set(enable ? 1 : 0);
	// Save settings.
	sp_user * user = sp_session_user(session);
	if (user)
		settings_save(sp_user_canonical_name(user));
	return command_status(session, client);
}
