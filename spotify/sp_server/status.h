#ifndef __STATUS_H__
#define __STATUS_H__

#include <stdint.h>

#include <libspotify/api.h>
#include "client.h"

sp_link * current_context();

#endif // __STATUS_H__