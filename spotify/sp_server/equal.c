#include "equal.h"
#include "state.h"
#include <string.h>

int isCurrentAlbum(sp_link * link) {
	uint8_t is_same = playback()->context == kPlayingAlbum && playback()->album_browse;
	if (is_same) {
		sp_link * other_link = sp_link_create_from_album(sp_albumbrowse_album(playback()->album_browse));
		char uri[512];
		char other_uri[512];
		sp_link_as_string(link, uri, sizeof(uri));
		sp_link_as_string(other_link, other_uri, sizeof(other_uri));
		is_same = !strcmp(uri, other_uri);
		sp_link_release(other_link);
	}
	return is_same;
}

int isCurrentSearch(sp_link * link) {
	uint8_t is_same = playback()->context == kPlayingSearch && playback()->search;
	if (is_same) {
		sp_link * other_link = sp_link_create_from_search(playback()->search);
		char uri[512];
		char other_uri[512];
		sp_link_as_string(link, uri, sizeof(uri));
		sp_link_as_string(other_link, other_uri, sizeof(other_uri));
		is_same = !strcmp(uri, other_uri);
		sp_link_release(other_link);
	}
	return is_same;
}

int isCurrentPlaylist(sp_link * link, sp_playlist *playlist) {
	uint8_t is_same = playback()->context == kPlayingPlaylist && playback()->playlist;
	if (is_same) {
		char uri[512];
		char other_uri[512];
	
		if (playlist) {
			sp_link *link = sp_link_create_from_playlist(playlist);
			sp_link_as_string(link, uri, sizeof(uri));
			sp_link_release(link);
		} else {
			sp_link_as_string(link, uri, sizeof(uri));
		}
	
		sp_link * other_link = sp_link_create_from_playlist(playback()->playlist);
		sp_link_as_string(other_link, other_uri, sizeof(other_uri));
		is_same = !strcmp(uri, other_uri);
		sp_link_release(other_link);
	}
	return is_same;
}

