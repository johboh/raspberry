#include "settings.h"
#include "state.h"
#include <jansson.h>
#include <string.h>
#include "scrobble.h"

char BASE_PATH[1024];

void settings_init(const char *base_path) {
	if (!base_path)
		return;

	strncpy(BASE_PATH, base_path, sizeof(BASE_PATH));
}

void compose_path(char *path, const char *username) {
	strcpy(path, BASE_PATH);
	strcat(path, "/");
	strcat(path, username);
	strcat(path, ".settings");
}

void settings_load(const char *username) {
	if (!username)
		return;

	char path[1024];
	compose_path(path, username);
	json_error_t error;
	json_t *settings = json_load_file(path, JSON_DISABLE_EOF_CHECK, &error);
	if (settings) {
		json_t * json_repeat = json_object_get(settings, "repeat");
		if (json_repeat)
			repeat_set(json_integer_value(json_repeat));

		json_t * json_shuffle = json_object_get(settings, "shuffle");
		if (json_shuffle)
			shuffle_set(json_integer_value(json_shuffle));

		// Last.FM settings.
		json_t * json_lastfm = json_object_get(settings, "lastfm");
		if (json_lastfm) {
			json_t * json_username = json_object_get(json_lastfm, "username");
			if (json_username)
				scrobble_lastfm_set_username(json_string_value(json_username));
		}

		json_decref(settings);
	}
}
void settings_save(const char *username) {
	if (!username)
		return;

	json_t *settings = json_object();
	json_object_set_new(settings, "shuffle", json_integer(shuffle()));
	json_object_set_new(settings, "repeat", json_integer(repeat()));
	// Last.FM settings.
	json_t *lastfm = json_object();
	json_object_set_new(lastfm, "username", json_string(scrobble_lastfm_username()));
	json_object_set_new(settings, "lastfm", lastfm);
	char path[1024];
	compose_path(path, username);
	json_dump_file(settings, path, JSON_COMPACT);
	json_decref(settings);
}
