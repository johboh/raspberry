#include "commands.h"
#include "state.h"
#include "error.h"
#include "state.h"
#include "metadata.h"
#include "limits.h"
#include "status.h"

int command_queue(sp_session *session, client_t * client, int explicit_limit, int future_limit) {
	json_t * result = json_object();

	json_t *tracks = json_array();
	uint32_t i = 0;

	uint32_t count = UINT_MAX;

	if (explicit_limit >= 0) {
		count = explicit_limit;
	}

	// Add current playing track, but not if in queue. Then it will be added below.
	if (playback()->context != kPlayingNone && playback()->current_track != NULL && count > 0) {
		queue_entry_t *entry = queue_get_entry(0);
		if (queue_num_tracks() == 0 || !entry || !entry->track || entry->track != playback()->current_track) {
			json_t * result_track = track_to_json(session, playback()->current_track, 1, 1);
			json_object_set_new(result_track, "is_playing", json_integer(1));
			json_array_append_new(tracks, result_track);
			count--;
		}
	}

	// Print queued tracks.
	for (i = 0; i < queue_num_tracks() && count > 0; i++) {
		queue_entry_t *entry = queue_get_entry(i);
		// Dont add empty track.
		if (!entry || !entry->track)
			continue;
		json_t * result_track = track_to_json(session, entry->track, 1, 1);
		json_object_set_new(result_track, "index", json_integer(i));
		json_object_set_new(result_track, "is_queued", json_integer(1));
		if (entry->metadata) {
			json_object_set_new(result_track, "metadata", json_string(entry->metadata));
		}
		if (entry->timestamp > 0) {
			json_object_set_new(result_track, "timestamp", json_integer(entry->timestamp));
		}
		json_array_append_new(tracks, result_track);
		count--;
	}

	// future_limit = -2: No future limit specified, use explicit limit as a total limit.
	if (future_limit >= -1) {
		count = UINT_MAX;
		if (future_limit >= 0) {
			count = future_limit;
		}
	}

	// Get all tracks left until end of context.
	for(i = playback()->current_index; i < get_context_num_tracks() && count > 0; i++) {
		sp_track *track = get_context_track(get_mapping(i));
		// Dont add already added playing track.
		if (i == playback()->current_index && playback()->current_track != NULL && track == playback()->current_track)
			continue;
		json_t * result_track = track_to_json(session, track, 1, 1);
		json_array_append_new(tracks, result_track);
		count--;
	}
	// If repeat, start over and get tracks up to (but not including) this track.
	if (repeat()) {
		for(i = 0; i < playback()->current_index && count-- > 0; i++) {
			sp_track *track = get_context_track(get_mapping(i));
			json_t * result_track = track_to_json(session, track, 1, 1);
			json_array_append_new(tracks, result_track);
		}
	}
	json_object_set_new(result, "tracks", tracks);
	json_object_set_new(result, "playing", json_integer(!playback()->paused));
	sp_link *link = current_context();
	if (link != NULL) {
		char str[512];
		sp_link_as_string(link, str, sizeof(str));
		json_object_set_new(result, "context", json_string(str));
		sp_link_release(link);
	}

	return command_success(client, result);
}

int command_queue_add(sp_session *session, client_t * client, sp_link * link, uint32_t index, char * metadata) {
	if (!link || sp_link_type(link) != SP_LINKTYPE_TRACK) {
		if (link)
			sp_link_release(link);
		return command_error(client, ERROR_INVALID_LINK_TYPE, "The track uri in /queue/add/<track uri> is not a track.");
	}

	int r = queue_add(sp_link_as_track(link), index, metadata);
	sp_link_release(link);
	if (r != 0)
		return command_error(client, ERROR_UNKNOWN, "Failed to add track to queue.");
	else
		return command_status(session, client);
}

int command_queue_remove(sp_session *session, client_t * client, int index) {
	int r = queue_remove(index);
	if (r == 0)
		return command_status(session, client);
	else if (r == -1)
		return command_error(client, ERROR_INDEX_OUT_OF_BOUNDS, "Index is out of bounds.");
	else if (r == -2)
		return command_error(client, ERROR_TRACK_IS_PLAYING, "Unable to remove current playing track from play queue.");
	else
		return command_error(client, ERROR_UNKNOWN, "Failed to remove track from queue.");
}

int command_queue_clear(sp_session *session, client_t * client) {
	queue_clear();
	return command_status(session, client);
}