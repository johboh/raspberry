#include "commands.h"
#include "metadata.h"
#include "error.h"
#include "equal.h"
#include "state.h"
#include <semaphore.h>

static void search_complete(sp_search *result, void * userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

int command_search(sp_session *session, client_t * client, const char * query, uint32_t num_tracks, uint32_t num_albums, uint32_t num_artists, uint32_t num_playlists) {
	sp_search * search = sp_search_create(session, query, 0, num_tracks, 0, num_albums, 0, num_artists, 0, num_playlists, SP_SEARCH_STANDARD, &search_complete, client);
	if (!search)
		return command_error(client, ERROR_SEARCH_FAILED, "Failed to create search request.");

	// If not loaded? wait for callback.
	while(!sp_search_is_loaded(search)) {
		pthread_mutex_unlock(client->api_mutex);
		sem_wait(&client->sem);
		pthread_mutex_lock(client->api_mutex);
	}

	sp_error err = sp_search_error(search);
	if (err != SP_ERROR_OK) {
		sp_search_release(search);
		return command_error(client, ERROR_SEARCH_FAILED, sp_error_message(err));
	}

	json_t * result = json_object();
	uint32_t i = 0;

	sp_link *link = sp_link_create_from_search(search);
	uint8_t isCurrentContext = isCurrentSearch(link);
	sp_link_release(link);

	// Search metadata.
	json_object_set_new(result, "did_you_mean", json_string(sp_search_did_you_mean(search)));
	json_object_set_new(result, "query", json_string(sp_search_query(search)));

	json_t *tracks = json_array();
	for(i = 0; i < sp_search_num_tracks(search); i++) {
		sp_track *track = sp_search_track(search, i);
		json_t * result_track = track_to_json(session, track, 1, 1);
		json_object_set_new(result_track, "index", json_integer(i));
		if (isCurrentContext && playback()->current_index == i)
			json_object_set_new(result_track, "is_playing", json_integer(1));
		json_array_append_new(tracks, result_track);
	}
	json_object_set_new(result, "tracks", tracks);

	json_t *albums = json_array();
	for(i = 0; i < sp_search_num_albums(search); i++) {
		sp_album *album = sp_search_album(search, i);
		json_t * result_album = album_to_json(album, 1);
		json_object_set_new(result_album, "index", json_integer(i));
		json_array_append_new(albums, result_album);
	}
	json_object_set_new(result, "albums", albums);

	json_t *artists = json_array();
	for(i = 0; i < sp_search_num_artists(search); i++) {
		sp_artist *artist = sp_search_artist(search, i);
		json_t * result_artist = artist_to_json(artist);
		json_object_set_new(result_artist, "index", json_integer(i));
		json_array_append_new(artists, result_artist);
	}
	json_object_set_new(result, "artists", artists);

	json_t *playlists = json_array();
	for(i = 0; i < sp_search_num_playlists(search); i++) {
		sp_playlist *playlist = sp_search_playlist(search, i);
		json_t * result_playlist = playlist_to_json(session, playlist);
		json_object_set_new(result_playlist, "index", json_integer(i));
		json_array_append_new(playlists, result_playlist);
	}
	json_object_set_new(result, "playlists", playlists);

	sp_search_release(search);
	return command_success(client, result);
}
