#include "commands.h"
#include "error.h"
#include "client.h"
#include <string.h>

static void image_loaded(sp_image *image, void *userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

static void album_browse_complete(sp_albumbrowse *result, void * userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

static void artist_browse_complete(sp_artistbrowse *result, void * userdata) {
	client_t *client = (client_t *)userdata;
	sem_post(&client->sem);
}

int command_image(sp_session *session, client_t * client, sp_link * link, sp_image_size image_size) {
	sp_linktype type = sp_link_type(link);

	const byte* image_id = NULL;
	switch(type) {
		case SP_LINKTYPE_ALBUM: {
			sp_album * album = sp_link_as_album(link);
			image_id = sp_album_cover(album, image_size);
			// Already have id? Ok!
			if (image_id) {
				sp_link_release(link);
				break;
			}
			
			// Must browse for album id.
			sp_albumbrowse * browse = sp_albumbrowse_create(session, album, &album_browse_complete, client);
			sp_link_release(link);
			if (!browse)
				return command_error(client, ERROR_BROWSE_FAILED, "Failed to create album image browse request.");
			
			// If not loaded? wait for callback.
			while(sp_albumbrowse_error(browse) == SP_ERROR_IS_LOADING) {
				pthread_mutex_unlock(client->api_mutex);
				sem_wait(&client->sem);
				pthread_mutex_lock(client->api_mutex);
			}
			
			// Check for browse error, like non existing resource.
			sp_error err = sp_albumbrowse_error(browse);
			if (err != SP_ERROR_OK) {
				sp_albumbrowse_release(browse);
				if (err == SP_ERROR_OTHER_PERMANENT)
					return command_error(client, ERROR_RESOURCE_NOT_FOUND, sp_error_message(err));
				else
					return command_error(client, ERROR_BROWSE_FAILED, sp_error_message(err));
			}
			
			// Get id again.
			image_id = sp_album_cover(sp_albumbrowse_album(browse), image_size);
			break;
		}
		case SP_LINKTYPE_ARTIST: {
			sp_artist * artist = sp_link_as_artist(link);
			image_id = sp_artist_portrait(artist, image_size);
			// Already have id? Ok!
			if (image_id) {
				sp_link_release(link);
				break;
			}
	
			// Must browse for artist image id.
			sp_artistbrowse * browse = sp_artistbrowse_create(session, artist, SP_ARTISTBROWSE_NO_ALBUMS, &artist_browse_complete, client);
			sp_link_release(link);
			if (!browse)
				return command_error(client, ERROR_BROWSE_FAILED, "Failed to create artist image browse request.");
	
			// If not loaded? wait for callback.
			while(sp_artistbrowse_error(browse) == SP_ERROR_IS_LOADING) {
				pthread_mutex_unlock(client->api_mutex);
				sem_wait(&client->sem);
				pthread_mutex_lock(client->api_mutex);
			}

			// Check for browse error, like non existing resource.
			sp_error err = sp_artistbrowse_error(browse);
			if (err != SP_ERROR_OK) {
				sp_artistbrowse_release(browse);
				if (err == SP_ERROR_OTHER_PERMANENT)
					return command_error(client, ERROR_RESOURCE_NOT_FOUND, sp_error_message(err));
				else
					return command_error(client, ERROR_BROWSE_FAILED, sp_error_message(err));
			}	
	
			// Get id again.
			if (sp_artistbrowse_num_portraits(browse) > 0)
				image_id = sp_artistbrowse_portrait(browse, 0);
			sp_artistbrowse_release(browse);
			break;
		}
		default:
		sp_link_release(link);
		return command_error(client, ERROR_UNKNOWN_COMMAND, "Image not available for this link type.");
		// TODO: Add support for playlist.
		// TODO: Add support for profile.
	}
	
	if (!image_id)
		return command_error(client, ERROR_BROWSE_FAILED, "Image id is null.");
		
	// Create image.
	sp_image * image = sp_image_create(session, image_id);
	if (!image)
		return command_error(client, ERROR_BROWSE_FAILED, "Failed to create image load request.");

	// Add callback.
	sp_image_add_load_callback(image, &image_loaded, client);
	
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	
	// If not loaded? wait for callback.
	while(sp_image_error(image) == SP_ERROR_IS_LOADING) {
		pthread_mutex_unlock(client->api_mutex);
		// Timeout 1 sec.
		ts.tv_sec += 1;
		sem_timedwait(&client->sem, &ts);
		pthread_mutex_lock(client->api_mutex);
	}
	
	// Remove callback.
	sp_image_remove_load_callback(image, &image_loaded, client);
	
	const void* data = sp_image_data(image, &client->response_length);
	
	client->content_type = kContentTypeJpeg;
	client->response = malloc(client->response_length);
	memcpy(client->response, data, client->response_length);

	sp_image_release(image);
	return 0;
}
