#include "commands.h"

int command_seek(sp_session *session, client_t * client, uint32_t offset_ms) {
	sp_session_player_seek(session, offset_ms);
	set_position(offset_ms);
	return command_status(session, client);
}
