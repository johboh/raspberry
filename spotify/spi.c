#include "spi.h"
#include <bcm2835.h>

void spi_init() {
	bcm2835_spi_begin();
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // The default
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // The default
	bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_512);   // 512 = 2us = 500kHz
	bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // CS is CS0
	bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);
}

void spi_close() {
	bcm2835_spi_end();
}