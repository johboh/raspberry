#include "input.h"
#include <bcm2835.h>

void input_init(input_t * inputs, uint8_t length) {
	if (!inputs) {
		printf("inputs is null!\n");
		return;
	}

	// Setup input pins.
	uint8_t i = 0;
	for(i = 0; i < length; i++) {
		input_t *input = &inputs[i];
	
		// Setup pin as input.
		bcm2835_gpio_fsel(input->pin, BCM2835_GPIO_FSEL_INPT);
		// With pullup
		bcm2835_gpio_set_pud(input->pin, BCM2835_GPIO_PUD_UP);
		// Clear low level detection, as it will hang (bug).
		// See http://www.open.com.au/mikem/bcm2835/index.html
		bcm2835_gpio_clr_len(input->pin);
		
		input->index = i;
	}
}

input_t * input_wait_any(input_t * inputs, uint8_t length, uint32_t timeout_ms) {
	if (!inputs) {
		printf("inputs is null!\n");
		return NULL;
	}

	const uint32_t delay_ms = 50;
	
	// block until any input is active.
	input_t * input = 0;
	uint32_t delayed = 0;
	while(1) {
		if(timeout_ms > 0 && delayed > timeout_ms)
			break;
	
		input = input_read(inputs, length);
		if (input)
			break;

		delay(delay_ms);
		delayed+=delay_ms;
	}

	return input;
}

input_t * input_read(input_t * inputs, uint8_t length) {
	if (!inputs) {
		printf("inputs is null!\n");
		return NULL;
	}

	uint8_t i = 0;
	input_t * input = 0;
	
	for(i = 0; i < length; i++) {
		input_t *input = &inputs[i];
		if (!bcm2835_gpio_lev(input->pin))
			return input;
	}

	return input;
}