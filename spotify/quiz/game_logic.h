#ifndef __GAME_LOGIC_H__
#define __GAME_LOGIC_H__

#include <stdio.h>
#include <stdint.h>

#include <libspotify/api.h>

#define NUM_QUESTIONS 7
#define MAX_ALTERNATIVES 4

/**
* Print available genres to the user and wait for input/selection.
* blocking, then search and generate questions based on the
* search result.
* The game will then start.
*/
void start_game(sp_session *session);

#endif // __GAME_LOGIC_H__
