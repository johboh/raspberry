#include "game_logic.h"
#include "hardware.h"

#include <stdio.h>
#include <string.h>

#define MIN(a,b) (((a)<(b))?(a):(b))

#define ASSERT(condition, str) \
{ if (!(condition)) { printf("Assertion failed, %s@%d: %s\n", __FILE__, __LINE__, str); exit(1); } }

#define QUESTION_TIMEOUT_S 15

static void SP_CALLCONV search_complete(sp_search *search, void *userdata);

static sp_session *g_sess = NULL;

typedef struct {
	uint8_t number;
	sp_track *track;
	sp_artist *correct;
	uint8_t num_alternatives;
	sp_artist **alternatives;
} question_t;

typedef struct {
	uint8_t score;
	uint8_t current_question;
	uint8_t num_questions;
	question_t * questions;
} game_t;

typedef struct {
    char code[256];
    char name[256];
} genre_t;

/**
* True random, yep! Using the high bits.
*/
uint32_t true_random(uint32_t max) {
	return (int) ((double)max * (rand() / (RAND_MAX + 1.0))); 
}
/**
* Return !=0 if the artist exists as a answer in any of the previous questions.
*/
int artist_exists(sp_artist * artist, question_t * questions, uint8_t num_questions) {
	ASSERT(artist, "artist_exists(artist is null!);");
	ASSERT(questions, "artist_exists(questions is null!);");

	uint8_t i = 0;
	for(i = 0; i < num_questions; i++) {
		question_t * question = &questions[i];
		if (!question || !question->correct)
			continue;
			
		if (strcmp(sp_artist_name(artist), sp_artist_name(question->correct)) == 0)
			return 1;
	}
	
	return 0;
}

/**
* Print available genres to the user and wait for input/selection.
* blocking.
* \param genres a list of genres that the user can select from.
* \param length the length of the list.
* \return a pointer to the selected genre.
*/
static genre_t * get_genre(genre_t * genres, uint8_t length) {
	ASSERT(genres, "get_genre(genres is null!);");
	
	uint8_t i = 0;
	
	display_clear();
	
	// Initial, select genre.
	display_write_row(1, "Select genre:");
	for(i = 0; i < length; i++)
		display_write_row(i + 2, genres[i].name);

	// Wait for input.
	i = hardware_wait_button();
	ASSERT(i < length, "get_genre(): read input is out of range.");
	
    genre_t * genre = &genres[i];
	return genre;
}

/**
* Search spotify backend for tracks using a specific genre.
* This will eventually generate questions.
* \param genre the genre to search for.
* \param num_questions number of questions to generate.
*/
static void do_genre_search(genre_t *genre, uint8_t num_questions) {
	ASSERT(genre, "do_genre_search(genre is null!);");

	// Search for tracks.
	int tracks = 200;
	int albums = 0;
	int artists = 0;
	sp_search_create(g_sess, genre->code, 0, tracks, 0, albums, 0, artists, &search_complete, &num_questions);
}

void start_game(sp_session *session) {
	ASSERT(session, "start_game(session is null!);");

	g_sess = session;

	// Let user choose genre.
	genre_t genres[4] = {
		{"genre:pop", "Pop"},
		{"genre:\"Rock & Roll\"", "Rock n Roll"},
		{"genre:\"Singer/Songwriter\"", "Singer/Songwriter"},
		{"genre:\"Club/Dance\"", "Club/Dance"},
	};
	
	genre_t * genre = get_genre(genres, sizeof(genres) / sizeof(genre_t));
	ASSERT(genre, "start_game(); get_genre() == null!");
	
	display_clear();
	display_write_row(1, "Selected genre:");
	display_write_row(2, genre->name);
	display_write_row(4, "Generating questions, please wait...");
	
	// Create initial data set fo generate questions from.
	do_genre_search(genre, NUM_QUESTIONS);
}

/**
* Play the question (the music) and print the 
* alternatives to the user.
*/
static void ask_question(game_t * game) {
	ASSERT(game, "ask_question(game is null!);");

   // Write to display.
    display_clear();

	// Get current question.
	question_t * question = &game->questions[game->current_question];
	ASSERT(question, "ask_question(); current question == null!");
	
	// Write alternatives.
	uint8_t i = 0;
    for(i = 0; i < question->num_alternatives; i++) {
		if (!question->alternatives[i]) {
			printf("ask_question(); question->alternatives[%d] == null!\n", i);
			continue;
		}
		display_write_row(i + 2, sp_artist_name(question->alternatives[i]));
	}
		
	ASSERT(question->track, "ask_question(); question->track == null!");
	
	// Play music.
	sp_error err = sp_track_error(question->track);
	if (err != SP_ERROR_OK) {
		printf("ERR! %s: \n", sp_error_message(err));
	} else {
		sp_session_player_load(g_sess, question->track);
		sp_session_player_play(g_sess, 1);
	}
}

/**
* Wait for the user to answer the question.
* blocking.
* When answer detected, print correct or wrong message.
*/
static void handle_answer(game_t * game) {
	ASSERT(game, "handle_answer(game is null!);");

	// Get current question.
	question_t * question = &game->questions[game->current_question];
	ASSERT(question, "handle_answer(); current question == null!");

    // Write header.
	char str[64];
	int8_t answer = 0;
	int8_t timeout = QUESTION_TIMEOUT_S;
	do {
		snprintf(str, sizeof(str), "%2d  Who? %d/%d", timeout, game->current_question+1, game->num_questions);
		display_write_row_noclear(1, str);
		
		// Wait for answer.
		answer = hardware_wait_button_tmo(1000 /* 1 sec */);
		if (answer != WAIT_TIMEOUT)
			break;
			
		// Timeout.
	} while(--timeout > 0);
	
	
	answer = MIN(answer, question->num_alternatives);
	
    display_clear();
	
	ASSERT(question->correct, "handle_answer(); question->correct == null!");
	ASSERT(question->alternatives[answer], "handle_answer(); question->alternatives[answer] == null!");

	if (timeout > 0 && strcmp(sp_artist_name(question->correct), sp_artist_name(question->alternatives[answer])) == 0) {
		display_write_row(1, "Correct!");
		game->score++;
	} else {
		if (timeout == 0)
			display_write_row(1, "Timeout!");
		else
			display_write_row(1, "Wrong!");
		display_write_row(2, "Correct:");
		display_write_row(3, sp_artist_name(question->correct));
	}
    
	game->current_question++;
}

/**
* Check if game is over. If over, print game result.
* Also cleanup game variables.
*
*/
static int check_game_end(game_t * game) {
	ASSERT(game, "check_game_end(game is null!);");

	display_clear();

	if (game->current_question >= game->num_questions) {
		display_write_row(1, "Game over!");
		display_write_row(2, "Score:");

		char str[64];
		sprintf(str, "%d/%d", game->score, game->num_questions);
		display_write_row(3, str);

		// Stop playback.
		sp_session_player_unload(g_sess);
		
		// Cleanup!
		uint8_t i = 0;
		uint8_t j = 0;
		for(i = 0; i < game->num_questions; i++) {
			question_t * question = &game->questions[i];
			sp_track_release(question->track);
			sp_artist_release(question->correct);
			
			for(j = 0; j < question->num_alternatives; j++) {
				sp_artist_release(question->alternatives[j]);
			}
		}
		
		free(game->questions);
		game->questions = NULL;
		
		free(game);
		game = NULL;
		
		
		return 1;
	} 
	return 0;
}

/**
* Callback when an artist browse is complete.
* When complete, get releated artists as answers.
* Then ask question and wait for answer.
* Check for game end or goto next question.
*/
static void browse_artist_callback(sp_artistbrowse *browse, void *userdata) {
	if (sp_artistbrowse_error(browse) != SP_ERROR_OK) {
		fprintf(stderr, "Failed to browse artist: %s\n",
			sp_error_message(sp_artistbrowse_error(browse)));
		sp_artistbrowse_release(browse);
		return;
	}

	game_t * game = (game_t*)userdata;
	ASSERT(game, "browse_artist_callback(): game is null!");
	
	// Get new current question.
	question_t * question = &game->questions[game->current_question];
	ASSERT(question, "browse_artist_callback(); current question == null!");
	
	uint8_t i = 0;
	
	question->num_alternatives = MIN(MAX_ALTERNATIVES, sp_artistbrowse_num_similar_artists(browse));
	
	size_t size = sizeof(sp_artist**) * question->num_alternatives;
	question->alternatives = (sp_artist **)malloc(size);
	ASSERT(question->alternatives, "browse_artist_callback(); question->alternatives is null!");
	memset(question->alternatives, 0, size);
	
	// Randomize correct answer.
	uint8_t correct = true_random(question->num_alternatives);
	sp_artist_add_ref(question->alternatives[correct] = sp_artistbrowse_artist(browse));

	ASSERT(question->alternatives[correct], "browse_artist_callback(); question->alternatives[correct] is null!");
	
	// Get releated artists.
	for(i = 0; i < question->num_alternatives; i++) {
		if (i != correct) {
			sp_artist_add_ref(question->alternatives[i] = sp_artistbrowse_similar_artist(browse, i));
			ASSERT(question->alternatives[i], "browse_artist_callback(); question->alternatives[i] is null!");
		}
	}
	
	sp_artistbrowse_release(browse);
	
	// Ask questions as long as we have questions to ask.
	ask_question(game);
	handle_answer(game);
	hardware_delay(2000);
		
	// Last question?
	if (check_game_end(game)) {
		display_write_row(4, "Press any key to restart!");
		hardware_wait_button();
		display_clear();
		hardware_delay(250);
		start_game(g_sess);
	} else {
		// Continue.
		// Get new current question.
		question_t * question = &game->questions[game->current_question];
		ASSERT(question, "browse_artist_callback(); new current question == null!");
		ASSERT(question->correct, "browse_artist_callback(); new current question->correct == null!");
		
		// Get answers/alternatives for the next
		sp_artistbrowse_create(g_sess, question->correct, SP_ARTISTBROWSE_NO_ALBUMS, browse_artist_callback, game);
	}
}

/**
* For a set of questions, start a new game.
* \param questions generated questions.
* \param num_questions number of generated questions.
*/
static void new_game(question_t *questions, uint8_t num_questions) {
	if (num_questions < 1) {
		display_clear();
		display_write_row(1, "no questions");
		display_write_row(2, "found :(");
		fprintf(stderr, "No questions found.");
		return;
	}

	ASSERT(questions, "new_game(questions == null!)");
	
	// Create new game.
	game_t * game = (game_t*)malloc(sizeof(game_t));
	ASSERT(game, "new_game(); allocated game == null!");
	memset(game, 0, sizeof(game_t));
	game->num_questions = num_questions;
	game->questions = questions;
	
	// Get answers/alternatives for the first question.
	sp_artistbrowse_create(g_sess, questions[0].correct, SP_ARTISTBROWSE_NO_ALBUMS, browse_artist_callback, game);
}

/**
* Callback when the initial genre search is completed.
* Allocated questions and create the correct answer for each question.
* Then start a new game.
*/
static void SP_CALLCONV search_complete(sp_search *search, void *userdata) {

	if (sp_search_error(search) != SP_ERROR_OK) {
		display_clear();
		display_write_row(1, "Generation failed");
		display_write_row(3, sp_error_message(sp_search_error(search)));
		fprintf(stderr, "Failed to search: %s\n", sp_error_message(sp_search_error(search)));
		sp_search_release(search);
		return;
	}
	
	// Allocate questions.
	uint8_t num_questions = *((uint8_t*)userdata);
	size_t size = sizeof(question_t) * num_questions;
	question_t * questions = (question_t*)malloc(size);
	ASSERT(questions, "search_complete(): allocated questions is null!\n");
	memset(questions, 0, size);

	uint32_t total_tracks = 200; // ERR: reporting wrong? 33k tracks...  sp_search_total_tracks(search);
	
	// Loop until all questions found, or 
	// until out of tracks.
	uint8_t questions_found = 0;
	uint32_t track_tried = 0;
	while(questions_found < num_questions && track_tried < total_tracks) {
		// Get random track.
		uint32_t index = true_random(total_tracks);
		sp_track * track = sp_search_track(search, index);
		// Todo: We should remove the used track from the search result.
		track_tried++;
		
		if (!track) {
			printf("search_complete(): track in loop is null\n");
			continue;
		}
	
		sp_artist * artist = sp_track_artist(track, 0);
		
		if (!artist) {
			printf("search_complete(): artist in loop is null\n");
			continue;
		}

		// Does artist already exists?
		if (artist_exists(artist, questions, num_questions)) {
			// Yes, continue on.
			continue;
		}
		
		questions[questions_found].number  = questions_found;
		sp_track_add_ref(questions[questions_found].track = track);
		sp_artist_add_ref(questions[questions_found].correct = artist);
		
		questions_found++;
	}

	new_game(questions, questions_found);

	sp_search_release(search);
}