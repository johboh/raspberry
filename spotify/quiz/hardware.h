#ifndef __HARDWARE_H__
#define __HARDWARE_H__

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// 3310 display specific pins.
#define PIN_DISPLAY_3310_RES RPI_GPIO_P1_22
#define PIN_DISPLAY_3310_DC RPI_GPIO_P1_15

// Input pins.
#define PIN_INPUT_A RPI_GPIO_P1_13
#define PIN_INPUT_B RPI_GPIO_P1_12
#define PIN_INPUT_C RPI_GPIO_P1_18
#define PIN_INPUT_D RPI_GPIO_P1_11

#define WAIT_TIMEOUT -1

/**
* Initalize hardware: inputs and display.
*/
void hardware_init(void);

/**
* Close hardware and release resources/pins.
*/
void hardware_close(void);

/**
* Wait for an active button. Block until any button is pressed.
* \return the index, 0-3, for the pressed button.
*/
uint8_t hardware_wait_button(void);

/**
* Wait for an active button. Block until any button is pressed,
* or until timeout.
* \return the index, 0-3, for the pressed button. or WAIT_TIMEOUT on timeout.
*/
int8_t hardware_wait_button_tmo(uint32_t timeout_ms);

/**
* Clear display.
*/
void display_clear(void);

/**
* Write a string starting at the specified row, clearing first.
* \param row the row to start writing at.
* \param str the string to write.
*/
void display_write_row(uint8_t row, const char * str);

/**
* Write a string starting at the specified row.
* \param row the row to start writing at.
* \param str the string to write.
*/
void display_write_row_noclear(uint8_t row, const char * str);

/**
* Delay. Blocking.
*/
void hardware_delay(uint16_t ms);

#endif // __HARDWARE_H__
