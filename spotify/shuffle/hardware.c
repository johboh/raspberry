#include "hardware.h"
#include "../spi.h"
#include "../display3310.h"
#include "../input.h"
#include <bcm2835.h>

static display3310_t display = { PIN_DISPLAY_3310_RES, PIN_DISPLAY_3310_DC };
static input_t inputs[4] = {
	{PIN_INPUT_A, 'A'},
	{PIN_INPUT_B, 'B'},
	{PIN_INPUT_C, 'C'},
	{PIN_INPUT_D, 'D'},
};

void hardware_init() {
	// Initalize gpio lib.
	if (!bcm2835_init())
		exit(1);

	// Setup SPI.
	spi_init();
	
	// Setup display.
	display3310_init(&display);
	
	// Setup input buttons.
	input_init(inputs, sizeof(inputs) / sizeof(input_t));
}

void hardware_close() {
	spi_close();
}

uint8_t hardware_wait_button() {
	return hardware_wait_button_tmo(0);
}

int8_t hardware_wait_button_tmo(uint32_t timeout_ms) {
	input_t * input = input_wait_any(inputs, sizeof(inputs) / sizeof(input_t), timeout_ms);
	return (input ? input->index : WAIT_TIMEOUT);
}

void display_clear(void) {
	display3310_clear(&display);
}

void display_write_row(uint8_t row, const char * str) {
	display3310_clear_row(&display, row);
	display3310_write_row(&display, row, str);
}

void display_write_row_noclear(uint8_t row, const char * str) {
	display3310_write_row(&display, row, str);
}

void hardware_delay(uint16_t ms) {
	delay(ms);
}