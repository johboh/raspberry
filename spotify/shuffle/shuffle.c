/**
 * Copyright (c) 2006-2010 Spotify Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 * This example application is the most minimal way to just play a spotify URI.
 *
 * This file is part of the libspotify examples suite.
 */

#include <errno.h>
#include <libgen.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include <libspotify/api.h>

#include "audio.h"
#include "hardware.h"

#define ACTION_NONE 0xFF
#define ACTION_PLAY_PAUSE 0
#define ACTION_NEXT 1

/* --- Data --- */
/// The application key is specific to each project, and allows Spotify
/// to produce statistics on how our service is used.
extern const uint8_t g_appkey[];
/// The size of the application key.
extern const size_t g_appkey_size;

/// The output queue for audo data
static audio_fifo_t g_audiofifo;
/// Synchronization mutex for the main thread
static pthread_mutex_t g_notify_mutex;
/// Synchronization condition variable for the main thread
static pthread_cond_t g_notify_cond;
/// Synchronization variable telling the main thread to process events
static int g_notify_do;
/// Synchronization variable telling that playback is done.
static int g_playback_done;
/// The global session handle
static sp_session *g_sess;
/// Playlist uri to shuffle within
static sp_link *g_playlist_link;
/// Playlist to shuffle witin
static sp_playlist *g_playlist;
/// Reference to current track.
static sp_track *g_currenttrack;
/// Current track in playlist.
static int g_track_index;
/// Thread for input.
static pthread_t g_pthread_input;
/// Action from input.
static int g_action = ACTION_NONE;


static void try_start_playback(void)
{
    sp_track *t;

    if (!g_playlist)
        return;

    if (!sp_playlist_num_tracks(g_playlist)) {
        fprintf(stderr, "shuffle: No tracks in playlist. Waiting\n");
        return;
    }

    if (sp_playlist_num_tracks(g_playlist) < g_track_index) {
        fprintf(stderr, "shuffle: No more tracks in playlist. Waiting\n");
        return;
    }

    t = sp_playlist_track(g_playlist, g_track_index);

    if (g_currenttrack && t != g_currenttrack) {
        /* Someone changed the current track */
        audio_fifo_flush(&g_audiofifo);
        sp_session_player_unload(g_sess);
        g_currenttrack = NULL;
    }

    if (!t) {
	    fprintf(stderr, "shuffle: Track is null, cannot play.\n");
        return;
	}

    if (sp_track_error(t) != SP_ERROR_OK) {
		fprintf(stderr, "shuffle: Track has error: %s\n", sp_error_message(sp_track_error(t)));
        return;
	}

    if (g_currenttrack == t)
        return;

    g_currenttrack = t;

    printf("shuffle: Now playing \"%s\"...\n", sp_track_name(t));
    fflush(stdout);
	
	display_write_row(1, "Now playing");
	display_write_row(3, sp_track_name(t));
	display_write_row(4, "by");
	display_write_row(5, sp_artist_name(sp_track_artist(t, 0)));

    sp_session_player_load(g_sess, t);
    sp_session_player_play(g_sess, 1);
}

/* --------------------------  PLAYLIST CALLBACKS  ------------------------- */
static void tracks_added(sp_playlist *pl, sp_track * const *tracks,
                         int num_tracks, int position, void *userdata)
{
    if (pl != g_playlist)
        return;

    printf("shuffle: %d tracks were added\n", num_tracks);
    fflush(stdout);
    try_start_playback();
}

static void tracks_removed(sp_playlist *pl, const int *tracks,
                           int num_tracks, void *userdata)
{
    int i, k = 0;

    if (pl != g_playlist)
        return;

    for (i = 0; i < num_tracks; ++i)
        if (tracks[i] < g_track_index)
            ++k;

    g_track_index -= k;

    printf("shuffle: %d tracks were removed\n", num_tracks);
    fflush(stdout);
    try_start_playback();
}

static void tracks_moved(sp_playlist *pl, const int *tracks,
                         int num_tracks, int new_position, void *userdata)
{
    if (pl != g_playlist)
        return;

    printf("shuffle: %d tracks were moved around\n", num_tracks);
    fflush(stdout);
    try_start_playback();
}

static void playlist_renamed(sp_playlist *pl, void *userdata)
{
	// eh? What todo?
    const char *name = sp_playlist_name(pl);
	
	if (g_playlist == pl) {
		printf("shuffle: current playlist renamed to \"%s\".\n", name);
	}
}

static sp_playlist_callbacks pl_callbacks = {
    .tracks_added = &tracks_added,
    .tracks_removed = &tracks_removed,
    .tracks_moved = &tracks_moved,
    .playlist_renamed = &playlist_renamed,
};

/* ---------------------------  SESSION CALLBACKS  ------------------------- */
/**
 * This callback is called when an attempt to login has succeeded or failed.
 *
 * @sa sp_session_callbacks#logged_in
 */
static void logged_in(sp_session *sess, sp_error error) {

	if (SP_ERROR_OK != error) {
		fprintf(stderr, "Login failed: %s\n", sp_error_message(error));
		exit(2);
	}
	
	// Create playlist.
	g_playlist = sp_playlist_create(sess, g_playlist_link);
	sp_playlist_add_callbacks(g_playlist, &pl_callbacks, NULL);
}
/**
 * This callback is called from an internal libspotify thread to ask
 * us to reiterate the main loop.
 *
 * We notify the main thread using a condition variable and a protected variable.
 *
 * @sa sp_session_callbacks#notify_main_thread
 */
static void notify_main_thread(sp_session *sess) {
	pthread_mutex_lock(&g_notify_mutex);
	g_notify_do = 1;
	pthread_cond_signal(&g_notify_cond);
	pthread_mutex_unlock(&g_notify_mutex);
}

/**
 * This callback is used from libspotify whenever there is PCM data available.
 *
 * @sa sp_session_callbacks#music_delivery
 */
static int music_delivery(sp_session *sess, const sp_audioformat *format,
                          const void *frames, int num_frames) {
	audio_fifo_t *af = &g_audiofifo;
	audio_fifo_data_t *afd;
	size_t s;

	if (num_frames == 0)
		return 0; // Audio discontinuity, do nothing

	pthread_mutex_lock(&af->mutex);

	/* Buffer one second of audio */
	if (af->qlen > format->sample_rate) {
		pthread_mutex_unlock(&af->mutex);

		return 0;
	}

	s = num_frames * sizeof(int16_t) * format->channels;

	afd = malloc(sizeof(audio_fifo_data_t) + s);
	memcpy(afd->samples, frames, s);

	afd->nsamples = num_frames;

	afd->rate = format->sample_rate;
	afd->channels = format->channels;

	TAILQ_INSERT_TAIL(&af->q, afd, link);
	af->qlen += num_frames;

	pthread_cond_signal(&af->cond);
	pthread_mutex_unlock(&af->mutex);

	return num_frames;
}


/**
 * This callback is used from libspotify when the current track has ended
 *
 * @sa sp_session_callbacks#end_of_track
 */
static void end_of_track(sp_session *sess) {
	pthread_mutex_lock(&g_notify_mutex);
	g_playback_done = 1;
	g_notify_do = 1;
	pthread_cond_signal(&g_notify_cond);
	pthread_mutex_unlock(&g_notify_mutex);
}

/**
 * Callback called when libspotify has new metadata available
 *
 * @sa sp_session_callbacks#metadata_updated
 */
static void metadata_updated(sp_session *sess) {
	try_start_playback();
}

/**
 * Notification that some other connection has started playing on this account.
 * Playback has been stopped.
 *
 * @sa sp_session_callbacks#play_token_lost
 */
static void play_token_lost(sp_session *sess) {
	audio_fifo_flush(&g_audiofifo);

	if (g_currenttrack != NULL) {
		sp_session_player_unload(g_sess);
		g_currenttrack = NULL;
	}
}

static void log_message(sp_session *session, const char *msg) {
	puts(msg);
}

/**
 * The session callbacks
 */
static sp_session_callbacks session_callbacks = {
	.logged_in = &logged_in,
	.notify_main_thread = &notify_main_thread,
	.music_delivery = &music_delivery,
	.metadata_updated = &metadata_updated,
	.play_token_lost = &play_token_lost,
	.log_message = &log_message,
	.end_of_track = &end_of_track,
};

/**
 * The session configuration. Note that application_key_size is an
 * external, so we set it in main() instead.
 */
static sp_session_config spconfig = {
	.api_version = SPOTIFY_API_VERSION,
	.cache_location = "",
	.settings_location = "/ramcache",
	.application_key = g_appkey,
	.application_key_size = 0, // Set in main()
	.user_agent = "raspberrypi-shuffle",
	.callbacks = &session_callbacks,
	NULL,
};
/* -------------------------  END SESSION CALLBACKS  ----------------------- */

/**
* True random, yep! Using the high bits.
*/
uint32_t true_random(uint32_t max) {
	return (int) ((double)max * (rand() / (RAND_MAX + 1.0))); 
}

static void set_next_track(void) {
	g_track_index = true_random(sp_playlist_num_tracks(g_playlist));
}

static void track_ended(void)
{
    if (g_currenttrack) {
        g_currenttrack = NULL;
        sp_session_player_unload(g_sess);
		set_next_track();
		try_start_playback();
    }
}

/**
 * Show usage information
 *
 * @param  progname  The program name
 */
static void usage(const char *progname) {
	fprintf(stderr, "usage: %s -u <username> -p <password> -l playlist_uri\n", progname);
}

static void read_input(void *ptr) {
	// Read input forever.
	while(1) {
		uint8_t i = hardware_wait_button();
		pthread_mutex_lock(&g_notify_mutex);
		g_notify_do = 1;
		g_action = i;
		pthread_cond_signal(&g_notify_cond);
		pthread_mutex_unlock(&g_notify_mutex);
		hardware_delay(200);
		// Wait until button released.
		while(hardware_wait_button_tmo(50) != WAIT_TIMEOUT);
	}
}

int main(int argc, char **argv) {
	sp_session *sp;
	sp_error err;
	int next_timeout = 0;
	const char *username      = NULL;
	const char *password      = NULL;
	const char *playlist_link = NULL;
	int opt;

	while ((opt = getopt(argc, argv, "u:p:l:")) != EOF) {
		switch (opt) {
		case 'u':
			username = optarg;
			break;
		case 'p':
			password = optarg;
			break;
		case 'l':
			playlist_link = optarg;
			break;
		default:
			exit(1);
		}
	}

	if (!username || !password || !playlist_link) {
		usage(basename(argv[0]));
		exit(1);
	}

	g_playlist_link = sp_link_create_from_string(playlist_link);
	
	// Seed random.
	srand(time(NULL));
	
	// Initalize hardware.
	hardware_init();
	display_clear();
	pthread_create(&g_pthread_input, NULL, (void *) &read_input, NULL);

	audio_init(&g_audiofifo);

	/* Create session */
	spconfig.application_key_size = g_appkey_size;

	err = sp_session_create(&spconfig, &sp);

	if (SP_ERROR_OK != err) {
		fprintf(stderr, "Unable to create session: %s\n",
			sp_error_message(err));
		exit(1);
	}

	g_sess = sp;

	pthread_mutex_init(&g_notify_mutex, NULL);
	pthread_cond_init(&g_notify_cond, NULL);
	
	sp_session_login(sp, username, password, 0);
	pthread_mutex_lock(&g_notify_mutex);

	for (;;) {
		if (next_timeout == 0) {
			while(!g_notify_do && !g_playback_done && g_action == ACTION_NONE)
				pthread_cond_wait(&g_notify_cond, &g_notify_mutex);
		} else {
			struct timespec ts;

#if _POSIX_TIMERS > 0
			clock_gettime(CLOCK_REALTIME, &ts);
#else
			struct timeval tv;
			gettimeofday(&tv, NULL);
			TIMEVAL_TO_TIMESPEC(&tv, &ts);
#endif
			ts.tv_sec += next_timeout / 1000;
			ts.tv_nsec += (next_timeout % 1000) * 1000000;

			pthread_cond_timedwait(&g_notify_cond, &g_notify_mutex, &ts);
		}

		g_notify_do = 0;
		pthread_mutex_unlock(&g_notify_mutex);
		
		if (g_playback_done) {
			track_ended();
			g_playback_done = 0;
		}
		
		if (g_action != ACTION_NONE) {
			switch(g_action) {
				case ACTION_PLAY_PAUSE:
					// Play/Pause!
					printf("Play/Pause!\n");
					// How to get current playback status, eh?
					int playing = 1;
					if (g_sess)
						sp_session_player_play(g_sess, !playing);
					break;
				case ACTION_NEXT:
					printf("Next!\n");
					set_next_track();
					try_start_playback();
					break;
			}
			g_action = ACTION_NONE;
		}
		
		do {
			sp_session_process_events(sp, &next_timeout);
		} while (next_timeout == 0);

		pthread_mutex_lock(&g_notify_mutex);
	}

	return 0;
}
