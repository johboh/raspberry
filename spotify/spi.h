#ifndef __SPI_H__
#define __SPI_H__

#include <stdio.h>
#include <stdint.h>

/**
* Initialize SPI.
* bcm2835_init() must have been called first!
*/
void spi_init(void);

/**
* Close SPI and release its pins.
*/
void spi_close(void);

#endif // __SPI_H__