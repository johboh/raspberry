#include "HttpServer.h"
#include "HttpClient.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

HttpServer::HttpServer(const uint32_t port)
	: _port(port)
	, _socket(-1)
	, _started(false) {
}

HttpServer::~HttpServer() {
	stop();
}

bool HttpServer::start() {
	if (_started)
		return false;

	struct sockaddr_in sa_server;
	struct in_addr addr_local;

	// Create a new TCP connection handle.
	if ((_socket = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		std::cerr << "HttpServer: Failed to create socket." << std::endl;
		return false;
	}

	addr_local.s_addr = INADDR_ANY;
	sa_server.sin_family = AF_INET;
	sa_server.sin_port = htons(_port);
	sa_server.sin_addr = addr_local;

	/* Bind the connection to port on any
	local IP address. */
	if (bind(_socket, (struct sockaddr *) &sa_server, sizeof(sa_server)) < 0) {
		std::cerr << "HttpServer: Failed to bind socket." << std::endl;
		return false;
	}

	/* Put the connection into LISTEN state. */
	if (listen(_socket, 2) != 0) {
		std::cerr << "HttpServer: Failed to listen on socket." << std::endl;
		return false;
	}

	std::cout << "HttpServer: Listening at port " << _port << "..." << std::endl;
	_server_thread = std::thread(&HttpServer::run, this);
	_server_thread.detach();
	_started = true;
	return true;
}

void HttpServer::stop() {
	if (!_started)
		return;

	std::cout << "Stopping server" << std::endl;
	close(_socket);
	_started = false;
}

void HttpServer::run() {
	struct sockaddr_in sa_client;
	socklen_t length;
	struct timeval tv;
	fd_set fds;
	int rc;

	/* Loop forever. */
	while(1) {
		FD_ZERO(&fds);
		FD_SET(_socket, &fds);
		tv.tv_sec  = 3600;
		tv.tv_usec = 0;
		if ((rc = select(FD_SETSIZE, &fds, 0, 0, &tv)) < 0) {
			std::cerr << "HttpServer: Select failed." << std::endl;
			break;
		}

		if (rc == 0 || FD_ISSET(_socket, &fds) == 0) {
			continue;
		}

		/* Accept a new connection. */
		length = sizeof(sa_client);
		int socket = accept(_socket, (struct sockaddr *) &sa_client, &length);
		if (socket < 0)
			continue;

		// Create new client. The client delete itself.
		HttpClient *client = new HttpClient(socket);
		client->start();
	}
	stop();
}