#include "HttpHelper.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

/*  Read a line from a socket  */
ssize_t HttpHelper::readLine(int sockd, void *vptr, size_t maxlen) {
	ssize_t rc;
	size_t n;
	char c, *buffer;

	buffer = (char *)vptr;
	
	for (n = 1; n < maxlen; n++) {
		if ( (rc = read(sockd, &c, 1)) == 1) {
			*buffer++ = c;
			if(c == '\n')
				break;
		}
		else if (rc == 0) {
			if (n == 1)
				return 0;
			else
				break;
		}
		else {
			if (errno == EINTR)
				continue;
			std::cerr << "HttpHelper: Error in write line." << std::endl;
			return 0;
		}
	}

	*buffer = 0;
	return n;
}

ssize_t HttpHelper::writeLine(int sockd, const std::string &str) {
	return HttpHelper::writeLine(sockd, str.c_str(), str.size());
}

/*  Write a line to a socket  */
ssize_t HttpHelper::writeLine(int sockd, const void *vptr, size_t n) {
	size_t      nleft;
	ssize_t     nwritten;
	const char *buffer;

	buffer = (char *)vptr;
	nleft  = n;

	// Write in pages.
	while (nleft > 0) {
		if ( (nwritten = write(sockd, buffer, std::min((int)nleft, 2048))) <= 0) {
			if (errno == EINTR)
				nwritten = 0;
			else {
				std::cerr << "Error in writeLine(): " << strerror(errno) << std::endl;
				return 0;
			}
		}
		nleft  -= nwritten;
		buffer += nwritten;
	}

	return n;
}

// Decode an url, e.g. replace %xx with the corresponding ASCII character.
std::string HttpHelper::urlDecode(const std::string input) {
	std::string ret;
	char ch;
	size_t i, ii;
	for (i=0; i<input.length(); i++) {
		if (int(input[i])=='%') {
			sscanf(input.substr(i+1, 2).c_str(), "%x", &ii);
			ch = static_cast<char>(ii);
			ret += ch;
			i += 2;
		} else {
			ret += input[i];
		}
	}
	return ret;
}

std::string HttpHelper::getUrl(const std::string input, int *error) {
	// GET or POST only.
	size_t found = input.find("GET ");
	if (found == std::string::npos)
		found = input.find("POST ");
	if (found == std::string::npos) {
		*error = 501;
		return "";
	}

	size_t start = input.find("/", found);
	std::string retr = input.substr(start);
	if (retr.empty())
		*error = 401;

	return retr;
}