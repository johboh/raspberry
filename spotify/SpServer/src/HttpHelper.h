#ifndef __HTTPHELPER_H__
#define __HTTPHELPER_H__

#include <iostream>
#include <stdint.h>

class HttpHelper {
public:
	static ssize_t readLine(int sockd, void *vptr, size_t maxlen);
	static ssize_t writeLine(int sockd, const void *vptr, size_t n);
	static ssize_t writeLine(int sockd, const std::string &str);
	static std::string getUrl(const std::string input, int *error);
	static std::string urlDecode(const std::string input);
};

#endif // __HTTPHELPER_H__