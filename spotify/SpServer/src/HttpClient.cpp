#include "HttpClient.h"
#include "HttpHelper.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

#include <vector>
#include <iostream>
#include <sstream>

HttpClient::HttpClient(const int socket)
	: _socket(socket)
	, _status(200) {
}

HttpClient::~HttpClient() {
}

void HttpClient::run() {
	char buffer[16384] = {0};

	// Read first request line.
	HttpHelper::readLine(_socket, buffer, sizeof(buffer) - 1);

	// Parse Request path.
	std::string input = buffer;
	std::string url = HttpHelper::urlDecode(HttpHelper::getUrl(input, &_status));
	std::cout << "Uri: " << url << std::endl;

	// Split by /
	std::vector<std::string> args;
	std::stringstream ss(url);
	std::string item;
	while(std::getline(ss, item, '/')) {
		args.push_back(item);
	}

	// Read payload

	// Process request

	// Write back.
	{
		std::ostringstream oss;
		oss << "HTTP/1.0 " << _status << " OK\r\n";
		HttpHelper::writeLine(_socket, oss.str());
	}

	// Write content type header.

	HttpHelper::writeLine(_socket, "Server: SpServer 1.0\r\n");
	HttpHelper::writeLine(_socket, "\r\n");

	// Write payload.

	close(_socket);
	delete this;
}

void HttpClient::start() {
	_thread = std::thread(&HttpClient::run, this);
	_thread.detach();
}

