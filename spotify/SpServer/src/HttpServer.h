#ifndef __HTTPSERVER_H__
#define __HTTPSERVER_H__

#include <iostream>
#include <thread>
#include <stdint.h>

class HttpClient;

class HttpServer {
public:
	HttpServer(const uint32_t port);
	virtual ~HttpServer();

	bool start();
	void stop();

private:
	void run();

private:
	uint32_t _port;
	int _socket;
	bool _started;
	std::thread _server_thread;
};

#endif // __HTTPSERVER_H__