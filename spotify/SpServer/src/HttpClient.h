#ifndef __HTTPCLIENT_H__
#define __HTTPCLIENT_H__

#include <iostream>
#include <thread>
#include <stdint.h>

class HttpClient {
public:
	HttpClient(const int socket);
	virtual ~HttpClient();

	void start();

private:
	void run();

private:
	std::thread _thread;
	int _socket;
	int _status;
};

#endif // __HTTPCLIENT_H__