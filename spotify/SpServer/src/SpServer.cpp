#include <iostream>
#include "HttpServer.h"
using namespace std;

int main() {
	std::cout << "SpServer started." << std::endl;
	HttpServer http(666);
	http.start();

	getchar();
	http.stop();

	return 0;
}
