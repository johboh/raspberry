#ifndef __DISPLAY3310_H__
#define __DISPLAY3310_H__

#include <bcm2835.h>
#include <stdio.h>
#include <stdint.h>

typedef struct {
	RPiGPIOPin res; // Pin to !reset
	RPiGPIOPin dc;  // Pin to Data/!Command
} display3310_t;

/**
* Initialize and clear display.
* SPI must have been initalized first!
*/
void display3310_init(display3310_t * instance);

/**
* Clear display.
*/
void display3310_clear(display3310_t * instance);

/**
* Clear a specific row.
*/
void display3310_clear_row(display3310_t * instance, uint8_t row);

/**
* Goto a specific row and column. All writes will start at this
* position.
* \param column range 0-80
* \param row range 0-6
*/
void display3310_goto(display3310_t * instance, uint8_t column, uint8_t row);

/**
* Write a string at the current position.
* \param str the string to write.
*/
void display3310_write(display3310_t * instance, const char * str);

/**
* Write a string starting at the specified row.
* \param row the row to start writing at.
* \param str the string to write.
*/
void display3310_write_row(display3310_t * instance, uint8_t row, const char * str);

#endif // __DISPLAY3310_H__
