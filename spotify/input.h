#ifndef __INPUT_H__
#define __INPUT_H__

#include <bcm2835.h>
#include <stdio.h>
#include <stdint.h>

typedef struct {
	RPiGPIOPin pin;
	char ch;
	uint8_t index;
} input_t;

/**
* Initialize inputs. All inputs are configurated as active low.
* bcm2835_init() must have been called first!
* \param inputs a list of pins/gpio/inputs to initalize.
* \param length number of pins/gpio/inputs in the list.
*/
void input_init(input_t * inputs, uint8_t length);

/**
* Wait for any input to be active (low).
* blocking until any button is active, or until timeout if timeout enabled.
* \param inputs a list inputs to read.
* \param length number of inputs in the list.
* \param timeout_ms timeout in ms, or < 1 to disable timeout.
* \return the active input, or NULL if no button was active.
*/
input_t * input_wait_any(input_t * inputs, uint8_t length, uint32_t timeout_ms);

/**
* Check if any input is active (low), non-blocking.
* \param inputs a list inputs to read.
* \param length number of inputs in the list.
* \return the active input, or null if no input is active.
*/
input_t * input_read(input_t * inputs, uint8_t length);

#endif // __INPUT_H__