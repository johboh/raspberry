﻿using System;
using System.Collections.Generic;
using System.Text;
using libspotifydotnet;
using System.Runtime.InteropServices;
using Jamcast.Plugins.Spotify.API;
using System.IO;

namespace playtrack
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");

            try
            {
                Spotify.Initialize();
            }
            catch (Exception ex)
            {
                Log.Error("Could not initialize the Spotify interface.", ex);
                return;
            }

            string username = "";
            string password = "";
            byte[] applicationKey = File.ReadAllBytes("spotify_appkey.key");

            if (String.IsNullOrEmpty(username.Trim()) ||
                String.IsNullOrEmpty(password.Trim()) ||
                applicationKey == null)
                return;

            try
            {
                if (!Jamcast.Plugins.Spotify.API.Spotify.Login(applicationKey, username, password))
                {
                    Log.Error("Spotify login failed for user {0}.", username);
                    return;
                }
            }
            catch
            {
                Log.Error("Spotify login error for user {0}.", username);
                return;
            }

            Log.Info("Spotify plugin initialized successfully.");


            /*
            libspotify.sp_session_callbacks sp_callbacks = new libspotify.sp_session_callbacks();
            sp_callbacks.connection_error = IntPtr.Zero;
            sp_callbacks.connectionstate_updated = IntPtr.Zero;
            sp_callbacks.credentials_blob_updated = IntPtr.Zero;
            sp_callbacks.end_of_track = IntPtr.Zero;
            sp_callbacks.get_audio_buffer_stats = IntPtr.Zero;
            sp_callbacks.log_message = IntPtr.Zero;
            sp_callbacks.logged_in = IntPtr.Zero;
            sp_callbacks.logged_out = IntPtr.Zero;
            sp_callbacks.message_to_user = IntPtr.Zero;
            sp_callbacks.metadata_updated = IntPtr.Zero;
            sp_callbacks.music_delivery = IntPtr.Zero;
            sp_callbacks.notify_main_thread = IntPtr.Zero;
            sp_callbacks.offline_error = IntPtr.Zero;
            sp_callbacks.offline_status_updated = IntPtr.Zero;
            sp_callbacks.play_token_lost = IntPtr.Zero;
            sp_callbacks.private_session_mode_changed = IntPtr.Zero;
            sp_callbacks.scrobble_error = IntPtr.Zero;
            sp_callbacks.start_playback = IntPtr.Zero;
            sp_callbacks.stop_playback = IntPtr.Zero;
            sp_callbacks.streaming_error = IntPtr.Zero;
            sp_callbacks.userinfo_updated = IntPtr.Zero;

            IntPtr callbacksPtr = Marshal.AllocHGlobal(Marshal.SizeOf(sp_callbacks));
            Marshal.StructureToPtr(sp_callbacks, callbacksPtr, true);

            libspotify.sp_session_config sp_config = new libspotify.sp_session_config();
            sp_config.api_version = 12;
            //sp_config.application_key
            //sp_config.application_key_size
            sp_config.cache_location = System.IO.Path.GetTempPath();
            sp_config.settings_location = System.IO.Path.GetTempPath();
            sp_config.user_agent = "libspotifydotnet-mono";
            sp_config.callbacks = callbacksPtr;
            IntPtr g_sess;

            libspotify.sp_session_create(ref sp_config, out g_sess);*/
        }
    }
}
