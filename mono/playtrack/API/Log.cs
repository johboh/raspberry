﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jamcast.Plugins.Spotify.API
{
    class Log
    {
        public static void Debug(string format, params object[] args) 
        {
            Console.WriteLine("Debug: " + format, args);
        }
        public static void Error(string format, params object[] args)
        {
            Console.WriteLine("Error: " + format, args);
        }
        public static void Warning(string format, params object[] args)
        {
            Console.WriteLine("Warning: " + format, args);
        }
        public static void Info(string format, params object[] args)
        {
            Console.WriteLine("Info: " + format, args);
        }
    }
}
