#!/usr/bin/env python2

import cmd
import readline
import sys
import traceback
import time
import threading
import os

from spotify import ArtistBrowser, Link, ToplistBrowser
from spotify.audiosink import import_audio_sink
from spotify.manager import (SpotifySessionManager, SpotifyPlaylistManager,
    SpotifyContainerManager)

AudioSink = import_audio_sink()
container_loaded = threading.Event()

class JukeboxUI(cmd.Cmd, threading.Thread):

    prompt = "jukebox> "

    def __init__(self, jukebox):
        cmd.Cmd.__init__(self)
        threading.Thread.__init__(self)
        self.jukebox = jukebox
        self.playlist = None
        self.track = None
        self.results = False

    def run(self):
        self.cmdloop()

    def do_logout(self, line):
        self.jukebox.session.logout()

    def do_quit(self, line):
        print "Goodbye!"
        self.jukebox.terminate()
        return True

    def do_play(self, line):
        if not line:
            self.jukebox.play()
            return
        if line.startswith("spotify:"):
            # spotify url
            l = Link.from_string(line)
            if not l.type() == Link.LINK_TRACK:
                print "You can only play tracks!"
                return
            self.jukebox.load_track(l.as_track())
        else:
            try:
                playlist, track = map(int, line.split(' ', 1))
            except ValueError:
                print "Usage: play [track_link] | [playlist] [track]"
                return
            self.jukebox.load(playlist, track)
        self.jukebox.play()

    def do_stop(self, line):
        self.jukebox.stop()

    def do_next(self, line):
        self.jukebox.next()

    def emptyline(self):
        pass

    def do_watch(self, line):
        if not line:
            print """Usage: watch [playlist]
You will be notified when tracks are added, moved or removed from the playlist."""
        else:
            try:
                p = int(line)
            except ValueError:
                print "That's not a number!"
                return
            if p < 0 or p >= len(self.jukebox.ctr):
                print "That's out of range!"
                return
            self.jukebox.watch(self.jukebox.ctr[p])

    def do_unwatch(self, line):
        if not line:
            print "Usage: unwatch [playlist]"
        else:
            try:
                p = int(line)
            except ValueError:
                print "That's not a number!"
                return
            if p < 0 or p >= len(self.jukebox.ctr):
                print "That's out of range!"
                return
            self.jukebox.watch(self.jukebox.ctr[p], True)

    def do_shell(self, line):
        self.jukebox.shell()

    do_EOF = do_quit


## playlist callbacks ##
class JukeboxPlaylistManager(SpotifyPlaylistManager):
    def tracks_added(self, p, t, i, u):
        print 'Tracks added to playlist %s' % p.name()

    def tracks_moved(self, p, t, i, u):
        print 'Tracks moved in playlist %s' % p.name()

    def tracks_removed(self, p, t, u):
        print 'Tracks removed from playlist %s' % p.name()

## container calllbacks ##
class JukeboxContainerManager(SpotifyContainerManager):
    def container_loaded(self, c, u):
        print 'Container loaded !'

    def playlist_added(self, c, p, i, u):
        print 'Container: playlist "%s" added.' % p.name()

    def playlist_moved(self, c, p, oi, ni, u):
        print 'Container: playlist "%s" moved.' % p.name()

    def playlist_removed(self, c, p, i, u):
        print 'Container: playlist "%s" removed.' % p.name()

class Jukebox(SpotifySessionManager):

    queued = False
    playlist = 2
    track = 0
    appkey_file = os.path.join(os.path.dirname(__file__), 'spotify_appkey.key')

    def __init__(self, *a, **kw):
        SpotifySessionManager.__init__(self, *a, **kw)
        self.audio = AudioSink(backend=self)
        self.ui = JukeboxUI(self)
        self.ctr = None
        self.playing = False
        self._queue = []
        self.playlist_manager = JukeboxPlaylistManager()
        self.container_manager = JukeboxContainerManager()
        print "Logging in, please wait..."


    def logged_in(self, session, error):
        if error:
            print error
            return
        print "Loggin OK!"
        self.session = session
        try:
            #self.ctr = session.playlist_container()
            #self.container_manager.watch(self.ctr)
            #self.starred = session.starred()
            #self.ui.start()
            self.load_track(Link.from_string('spotify:track:0knqVSsgD7C8yu5yNmQFbA').as_track())
            self.play()
        except:
            traceback.print_exc()

    def logged_out(self, session):
        self.ui.cmdqueue.append("quit")

    def metadata_updated(self, session):
        print 'Metadata updated, play again.'
        self.load_track(Link.from_string('spotify:track:0knqVSsgD7C8yu5yNmQFbA').as_track())
        self.play()

    def load_track(self, track):
        if self.playing:
            self.stop()
        try:
            self.session.load(track)
        except:
            print 'exception when loading'
        print "Loading %s" % track.name()

    def load(self, playlist, track):
        if self.playing:
            self.stop()
        #if 0 <= playlist < len(self.ctr):
        #    pl = self.ctr[playlist]
        #elif playlist == len(self.ctr):
        #    pl = self.starred
        self.session.load(pl[track])
        print "Loading %s from %s" % (pl[track].name(), pl.name())

    def play(self):
        self.session.play(1)
        print "Playing"
        self.playing = True

    def stop(self):
        self.session.play(0)
        print "Stopping"
        self.playing = False

    def music_delivery_safe(self, *a, **kw):
        return self.audio.music_delivery(*a, **kw)

    def next(self):
        self.stop()
        if self._queue:
            t = self._queue.pop()
            self.load(*t)
            self.play()
        else:
            self.stop()

    def end_of_track(self, sess):
        print "track ends, play same track again..."
        self.load_track(Link.from_string('spotify:track:0knqVSsgD7C8yu5yNmQFbA').as_track())
        #self.next()

    def watch(self, p, unwatch=False):
        if not unwatch:
            print "Watching playlist: %s" % p.name()
            self.playlist_manager.watch(p);
        else:
            print "Unatching playlist: %s" % p.name()
            self.playlist_manager.unwatch(p)

    def shell(self):
        import code
        shell = code.InteractiveConsole(globals())
        shell.interact()

if __name__ == '__main__':
    import optparse
    op = optparse.OptionParser(version="%prog 0.1")
    op.add_option("-u", "--username", help="spotify username")
    op.add_option("-p", "--password", help="spotify password")
    (options, args) = op.parse_args()
    session_m = Jukebox(options.username, options.password, True, '', '/ramcache')
    session_m.connect()
